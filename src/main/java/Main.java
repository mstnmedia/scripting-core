
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.RESTTemplate;
import com.mstn.scripting.core.Velocity;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.enums.WorkflowStepOptionTypes;
import com.mstn.scripting.core.models.IdValueParams;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.datatype.DatatypeFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Clase con métodos que prueban comportamiento de las clases y funciones en
 * este proyecto.
 *
 * @author amatos
 */
public class Main {

	/**
	 * @param args the command line arguments
	 * @throws java.io.IOException
	 */
	static Object testJSON(String[] args) throws IOException {
		String a = JSON.toString(args);
		String b = JSON.toString("weuhwf");
		return JSON.toNode("{}");
	}

	static Object testJSON2() throws Exception {
		String a = "[1,2,4,5,{\"name\":\"Abel\", \"age\": 21}]";
		ArrayNode array = JSON.toArrayNode(a);
		return array;
	}

	static Object dates() throws Exception {
		return Arrays.asList(
				DATE.toDate("2018-05-31"),
				DatatypeFactory.newInstance().newXMLGregorianCalendar("2018-05-31")
		);
	}

	static Object interpolateVTL() throws Exception {
		Map<String, Object> values = new HashMap();
		String emailText = "$defaultTimeZone $defaultLocale\n"
				+ "#set($date = $dateTool.toDate('yyyy-MM-dd', $fecha))\n"
				+ "$fecha $dateTool\n"
				+ "Fecha de prueba: $dateTool.format('short', $date)\n"
				+ "#set($columns=["
				+ " {'name': 'id', 'label': 'ID' },"
				+ " { "
				+ "  'name': 'value', "
				+ "  'label': 'Valor', "
				+ "  'type': 'datetime', "
				+ "  'parseFormat': 'yyyy-MM-ddTHH:mm:ss', " //Esta estructura es la predeterminada para convertir la fecha
				+ "  'format': 'short' " //Esta estructura es la predeterminada para formatear la fecha.
				+ " }"
				+ "])\n"
				+ "#set($prefix = 'fechas')\n"
				+ "#tableTemplate($columns $prefix)\n";
		values.put("fecha", "2018-05-31");
		List<IdValueParams> fechas = Arrays.asList(
				new IdValueParams(1, "2018-04-21"),
				new IdValueParams(1, "2014-07-01"),
				new IdValueParams(1, "2017-09-02"),
				new IdValueParams(1, "2019-04-25"),
				new IdValueParams(1, "2048-07-16"),
				new IdValueParams(1, "2015-05-31")
		);
		values.put("fechas_items", JSON.toString(fechas));
		values.put("fechas_size", fechas.size());
		for (int i = 0; i < fechas.size(); i++) {
			IdValueParams param = fechas.get(i);
			values.put("fechas" + i + "_id", param.getId());
			values.put("fechas" + i + "_value", param.getValue());
		}
		emailText = "\n#set($sizeName='$' + 'ensamblegetsubscriptionsbycustomerid_get_subscriptions_subscription' + '_size')";
		Object result = Velocity.interpolate(emailText, values);
		return result;
	}

	static List testParamChange(List list) {
		list = Arrays.asList(1, 2, 3);
		return list;
	}

	static Object testWhere() {
		List<Integer> list = new ArrayList();
		for (int i = 0; i < 2301; i++) {
			list.add(i);
		}
		Where where = new Where("id_workflow_step", Where.IN, list);
		return new WhereClause(
				new Where("wso.id_type", WorkflowStepOptionTypes.STEP),
				new Where("ws.name", Where.LIKE, "Paso").setType("clob"),
				new Where("active", Where.AND,
						new Where("w.main", Where.EQUALS, 1, Where.OR),
						new Where("", "value", Where.DESEQUALS, "ADSL", Where.OR, "clob")
				)//,
		//where
		).getPreparedString();
	}

	/**
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Object result = null;
//		result = testJSON(args);
//		result = testJSON2();
//		List result2 = Arrays.asList("a", "b");
//		result = testParamChange(result2);
//		result = String.valueOf(null);
//		result = String.valueOf("");
//		result = String.valueOf(new Transaction());
//		Probar envío de SMS
//		Probar envío de Correo
//		Probar envío de Correos de alertas
//		result = interpolateVTL();
//		result = testWhere();
//		result = RESTTemplate.postText();
//		result = RESTTemplate.getDiagnostico();
//		result = String.format("v%s %s %s %s %s ", "1", "dkjbds", "3", "4", "5");
		System.out.println(result);
	}

}
