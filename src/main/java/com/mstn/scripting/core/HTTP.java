/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import com.mstn.scripting.core.auth.Secrets;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

/**
 * Clase para realizar solicitudes HTTP entre microservicios, permitiendo la
 * autenticación y enviar datos en cuerpo de solicitudes POST.
 *
 * @author amatos
 */
public class HTTP {

	static public String queryString(Map<String, Object> query) {
		String strQuery = "";
		List<Map.Entry<String, Object>> params = query.entrySet().stream().collect(Collectors.toList());
		for (int i = 0; i < params.size(); i++) {
			Map.Entry<String, Object> param = params.get(i);
			strQuery += (i > 0 ? "&" : "") + param.getKey() + "=" + param.getValue();
		}
		return strQuery;
	}

	/**
	 * Permite la comunicación entre microservicios mediante solicitudes GET.
	 *
	 * @param httpClient Cliente HTTP del que se realizará la solicitud.
	 * @param url Localización del recurso que se solicitará.
	 * @param bearerToken Token de autenticación del usuario. El microservicio
	 * que recibió la solicitud inicial debe reembiarlo para que los demás
	 * puedan conocer quién realiza las acciones.
	 * @return La respueta obtenida como texto plano. Si una excepción {@code e}
	 * es obtenida en el proceso, devolverá {@code e.toString()}.
	 */
	static public String getResource(HttpClient httpClient, String url, String bearerToken) {
		try {
			HttpRequestBase request = new HttpGet(url);
			request.addHeader("Cookie", Secrets.TOKEN_COOKIE_NAME + "=" + bearerToken);
			HttpResponse response = httpClient.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(),
					Charset.forName("UTF-8")
			));

			StringBuilder result = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			return result.toString();
		} catch (IOException | UnsupportedOperationException e) {
			return e.toString();
		}
	}

	/**
	 *
	 * Permite la comunicación entre microservicios mediante solicitudes POST.
	 *
	 * @param httpClient Cliente HTTP del que se realizará la solicitud.
	 * @param url Localización del recurso que se solicitará.
	 * @param jsonPayload JSON que será enviado commo cuerpo de la solicutud.
	 * @param bearerToken Token de autenticación del usuario. El microservicio
	 * que recibió la solicitud inicial debe reembiarlo para que los demás
	 * puedan conocer quién realiza las acciones.
	 * @return La respueta obtenida como texto plano. Si una excepción {@code e}
	 * es obtenida en el proceso, devolverá {@code e.toString()}.
	 */
	static public String postResource(HttpClient httpClient, String url, String jsonPayload, String bearerToken) {
		try {
			StringEntity input = new StringEntity(jsonPayload, "UTF-8");
			input.setContentType(MediaType.APPLICATION_JSON_UTF_8);
			HttpRequestBase request = new HttpPost(url);
			((HttpPost) request).setEntity(input);

			request.addHeader("Cookie", Secrets.TOKEN_COOKIE_NAME + "=" + bearerToken);
			HttpResponse response = httpClient.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(),
					Charset.forName("UTF-8")
			));

			StringBuilder result = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			return result.toString();
		} catch (IOException | UnsupportedOperationException e) {
			return e.toString();
		}
	}

	/**
	 *
	 * @param httpClient
	 * @param server
	 * @param service
	 * @param query
	 * @param jsonPayload
	 * @return
	 */
	static public String requestResource(HttpClient httpClient, String server, String service, String query, String jsonPayload) {

		StringBuilder builder = new StringBuilder(server)
				.append("/")
				.append(service);
		if (query != null) {
			builder.append("?").append(query);
		}
		String url = builder.toString();
		return postResource(httpClient, url, jsonPayload, null);
	}
}
