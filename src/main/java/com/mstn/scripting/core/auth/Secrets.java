package com.mstn.scripting.core.auth;

/**
 * Contains secrets for demo purposes only. These values should be stored and
 * used securely.
 *
 * @author Hendrik van Huyssteen
 * @since 21 Sep 2017
 */
public class Secrets {

	/**
	 * Usado para pruebas.
	 *
	 * @deprecated
	 */
	public static final String LOGIN_USERNAME = "RoleOneUser";

	/**
	 * Usado para pruebas.
	 *
	 * @deprecated
	 */
	public static final String LOGIN_PASSWORD = "RoleOnePass";

	/**
	 * Key usado para generar los tokens de acceso.
	 */
	public static final byte[] JWT_SECRET_KEY = "dfwzsdzwh823zebdwdz772632gdsbd3333".getBytes();

	/**
	 * Nombre de la cookie en que se guardará el token en los clientes.
	 */
	public static final String TOKEN_COOKIE_NAME = "X-STRATUM-accesstoken";
}
