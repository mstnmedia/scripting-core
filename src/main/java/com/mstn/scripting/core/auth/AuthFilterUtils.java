package com.mstn.scripting.core.auth;

import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.JwtContext;
import org.jose4j.keys.HmacKey;
import com.github.toastshaman.dropwizard.auth.jwt.JwtAuthFilter;
import com.mstn.scripting.core.auth.basic.BasicAuthenticator;
import com.mstn.scripting.core.auth.jwt.JwtAuthenticator;
import com.mstn.scripting.core.auth.jwt.JwtAuthorizer;
import com.mstn.scripting.core.auth.jwt.User;

import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.PrincipalImpl;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;

/**
 * Auth filter utilities used elsewhere, just to keep all the auth config nice and central.
 *
 * @author Hendrik van Huyssteen
 * @since 21 Sep 2017
 */
public class AuthFilterUtils {
	
	/**
	 *
	 * @return
	 */
	public BasicCredentialAuthFilter<PrincipalImpl> buildBasicAuthFilter() {
		return new BasicCredentialAuthFilter.Builder<PrincipalImpl>()
				.setAuthenticator(new BasicAuthenticator())
				.setPrefix("Basic")
				.buildAuthFilter();
	}

	/**
	 *
	 * @return
	 */
	public AuthFilter<JwtContext, User> buildJwtAuthFilter() {
		// These requirements would be tightened up for production use
		final JwtConsumer consumer = new JwtConsumerBuilder()
				.setAllowedClockSkewInSeconds(300)
				.setRequireSubject()
				.setVerificationKey(new HmacKey(Secrets.JWT_SECRET_KEY))
				.build();

		return new JwtAuthFilter.Builder<User>()
				.setJwtConsumer(consumer)
				.setCookieName(Secrets.TOKEN_COOKIE_NAME)
				.setRealm("realm")
				.setPrefix("Bearer")
				.setAuthenticator(new JwtAuthenticator())
				.setAuthorizer(new JwtAuthorizer())
				.buildAuthFilter();
	}
}
