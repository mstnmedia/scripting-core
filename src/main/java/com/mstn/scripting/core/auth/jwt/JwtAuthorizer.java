package com.mstn.scripting.core.auth.jwt;

import com.mstn.scripting.core.DATE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.auth.Authorizer;
import java.util.List;

/**
 * Determines if a user is authorised to access an API endpoint, after they were
 * authenticated with {@link JwtAuthenticator}.
 *
 * See {@link com.hendrikvh.demos.jwtdemo.resources.ProtectedResourceOne}.
 *
 * @author Hendrik van Huyssteen
 * @since 21 Sep 2017
 */
public class JwtAuthorizer implements Authorizer<User> {

	private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthorizer.class);

	@Override
	public boolean authorize(User loggedUser, String requiredPermission) {
		if (loggedUser == null) {
			LOGGER.warn("msg=user object was null");
			return false;
		}
		if (loggedUser.getExpired() < DATE.nowAsLong()) {
			LOGGER.warn("msg=login time expired");
			return false;
		}
		List<String> permissions = loggedUser.getPermissions();
		if (permissions == null) {
			LOGGER.warn("msg=permissions were null, user={}, userId={}", loggedUser.getName(), loggedUser.getId());
			return false;
		}
		return loggedUser.hasPermissions(requiredPermission);
	}
}
