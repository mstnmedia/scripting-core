package com.mstn.scripting.core.auth.jwt;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Communicates JWT token to be used after login.
 *
 * @author Hendrik van Huyssteen
 * @since 21 Sep 2017
 */
public class LoginResponse {

	/**
	 *
	 * @param token
	 */
	public LoginResponse(String token) {
		this.token = token;
	}

	/**
	 *
	 */
	@JsonProperty("token")
	public String token;

}