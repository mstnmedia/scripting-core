package com.mstn.scripting.core.auth.jwt;

import com.mstn.scripting.core.DATE;
import io.dropwizard.auth.AuthenticationException;
import java.util.Optional;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.auth.Authenticator;

/**
 * Validates credentials for jwt auth on login in
 * @author Hendrik van Huyssteen
 * @since 21 Sep 2017
 */
public class JwtAuthenticator implements Authenticator<JwtContext, User> {

	private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticator.class);

	/**
	 * Extracts user roles from Jwt. This method will be called once the token's
	 * signature has been verified.
	 * <p>
	 * All JsonWebTokenExceptions will result in a 401 Unauthorized response.
	 *
	 * @param context
	 * @return {@link Optional}&lt;{@link User}&gt;
	 * @throws io.dropwizard.auth.AuthenticationException
	 */
	@Override
	public Optional<User> authenticate(JwtContext context) throws AuthenticationException {
		try {
			JwtClaims claims = context.getJwtClaims();
			User user = User.from(claims).setToken(context.getJwt());
			if (user.getExpired() < DATE.nowAsLong()) {
				LOGGER.warn("Authenticator: User expired");
				throw new AuthenticationException("Session timeout");
			}
			// TODO: Invalidar request si no se hace desde la misma IP desde la cual se creó el token.
			return Optional.of(user);
		} catch (Exception e) {
			LOGGER.warn("msg=Failed to authorise user: {}", e.getMessage(), e);
			return Optional.empty();
		}
	}
}
