package com.mstn.scripting.core.auth.jwt;

import java.util.Arrays;
import java.util.List;

/**
 * Contiene una lista de los permisos que pueden ser concedidos a los perfiles
 * del sistema.
 *
 * @author amatos
 */
public class UserPermissions {

	/**
	 * Permite al usuario tener acceso a todas las funcionalidades y le permite
	 * consultar todos los datos, sin importar la cadena de mando.
	 */
	public static final String SCRIPTING_MASTER = "scripting.master";

	/**
	 * Permite al usuario ver la opción Alertas en el menú web.
	 */
	public static final String ALERTS_SHOW = "alerts.show";

	/**
	 * Permite al usuario ver la opción Alertas disparadas en el menú web.
	 */
	public static final String ALERTS_HISTORY = "alerts.history";

	/**
	 * Permite al usuario consultar alertas y alertas disparadas.
	 */
	public static final String ALERTS_LIST = "alerts.list";

	/**
	 * Permite al usuario agregar alertas y alertas disparadas.
	 */
	public static final String ALERTS_ADD = "alerts.add";

	/**
	 * Permite al modificar modificar alertas y alertas disparadas.
	 */
	public static final String ALERTS_EDIT = "alerts.edit";

	/**
	 * Permite al usuario eliminar alertas y alertas disparadas.
	 */
	public static final String ALERTS_DELETE = "alerts.delete";

	/**
	 * Permite al usuario ver la opción Categorías en el menú web.
	 */
	public static final String CATEGORIES_SHOW = "categories.show";

	/**
	 * Permite al usuario consultar categorías.
	 */
	public static final String CATEGORIES_LIST = "categories.list";

	/**
	 * Permite al usuario agregar categorías.
	 */
	public static final String CATEGORIES_ADD = "categories.add";

	/**
	 * Permite al usuario modificar categorías.
	 */
	public static final String CATEGORIES_EDIT = "categories.edit";

	/**
	 * Permite al usuario eliminar categorías.
	 */
	public static final String CATEGORIES_DELETE = "categories.delete";

	/**
	 * Permite al usuario ver la opción Centros en el menú web.
	 */
	public static final String CENTERS_SHOW = "centers.show";

	/**
	 * Permite al usuario consultar centros.
	 */
	public static final String CENTERS_LIST = "centers.list";

	/**
	 * Permite al usuario agregar centros.
	 */
	public static final String CENTERS_ADD = "centers.add";

	/**
	 * Permite al usuario modificar centros.
	 */
	public static final String CENTERS_EDIT = "centers.edit";

	/**
	 * Permite al usuario eliminar centros.
	 */
	public static final String CENTERS_DELETE = "centers.delete";

	/**
	 * Permite al usuario modificar las notas del centro al que pertenece.
	 */
	public static final String CENTERS_EDITNOTES = "centers.editnotes";

	/**
	 * Permite al usuario ver la opción Contenidos en el menú web.
	 */
	public static final String CONTENTS_SHOW = "contents.show";

	/**
	 * Permite al usuario consultar contenidos.
	 */
	public static final String CONTENTS_LIST = "contents.list";

	/**
	 * Permite al usuario agregar contenidos.
	 */
	public static final String CONTENTS_ADD = "contents.add";

	/**
	 * Permite al usuario modificar contenidos.
	 */
	public static final String CONTENTS_EDIT = "contents.edit";

	/**
	 * Permite al usuario eliminar contenidos.
	 */
	public static final String CONTENTS_DELETE = "contents.delete";

	/**
	 * Permite al usuario ver la opción Criterios en el menú web.
	 */
	public static final String CRITERIAS_SHOW = "criterias.show";

	/**
	 * Permite al usuario consultar criterios.
	 */
	public static final String CRITERIAS_LIST = "criterias.list";

	/**
	 * Permite al usuario agregar criterios.
	 */
	public static final String CRITERIAS_ADD = "criterias.add";

	/**
	 * Permite al usuario modificar criterios.
	 */
	public static final String CRITERIAS_EDIT = "criterias.edit";

	/**
	 * Permite al usuario eliminar criterios.
	 */
	public static final String CRITERIAS_DELETE = "criterias.delete";

	/**
	 * Permite al usuario ver la opción Dashboard en el menú web.
	 */
	public static final String DASHBOARD_SHOW = "dashboard.show";

	/**
	 * Permite al usuario modificar las notas del Dashboard.
	 *
	 * @deprecated
	 * @see UserPermissions#CENTERS_EDITNOTES
	 */
	public static final String DASHBOARD_EDIT = "dashboard.edit";

	/**
	 * Permite al usuario ver los gráficos gerenciales en el Dashboard.
	 */
	public static final String DASHBOARD_CHARTS1 = "dashboard.charts1";

	/**
	 * Permite al usuario ver los gráficos personales en el Dashboard.
	 */
	public static final String DASHBOARD_CHARTS2 = "dashboard.charts2";

	/**
	 * Permite al usuario ver la opción Delegaciones en el menú web.
	 */
	public static final String DELEGATIONS_SHOW = "delegations.show";

	/**
	 * Permite al usuario consultar delegaciones.
	 */
	public static final String DELEGATIONS_LIST = "delegations.list";

	/**
	 * Permite al usuario agregar delegaciones.
	 */
	public static final String DELEGATIONS_ADD = "delegations.add";

	/**
	 * Permite al usuario modificar delegaciones.
	 */
	public static final String DELEGATIONS_EDIT = "delegations.edit";

	/**
	 * Permite al usuario eliminar delegaciones.
	 */
	public static final String DELEGATIONS_DELETE = "delegations.delete";

	/**
	 * Permite al usuario ver la opción Documentaciones en el menú web.
	 */
	public static final String DOCS_SHOW = "docs.show";

	/**
	 * Permite al usuario consultar documentaciones.
	 */
	public static final String DOCS_LIST = "docs.list";

	/**
	 * Permite al usuario agregar documentaciones.
	 */
	public static final String DOCS_ADD = "docs.add";

	/**
	 * Permite al usuario modificar documentaciones.
	 */
	public static final String DOCS_EDIT = "docs.edit";

	/**
	 * Permite al usuario eliminar documentaciones.
	 */
	public static final String DOCS_DELETE = "docs.delete";

	/**
	 *
	 */
	public static final String DOCS_ALERT = "docs.alert";

	/**
	 *
	 */
	public static final String DOCS_CENTER = "docs.center";

	/**
	 *
	 */
	public static final String DOCS_CONTENT = "docs.content";

	/**
	 *
	 */
	public static final String DOCS_DASHBOARD = "docs.dashboard";

	/**
	 *
	 */
	public static final String DOCS_INTERFACE = "docs.interface";

	/**
	 *
	 */
	public static final String DOCS_LANGUAGE = "docs.language";

	/**
	 *
	 */
	public static final String DOCS_LOG = "docs.log";

	/**
	 *
	 */
	public static final String DOCS_REPORT = "docs.report";

	/**
	 *
	 */
	public static final String DOCS_SECURITY = "docs.security";

	/**
	 *
	 */
	public static final String DOCS_STATISTIC = "docs.statistic";

	/**
	 *
	 */
	public static final String DOCS_TRANSACTION = "docs.transaction";

	/**
	 *
	 */
	public static final String DOCS_WORKFLOW = "docs.workflow";

	/**
	 * Permite al usuario consultar empleados.
	 */
	public static final String EMPLOYEES_LIST = "employees.list";

	/**
	 * Permite al usuario ver la opción Razones de cierre de llamadas en el menú
	 * web.
	 */
	public static final String END_CALL_SHOW = "endcall.show";

	/**
	 * Permite al usuario consultar razones de cierre de llamadas.
	 */
	public static final String END_CALL_LIST = "endcall.list";

	/**
	 * Permite al usuario agregar razones de cierre de llamadas.
	 */
	public static final String END_CALL_ADD = "endcall.add";

	/**
	 * Permite al usuario modificar razones de cierre de llamadas.
	 */
	public static final String END_CALL_EDIT = "endcall.edit";

	/**
	 * Permite al usuario eliminar razones de cierre de llamadas.
	 */
	public static final String END_CALL_DELETE = "endcall.delete";

	/**
	 * Permite al usuario ver la opción Interfaces en el menú web.
	 */
	public static final String INTERFACES_SHOW = "interfaces.show";

	/**
	 * Permite al usuario consultar interfaces y servicios de interfaces.
	 */
	public static final String INTERFACES_LIST = "interfaces.list";

	/**
	 * Permite al usuario agregar servicios de interfaces.
	 *
	 */
	public static final String INTERFACES_ADD = "interfaces.add";

	/**
	 * Permite al usuario modificar servicios de interfaces.
	 */
	public static final String INTERFACES_EDIT = "interfaces.edit";

	/**
	 * Permite al usuario eliminar servicios de interfaces.
	 *
	 */
	public static final String INTERFACES_DELETE = "interfaces.delete";

	/**
	 * Permite a los usuario invocar interfaces desde una transacción. Requerido
	 * para trabajar el perfil que crear transacciones.
	 */
	public static final String INTERFACES_CONSULT = "interfaces.consult";

	/**
	 * Permite al usuario ver la opción Idiomas por usuario y Idiomas del
	 * sistema en el menú web.
	 */
	public static final String LANGUAGE_SHOW = "language.show";

	/**
	 * Permite al usuario consultar Idiomas por usuario e idiomas del sistema.
	 */
	public static final String LANGUAGE_LIST = "language.list";

	/**
	 * Permite al usuario agregar idiomas por usuario e idiomas del sistema.
	 */
	public static final String LANGUAGE_ADD = "language.add";

	/**
	 * Permite al usuario modificar idiomas por usuario e idiomas del sistema.
	 */
	public static final String LANGUAGE_EDIT = "language.edit";

	/**
	 * Permite al usuario eliminar idiomas por usuario e idiomas del sistema.
	 */
	public static final String LANGUAGE_DELETE = "language.delete";

	/**
	 * Permite al usuario consultar el reporte Historial.
	 *
	 * @deprecated
	 */
	public static final String LOGS_SHOW = "logs.show";

	/**
	 * Permite al usuario ver la opción Reportes en el menú web.
	 */
	public static final String REPORTS_SHOW = "reports.show";

	/**
	 *
	 */
	public static final String REPORTS_ALERT = "reports.alert";

	/**
	 *
	 */
	public static final String REPORTS_CENTER = "reports.center";

	/**
	 *
	 */
	public static final String REPORTS_INTERFACE = "reports.interface";

	/**
	 *
	 */
	public static final String REPORTS_LOG = "reports.log";

	/**
	 *
	 */
	public static final String REPORTS_TRANSACTION = "reports.transaction";

	/**
	 *
	 */
	public static final String REPORTS_WORKFLOW = "reports.workflow";

	/**
	 * La seguridad de administra en el PAW (Portal Web de Aplicaciones).
	 *
	 * @deprecated
	 */
	public static final String SECURITY_SHOW = "security.show";

	/**
	 * La seguridad de administra en el PAW (Portal Web de Aplicaciones).
	 *
	 * @deprecated
	 */
	public static final String SECURITY_LIST = "security.list";

	/**
	 * La seguridad de administra en el PAW (Portal Web de Aplicaciones).
	 *
	 * @deprecated
	 */
	public static final String SECURITY_ADD = "security.add";

	/**
	 * La seguridad de administra en el PAW (Portal Web de Aplicaciones).
	 *
	 * @deprecated
	 */
	public static final String SECURITY_EDIT = "security.edit";

	/**
	 * La seguridad de administra en el PAW (Portal Web de Aplicaciones).
	 *
	 * @deprecated
	 */
	public static final String SECURITY_DELETE = "security.delete";

	/**
	 * @deprecated
	 */
	public static final String STATISTICS_SHOW = "statistics.show";

	/**
	 * Permite al usuario ver la opción Transacciones en el menú web.
	 */
	public static final String TRANSACTIONS_SHOW = "transactions.show";

	/**
	 * Permite al usuario ver la opción Transacciones activas en el menú web.
	 */
	public static final String TRANSACTIONS_ACTIVE = "transactions.active";

	/**
	 * Permite al usuario consultar transacciones.
	 */
	public static final String TRANSACTIONS_LIST = "transactions.list";

	/**
	 * Permite al usuario agregar transacciones.
	 */
	public static final String TRANSACTIONS_ADD = "transactions.add";

	/**
	 * Permite al usuario ver la opción Flujos en el menú web.
	 */
	public static final String WORKFLOWS_SHOW = "workflows.show";

	/**
	 * Permite al usuario consultar flujos.
	 */
	public static final String WORKFLOWS_LIST = "workflows.list";

	/**
	 * Permite al usuario agregar flujos.
	 */
	public static final String WORKFLOWS_ADD = "workflows.add";

	/**
	 * Permite al usuario modificar flujos.
	 */
	public static final String WORKFLOWS_EDIT = "workflows.edit";

	/**
	 * Permite al usuario eliminar flujos.
	 */
	public static final String WORKFLOWS_DELETE = "workflows.delete";

	/**
	 * Permite al usuario publicar flujos.
	 */
	public static final String WORKFLOWS_PUBLISH = "workflows.publish";

	/**
	 *
	 * @return Lista de todos los permisos permitidos para un usuario.
	 */
	static public List<String> getAllPermissions() {
		return Arrays.asList(
				SCRIPTING_MASTER,
				ALERTS_SHOW, ALERTS_LIST, ALERTS_ADD, ALERTS_EDIT, ALERTS_DELETE, ALERTS_HISTORY,
				CATEGORIES_SHOW, CATEGORIES_LIST, CATEGORIES_ADD, CATEGORIES_EDIT, CATEGORIES_DELETE,
				END_CALL_SHOW, END_CALL_LIST, END_CALL_ADD, END_CALL_EDIT, END_CALL_DELETE,
				LANGUAGE_SHOW, LANGUAGE_LIST, LANGUAGE_ADD, LANGUAGE_EDIT, LANGUAGE_DELETE,
				CENTERS_SHOW, CENTERS_LIST, CENTERS_ADD, CENTERS_EDIT, CENTERS_DELETE,
				CONTENTS_SHOW, CONTENTS_LIST, CONTENTS_ADD, CONTENTS_EDIT, CONTENTS_DELETE,
				DASHBOARD_SHOW, DASHBOARD_EDIT,
				DOCS_SHOW, DOCS_ALERT, DOCS_CENTER, DOCS_CONTENT, DOCS_DASHBOARD, DOCS_INTERFACE, DOCS_LANGUAGE, DOCS_LOG, DOCS_REPORT, DOCS_SECURITY, DOCS_STATISTIC, DOCS_TRANSACTION, DOCS_WORKFLOW,
				EMPLOYEES_LIST,
				INTERFACES_SHOW, INTERFACES_LIST, INTERFACES_ADD, INTERFACES_EDIT, INTERFACES_DELETE, INTERFACES_CONSULT,
				LOGS_SHOW,
				REPORTS_SHOW, REPORTS_ALERT, REPORTS_CENTER, REPORTS_INTERFACE, REPORTS_LOG, REPORTS_TRANSACTION, REPORTS_WORKFLOW,
				SECURITY_SHOW, SECURITY_LIST, SECURITY_ADD, SECURITY_EDIT, SECURITY_DELETE,
				STATISTICS_SHOW,
				TRANSACTIONS_SHOW, TRANSACTIONS_ACTIVE, TRANSACTIONS_LIST, TRANSACTIONS_ADD,
				WORKFLOWS_SHOW, WORKFLOWS_LIST, WORKFLOWS_ADD, WORKFLOWS_EDIT, WORKFLOWS_DELETE, WORKFLOWS_PUBLISH
		);
	}
}
