package com.mstn.scripting.core.auth.jwt;

import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.Secrets;
import com.mstn.scripting.core.models.Center;
import com.mstn.scripting.core.models.Employee;
import java.security.Principal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.HmacKey;

/**
 * Represents an authenticated user and their associated permissions. Created by
 * {@link JwtAuthenticator}.
 *
 * @author Hendrik van Huyssteen
 * @since 21 Sep 2017
 */
public class User implements Principal {

	/**
	 * Instancia de usuario que representa al sistema. Se le ha asignado
	 * lógicamente el número -1 como id del usuario del sistema.
	 */
	static public final User SYSTEM = new User(-1, "System", null,
			Arrays.asList(UserPermissions.SCRIPTING_MASTER),
			new Employee(-1, -1, 0, "System"),
			new Center(-1, "System", -1, true, null, "System"),
			"es", "localhost", new Date(), 0);

	static final Logger LOGGER = Logger.getLogger(User.class.getName());
	private final int id;
	private final String name;
	private final String role;
	private List<String> permissions = Arrays.asList();
	private final Employee employee;
	private final Center center;
	private final String language;
	private final String ip;
	private final Date logged;
	private final long expired;

	private String token;

	/**
	 *
	 * @param id ID del usuario en la tabla de empleados
	 * @param name Nombre del usuario
	 * @param role Perfil del usuario
	 * @param permissions Lista de permisos del usuario. Están disponibles en la
	 * clase {@link UserPermissions}.
	 * @param employee Registro de la vista {@code employee} correspondiente al
	 * usuario.
	 * @param center Registro de la vista {@code v_center} correspondiente al
	 * usuario.
	 * @param language Código del registro de idioma en la tabla
	 * {@code system_config}.
	 * @param ip IP desde la cual el usuario el usuario creó el token.
	 * @param logged
	 * @param expired
	 */
	public User(
			int id, String name,
			String role, List<String> permissions,
			Employee employee, Center center,
			String language, String ip, Date logged, long expired
	) {
		this.id = id;
		this.name = name;
		this.role = role;
		this.permissions = permissions;
		this.employee = employee;
		this.center = center;
		if (this.center != null) {
			this.center.setManagers(null);
			this.center.setNotes(null);
		}
		this.language = language;
		this.ip = ip;
		this.logged = logged;
		this.expired = expired;
	}

	/**
	 *
	 * @param permissions
	 * @return
	 */
	public boolean hasPermissions(String... permissions) {
		if (isMaster()) {
			return true;
		}
		if (this.permissions != null && permissions != null) {
			return this.permissions.containsAll(Arrays.asList(permissions));
		}
		return false;
	}

	/**
	 *
	 * @param permissions
	 */
	public void addPermissions(String... permissions) {
		this.permissions.addAll(Arrays.asList(permissions));
	}

	/**
	 *
	 * @param permissions
	 */
	public void removePermissions(String... permissions) {
		this.permissions.removeAll(Arrays.asList(permissions));
	}

	/**
	 *
	 * @return
	 */
	public boolean isMaster() {
		if (this.permissions != null) {
			return this.permissions.contains(UserPermissions.SCRIPTING_MASTER);
		}
		return false;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	public String generateToken() throws Exception {
		return generateToken(this);
	}

	/**
	 *
	 * @param user
	 * @return
	 * @throws Exception
	 */
	static public String generateToken(User user) throws Exception {
		// TODO: These claims would be tightened up for production
		final JwtClaims claims = new JwtClaims();
		claims.setSubject(String.valueOf(user.getId()));
		claims.setStringClaim("name", user.getName());
		claims.setStringClaim("role", user.getRole());
		claims.setStringListClaim("permissions", user.getPermissions());
		claims.setStringClaim("employee", JSON.toString(user.getEmployee()));
		claims.setStringClaim("center", JSON.toString(user.getCenter()));
		claims.setStringClaim("language", user.getLanguage());
		claims.setStringClaim("ip", user.getIp());
		claims.setStringClaim("logged", DATE.toDateTimeString(user.getLogged()));
		claims.setStringClaim("expired", String.valueOf(user.getExpired()));
		claims.setIssuedAtToNow();
		claims.setGeneratedJwtId();

//		LOGGER.info(JSON.toString(claims));
		final JsonWebSignature jws = new JsonWebSignature();
		jws.setPayload(claims.toJson());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
		jws.setKey(new HmacKey(Secrets.JWT_SECRET_KEY));
		String token = jws.getCompactSerialization();
		return token;
	}

	/**
	 *
	 * @param claims
	 * @return
	 * @throws Exception
	 */
	static public User from(JwtClaims claims) throws Exception {
		int id = Integer.parseInt(claims.getSubject());
		String name = claims.getStringClaimValue("name");
		String role = claims.getStringClaimValue("role");
		List<String> permissions = claims.getStringListClaimValue("permissions");

		String employeeStr = claims.getStringClaimValue("employee");
		Employee employee = JSON.toObject(employeeStr, Employee.class);

		String centerStr = claims.getStringClaimValue("center");
		Center center = JSON.toObject(centerStr, Center.class);

		String language = claims.getStringClaimValue("language");
		String ip = claims.getStringClaimValue("ip");
		Date logged = DATE.toDate(claims.getStringClaimValue("logged"));
		String expiredStr = claims.getStringClaimValue("expired");
		Long expired = Long.parseLong(expiredStr);
		return new User(id, name, role, permissions, employee, center, language, ip, logged, expired);
	}

	@Override
	public User clone() throws CloneNotSupportedException {
		return User.clone(this);
	}

	/**
	 *
	 * @param user
	 * @return
	 */
	static public User clone(User user) {
		return new User(
				user.getId(), user.getName(),
				user.getRole(), user.getPermissions(),
				user.getEmployee(), user.getCenter(),
				user.getLanguage(), user.getIp(),
				user.getLogged(), user.getExpired()
		).setToken(user.getToken());
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	/**
	 *
	 * @return
	 */
	public String getRole() {
		return role;
	}

	/**
	 *
	 * @return
	 */
	public List<String> getPermissions() {
		return permissions;
	}

	/**
	 *
	 * @return
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 *
	 * @return
	 */
	public Center getCenter() {
		return center;
	}

	/**
	 *
	 * @return
	 */
	public int getCenterId() {
		return center == null ? 0 : center.getId();
	}

	/**
	 *
	 * @return
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 *
	 * @return
	 */
	public String getIp() {
		return ip;
	}

	/**
	 *
	 * @return
	 */
	public Date getLogged() {
		return logged;
	}

	/**
	 *
	 * @return
	 */
	public long getExpired() {
		return expired;
	}

	/**
	 *
	 * @return
	 */
	public String getToken() {
		return token;
	}

	/**
	 *
	 * @param token
	 * @return
	 */
	public User setToken(String token) {
		this.token = token;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof User)) {
			return false;
		}

		User otherUser = (User) o;

		if (id != otherUser.id) {
			return false;
		}
		if (name != null ? !name.equals(otherUser.name) : otherUser.name != null) {
			return false;
		}
		for (int i = 0; i < permissions.size(); i++) {
			String permission = permissions.get(i);
			if (!otherUser.permissions.contains(permission)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		@SuppressWarnings("ShiftOutOfRange")
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (permissions != null ? permissions.hashCode() : 0);
		return result;
	}
}
