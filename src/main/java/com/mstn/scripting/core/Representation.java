/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Clase que envuelve las respuestas al cliente.
 *
 * @author josesuero
 * @param <T>
 */
public class Representation<T> {

	/**
	 * Código de respuesta HTTP.
	 */
	private long code;
	/**
	 * Contenido solicitado por el cliente.
	 */
	private T items;
	/**
	 * Número de registros por página en esta respuesta.
	 */
	private long pageSize;
	/**
	 * Número de la página de los registros incluidos en esta respuesta.
	 */
	private long pageNumber;
	/**
	 * Número total de registros encontrados en la base de datos.
	 */
	private long totalRows;

	/**
	 * Constructor
	 */
	public Representation() {

	}

	/**
	 *
	 * @param code
	 */
	public Representation(long code) {
		this.code = code;
	}

	/**
	 *
	 * @param code
	 * @param items
	 */
	public Representation(long code, T items) {
		this.code = code;
		this.items = items;
	}

	/**
	 *
	 * @param code
	 * @param items
	 * @param pageSize
	 * @param pageNumber
	 */
	public Representation(long code, T items, long pageSize, long pageNumber) {
		this.code = code;
		this.items = items;
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
	}

	/**
	 *
	 * @param code
	 * @param items
	 * @param pageSize
	 * @param pageNumber
	 * @param totalRows
	 */
	public Representation(long code, T items, long pageSize, long pageNumber, long totalRows) {
		this.code = code;
		this.items = items;
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
		this.totalRows = totalRows;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty
	public long getCode() {
		return code;
	}

	/**
	 *
	 * @param code
	 */
	public void setCode(long code) {
		this.code = code;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty
	public T getItems() {
		return items;
	}

	/**
	 *
	 * @param items
	 */
	public void setItems(T items) {
		this.items = items;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty
	public long getPageSize() {
		return pageSize;
	}

	/**
	 *
	 * @param pageSize
	 */
	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty
	public long getPageNumber() {
		return pageNumber;
	}

	/**
	 *
	 * @param pageNumber
	 */
	public void setPageNumber(long pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty
	public long getTotalRows() {
		return totalRows;
	}

	/**
	 *
	 * @param totalRows
	 */
	public void setTotalRows(long totalRows) {
		this.totalRows = totalRows;
	}

}
