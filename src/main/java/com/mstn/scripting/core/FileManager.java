/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Clase creada para crear y mover archivos e imágenes adjuntos a los
 * contenidos.
 *
 * @author amatos
 */
public class FileManager {

	/**
	 * Guarda los datos dentro del stream indicado como un archivo en la ruta
	 * indicada.
	 *
	 * @param inputStream Stream que contiene los datos que se desean guardar
	 * @param filePath Ruta del archivo en que se guardarán los datos.
	 * @throws Exception Si algún error ocurre en el proceso.
	 */
	static public void saveFile(InputStream inputStream, String filePath) throws Exception {
		int read;
		final int BUFFER_LENGTH = 1024;
		final byte[] buffer = new byte[BUFFER_LENGTH];
		OutputStream out = new FileOutputStream(new File(filePath));
		while ((read = inputStream.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
		out.flush();
		out.close();
	}

	/**
	 * Mueve el archivo encontrado en la ruta indicada a otra.
	 *
	 * @param fromPath Ruta actual del archivo.
	 * @param toPath Ruta nueva a la que se moverá el archivo.
	 * @return {@code true} si el proceso fue éxitoso; de lo contrario
	 * {@code false}.
	 * @throws Exception Si ocurre algún error en el proceso.
	 * @see File#renameTo(java.io.File).
	 */
	static public boolean moveFile(String fromPath, String toPath) throws Exception {
		return new File(fromPath).renameTo(new File(toPath));
	}
}
