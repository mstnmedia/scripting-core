/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.apache.velocity.tools.ConversionUtils;
import org.apache.velocity.tools.generic.AlternatorTool;
import org.apache.velocity.tools.generic.ClassTool;
import org.apache.velocity.tools.generic.ComparisonDateTool;
import org.apache.velocity.tools.generic.ContextTool;
import org.apache.velocity.tools.generic.ConversionTool;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.DisplayTool;
import org.apache.velocity.tools.generic.EscapeTool;
import org.apache.velocity.tools.generic.LinkTool;
import org.apache.velocity.tools.generic.LoopTool;
import org.apache.velocity.tools.generic.MarkupTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.apache.velocity.tools.generic.RenderTool;
import org.apache.velocity.tools.generic.ResourceTool;
import org.apache.velocity.tools.generic.SortTool;
import org.apache.velocity.tools.generic.XmlTool;
import org.apache.velocity.util.ExceptionUtils;
import org.apache.velocity.util.StringUtils;
import org.xwiki.velocity.tools.CollectionsTool;
import org.xwiki.velocity.tools.JSONTool;
import org.xwiki.velocity.tools.RegexTool;
import org.xwiki.velocity.tools.URLTool;
import org.xwiki.velocity.tools.nio.NIOTool;

/**
 * Clase para interpolar textos con Apache Velocity.
 *
 * @author amatos
 */
public class Velocity {

	/**
	 * Obtiene el código VTL que declara el macro #tableTemplate.
	 */
	static String getTableMacro() {
		return "#set($_Integer = 0)\n"
				+ "#macro(tableTemplate $columns $prefix)\n"
				+ "<div class=\"table-responsive\">\n"
				+ " <table class=\"table\">\n"
				+ "  <thead>\n"
				+ "   <tr>\n"
				+ "    #foreach($column in $columns)\n"
				+ "    <th>$column.label</th>\n"
				+ "    #end\n"
				+ "   </tr>\n"
				+ "  </thead>\n"
				+ "  <tbody>\n"
				+ "   #set($sizeName='$' + $prefix + '_size')\n"
				+ "   #set($strCount=\"#evaluate($sizeName)\")\n"
				+ "   #set($count = $_Integer.parseInt($strCount))\n"
				+ "   #if($count > 0)\n"
				+ "    #set($count = $count - 1)\n"
				+ "    #foreach($i in [0..$count])\n"
				+ "     <tr>\n"
				+ "   	 #foreach($column in $columns)\n"
				+ "   	  #if (!$column.children || $column.children.size() == 0)\n"
				+ "   	   #set($name = '$' + $prefix + \"$i\" + '_' + \"$column.name\")\n"
				+ "   	   #if($column.type == 'datetime')\n"
				+ "         #set($strDate = \"#evaluate($name)\")\n"
				+ "         #set($date = $dateTool.toDate($Utils.coalesce($column.parseFormat, \"yyyy-MM-dd'T'HH:mm:ss\"), $strDate))\n"
				+ "         <td>$dateTool.format($Utils.coalesce($column.format, 'short'), $date)</td>\n"
				+ "   	   #else\n"
				+ "   	    <td>#evaluate($name)</td>\n"
				+ "   	   #end\n"
				+ "   	  #else\n"
				+ "        <td>\n"
				+ "   		#set($newPrefix = $prefix + \"$i\" + '_' + \"$column.name\")\n"
				+ "   		#tableTemplate($column.children $newPrefix)\n"
				+ "   	   </td>\n"
				+ "   	  #end\n"
				+ "   	 #end\n"
				+ "     </tr>\n"
				+ "    #end\n"
				+ "   #end\n"
				+ "  </tbody>\n"
				+ " </table>\n"
				+ "</div>\n"
				+ "#end\n";
	}

	/**
	 * Agrega herramientas de Velocity al contexto para dejarlas disponibles.
	 *
	 * @param values Map que contiene los valores que estarán disponibles en el
	 * contexto de Velocity.
	 * @return Un contexto de Velocity.
	 * @throws Exception
	 */
	static public VelocityContext getVelocityContext(Map<String, Object> values) throws Exception {
		VelocityContext vc = new VelocityContext(values);
		//http://velocity.apache.org/tools/devel/generic.html#tools
		vc.put("defaultTimeZone", TimeZone.getDefault());
		vc.put("defaultLocale", Locale.getDefault());
		vc.put("alternatorTool", new AlternatorTool());
		vc.put("classTool", new ClassTool());
		vc.put("comparisonDateTool", new ComparisonDateTool());
		vc.put("contextTool", new ContextTool());
		vc.put("conversionTool", new ConversionTool());
		vc.put("dateTool", new DateTool());
		vc.put("displayTool", new DisplayTool());
		vc.put("escapeTool", new EscapeTool());
		vc.put("linkTool", new LinkTool());
		vc.put("loopTool", new LoopTool());
		vc.put("markupTool", new MarkupTool());
		vc.put("mathTool", new MathTool());
		vc.put("numberTool", new NumberTool());
		vc.put("renderTool", new RenderTool());
		vc.put("resourceTool", new ResourceTool());
		vc.put("sortTool", new SortTool());
		vc.put("xmlTool", new XmlTool());
		vc.put("collectionsTool", CollectionsTool.class);
		vc.put("stringTool", new StringUtils());
		vc.put("conversionUtils", ConversionUtils.class);
		vc.put("exceptionTool", new ExceptionUtils());
		vc.put("jsonTool", new JSONTool());
		vc.put("regexTool", new RegexTool());
		vc.put("urlTool", new URLTool());
		vc.put("nioTool", new NIOTool());
		vc.put("Utils", Utils.class);
		return vc;
	}

	/**
	 * Obtiene el resultado de interpretar como VTL el texto de la plantilla
	 * indicada.
	 *
	 * @param template Plantilla que generará el texto resultante.
	 * @param values Map que contiene los valores que estarán disponibles en el
	 * contexto de Velocity.
	 * @return El texto resultante de la interpretación.
	 * @throws Exception Si la plantilla tiene un error de sintaxis u ocurre un
	 * error en el proceso de interpretación.
	 */
	static public String interpolate(String template, Map<String, Object> values) throws Exception {
		String vtl = getTableMacro() + Utils.coalesce(template, "");
		StringReader sr = new StringReader(vtl);
		RuntimeServices rs = RuntimeSingleton.getRuntimeServices();
		SimpleNode sn = rs.parse(sr, "Template");

		VelocityContext vc = getVelocityContext(values);
		StringWriter sw = new StringWriter();
		Template t = new Template();
		t.setRuntimeServices(rs);
		t.setData(sn);
		t.initDocument();
		t.merge(vc, sw);
		return sw.toString();
	}

}
