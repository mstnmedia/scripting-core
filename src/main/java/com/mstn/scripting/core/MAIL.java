/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import com.mstn.scripting.core.models.InterfaceConfigs;
import java.util.List;
import javax.activation.FileDataSource;
import javax.mail.Message;
import org.simplejavamail.email.Email;
import org.simplejavamail.mailer.Mailer;

/**
 * Clase que envia correos.
 *
 * @author amatos
 */
public class MAIL {

	/**
	 * Envía un correo sin adjuntos.
	 *
	 * @param to Un correo electrónico válido o varios separados por coma.
	 * @param subject El asunto del correo.
	 * @param content El contenido del correo. Puede tener el formato HTML.
	 */
	static public void send(String to, String subject, String content) {
		send(to, subject, content, null);
	}

	/**
	 * Envía un correo con adjuntos.
	 *
	 * @param to Un correo electrónico válido o varios separados por coma.
	 * @param subject El asunto del correo.
	 * @param content El contenido del correo. Puede tener el formato HTML.
	 * @param attachmentNames Lista separada por coma de los nombres de los
	 * archivos que se adjuntarán. Estos archivos deben estar localizados en la
	 * carpeta {@code contentsFolder} especificada en la configuración del
	 * microservicio de Interfaces.
	 */
	static public void send(String to, String subject, String content, String attachmentNames) {
		Mailer mailer = InterfaceConfigs.mailer;
		Email email = new Email();
		String mailFromName = InterfaceConfigs.get("mailFromName", "");
		String mailFromEmail = InterfaceConfigs.get("mailFromEmail", "scripting@claro.com.do");
		email.setFromAddress(mailFromName, mailFromEmail);
		List<String> TOs = Utils.splitAsList(to);
		for (int i = 0; i < TOs.size(); i++) {
			String iTO = TOs.get(i).trim();
			email.addRecipient("", iTO, Message.RecipientType.TO);
		}
		email.setSubject(subject);
		email.setTextHTML(content);

		if (Utils.stringNonNullOrEmpty(attachmentNames)) {
			String[] attachments = attachmentNames.split(",");
			for (String attachment : attachments) {
				String filePath = InterfaceConfigs.microserviceConfig.getContentsFolder()
						+ attachment;
				FileDataSource fileDS = new FileDataSource(filePath);
				email.addAttachment(attachment, fileDS);
			}
		}
		mailer.sendMail(email);
	}
}
