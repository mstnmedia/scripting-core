/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author amatos
 */
public class RESTTemplate {

//	static public List<MappingJackson2HttpMessageConverter> messageConverters = new ArrayList(Arrays.asList(
//			new MappingJackson2HttpMessageConverter(JSON.MAPPER)
//	));
	// TODO: Agregar configuración de Timeout
	// TODO: Agregar configuración de JsonMapper
	static RestTemplate restTemplate = new RestTemplate();

	static public <T> ResponseEntity<T> get(String url, Class<T> responseType) {
		Request request = new Request(url, HttpMethod.GET, null);
		return request(request, responseType);
	}

	static public <T> ResponseEntity<T> get(String url, HttpHeaders headers, HashMap<String, Object> params, HashMap<String, Object> queryString, Class<T> responseType) {
		Request request = new Request(url, HttpMethod.GET, null, headers, params, queryString);
		return request(request, responseType);
	}

	static public <T> ResponseEntity<T> post(String url, Object data, Class<T> responseType) {
		Request request = new Request(url, HttpMethod.POST, data);
		return request(request, responseType);
	}

	static public <T> ResponseEntity<T> post(String url, Object data, HttpHeaders headers, Class<T> responseType) {
		Request request = new Request(url, HttpMethod.POST, data);
		request.headers = headers;
		return request(request, responseType);
	}

	static public <T> ResponseEntity<T> post(String url, Object data, HttpHeaders headers, HashMap<String, Object> params, HashMap<String, Object> queryString, Class<T> responseType) {
		Request request = new Request(url, HttpMethod.POST, data, headers, params, queryString);
		return request(request, responseType);
	}

	static public <T> ResponseEntity<T> request(Request request, Class<T> responseType) {
		try {
			HttpEntity entity;
			HttpHeaders headers = request.headers;
			switch (request.method) {
				case POST:
					if (headers.getContentType() == null) {
						headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON_UTF8);
					}
					entity = new HttpEntity<>(request.data, headers);
					break;
				case PUT:
					if (headers.getContentType() == null) {
						headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON_UTF8);
					}
					entity = new HttpEntity<>(request.data, headers);
					break;
				case DELETE:
				case GET:
				default:
					entity = new HttpEntity<>(headers);
					break;
			}
			UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(request.url);
			urlBuilder.uriVariables(request.params);
			for (Map.Entry<String, Object> entry : request.queryString.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue() == null ? "null" : entry.getValue().toString();
				urlBuilder.queryParam(key, value);
			}
			String url = urlBuilder.buildAndExpand(request.params).toUriString();
			ResponseEntity<T> response = restTemplate.exchange(url, request.method, entity, responseType);
			return response;
		} catch (Exception ex) {
			throw ex;
		}
	}

	static public class Request {

		public String url;
		public HttpMethod method;
		public Object data;
		public Class responseType = String.class;
		public HttpHeaders headers = new HttpHeaders();
		public HashMap<String, Object> params = new HashMap();
		public HashMap<String, Object> queryString = new HashMap();

		public Request(String url, HttpMethod method, Object data) {
			this(url, method, data, String.class);
		}

		public Request(String url, HttpMethod method, Object data, Class responseType) {
			this.url = url;
			this.method = method;
			this.data = data;
			this.responseType = responseType;
		}

		public Request(String url, HttpMethod method, Object data, HttpHeaders headers, HashMap<String, Object> params, HashMap<String, Object> queryString) {
			this.url = url;
			this.method = method;
			this.data = null;
			this.headers = headers;
			this.params = params;
			this.queryString = queryString;
		}

	}

	static String diagnosticoUrl = "http://nttappsweb0010/sacsapi/api";

	static public String getTokenDiagnostico() {
		Login request = new Login("SCRIPTING", "Dy0t8W3TcNekrD+");
		ResponseEntity<AuthenticateResponse> response = restTemplate.postForEntity(diagnosticoUrl + "/Authenticate", request, AuthenticateResponse.class);
		AuthenticateResponse responseBody = response.getBody();
		String token = responseBody.Value.Token;
		return token;
	}

	static public class AuthenticateResponse {

		public AuthenticateValueResponse Value;
		public boolean Succeeded;
		public String Message;
		public Object Errors;
	}

	static public class AuthenticateValueResponse {

		public String Token;
		public String RefreshToken;
	}

	static public String getDiagnostico() {
		String url = diagnosticoUrl + "/Diagnostico/GetDiagnosticoByPhone?phone=8095914221&user=Josermando";
		Request request = new Request(url, HttpMethod.GET, null, String.class);
		String token = getTokenDiagnostico();
		request.headers.setBearerAuth(token);
		ResponseEntity<String> response = request(request, String.class);
		return JSON.toString(response.getBody());
	}

	static public class GetDiagnosticoResponse {

		public int Status;
		public String Message;
		public String Value;
	}

	static class Login {

		public String Username;
		public String Password;

		public Login(String Username, String Password) {
			this.Username = Username;
			this.Password = Password;
		}
	}

}
