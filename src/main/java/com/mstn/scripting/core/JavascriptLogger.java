/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author amatos
 */
public class JavascriptLogger {

	static final Logger logger = Logger.getLogger(JavascriptLogger.class.getName());

	/**
	 *
	 * @param <T>
	 * @param values
	 * @throws Exception
	 */
	static public <T> void log(T... values) throws Exception {
		for (T value : values) {
			logger.log(Level.INFO, JSON.toString(value));
		}
	}

	/**
	 *
	 * @param <T>
	 * @param ex
	 * @throws Exception
	 */
	public <T> void error(Object ex) throws Exception {
		logger.log(Level.SEVERE, JSON.toString(ex));
	}
}
