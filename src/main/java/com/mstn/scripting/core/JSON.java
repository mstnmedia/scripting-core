/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.time.DateUtils;

/**
 * Clase que realiza funciones comunes para trabajar con nodos JSON.
 *
 * @author amatos
 */
public class JSON {

	/**
	 * Mapper con que se trabajan los nodos hecho público para permitir
	 * modificación de su configuración en los microservicios.
	 */
	static public final ObjectMapper MAPPER = new ObjectMapper();

	static {
		MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		MAPPER.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
	}

	/**
	 * Obtiene un nuevo objeto JSON.
	 *
	 * @return Un objeto JSON.
	 */
	public static ObjectNode newObjectNode() {
		return MAPPER.createObjectNode();
	}

	/**
	 * Obtiene un nuevo arreglo JSON.
	 *
	 * @return Un arreglo JSON.
	 */
	public static ArrayNode newArrayNode() {
		return MAPPER.createArrayNode();
	}

	/**
	 * Obtiene un nodo objeto JSON.
	 *
	 * @return Un nodo JSON.
	 */
	public static JsonNode newJsonNode() {
		return newObjectNode();
	}

	/**
	 * Clona el objeto indicado serializándolo y creando la nueva instancia
	 * mediante deserialización.
	 *
	 * @param <T> Clase que instancia {@code object}
	 * @param object Objeto que será clonado.
	 * @return La nueva instancia que ha sido obtenida.
	 * @throws IOException Si ocurre un error en el proceso.
	 */
	public static <T> T clone(T object) throws IOException {
		if (object == null) {
			return null;
		}
		return toObject(toString(object), (Class<T>) object.getClass());
	}

	/**
	 * Obtiene un arreglo JSON desde un string.
	 *
	 * @param string Texto que puede ser convertido a arreglo JSON.
	 * @return El arreglo JSON obtenido del texto.
	 * @throws IOException Si el texto no está bien formateado o si ocurre un
	 * error en el proceso.
	 */
	static public ArrayNode toArrayNode(String string) throws IOException {
		return (ArrayNode) MAPPER.readTree(string);
	}

	/**
	 * Obtiene una lista Java de objetos de la clase indicada desde un string.
	 *
	 * @param <T> Clase que define a los objetos de la lista.
	 * @param jsonArray Texto que puede ser convertido a la lista Java.
	 * @param type Clase que define a los objetos de la lista.
	 * @return Una lista Java obtenida del texto.
	 * @throws IOException Si el texto no está bien formateado o si ocurre un
	 * error en el proceso.
	 */
	static public <T> List<T> toList(String jsonArray, Class<T> type) throws IOException {
		return JSON.MAPPER.readValue(jsonArray, TypeFactory.defaultInstance().constructCollectionType(List.class, type));
	}

	/**
	 * Obtiene un nodo JSON desde un string.
	 *
	 * @param string Texto que puede ser interpretado como JSON.
	 * @return Un nodo JSON obtenido del texto.
	 * @throws IOException Si el texto no está bien formateado o si ocurre un
	 * error en el proceso.
	 */
	static public JsonNode toJsonNode(String string) throws IOException {
		return MAPPER.readTree(string);
	}

	/**
	 * Obtiene un nodo JSON desde un objeto Java.
	 *
	 * @param object Objeto del que se generará el nodo JSON.
	 * @return La representación del objeto indicado como un nodo JSON.
	 */
	static public JsonNode toJsonNode(Object object) {
		return (JsonNode) MAPPER.valueToTree(object);
	}

	/**
	 * Obtiene un objeto JSON desde un string.
	 *
	 * @param string Texto que puede ser interpretado como objeto JSON.
	 * @return Un objeto JSON obtenido del texto.
	 * @throws IOException Si el texto no está bien formateado o si ocurre un
	 * error en el proceso.
	 */
	static public ObjectNode toNode(String string) throws IOException {
		return (ObjectNode) MAPPER.readTree(string);
	}

	/**
	 * Obtiene un objeto JSON desde un objeto Java.
	 *
	 * @param object Objeto del que se generará el objeto JSON.
	 * @return Un objeto JSON que representa al objeto Java.
	 */
	static public ObjectNode toNode(Object object) {
		return (ObjectNode) MAPPER.valueToTree(object);
	}

	/**
	 * Obtiene la serialización JSON del objeto indicado.
	 *
	 * @param object Objeto que se quiere serializar.
	 * @return Texto que representa al objeto indicado.
	 */
	static public String toString(Object object) {
		return toString(toJsonNode(object));
	}

	/**
	 * Obtiene la serialización JSON del nodo indicado.
	 *
	 * @param node Nodo que se quiere serializar.
	 * @return Texto que representa al nodo indicado.
	 */
	static public String toString(JsonNode node) {
		return node == null ? "null" : node.toString();
	}

	/**
	 * Obtiene un objeto Java desde un string como instancia de la clase
	 * indicada.
	 *
	 * @param <T> Clase que representa el objeto que se interpretará.
	 * @param json Texto que puede ser interpretado como JSON.
	 * @param valueType Clase que representa el objeto que se interpretará.
	 * @return El objeto que ha sido interpretado desde el texto.
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	static public <T> T toObject(String json, Class<T> valueType) throws JsonProcessingException, IOException {
		return MAPPER.readValue(json, valueType);
	}

	/**
	 * Obtiene un objeto Java desde un nodo JSON como instancia de la clase
	 * indicada.
	 *
	 * @param <T> Clase que representa el objeto que se interpretará.
	 * @param node Nodo JSON que representa el objeto deseado.
	 * @param valueType Clase que representa el objeto que se interpretará.
	 * @return El objeto que ha sido interpretado desde el nodo.
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	static public <T> T toObject(JsonNode node, Class<T> valueType) throws JsonProcessingException, IOException {
		String json = toString(node);
		return MAPPER.readValue(json, valueType);
	}

	/**
	 * Indica si un nodo JSON no tiene propiedades.
	 *
	 * @param form Nodo JSON que se desea evaluar.
	 * @return {@code true} si el nodo no tiene propiedades; {@code false} si
	 * tiene al menos una.
	 */
	static public boolean isEmpty(JsonNode form) {
		return form.size() == 0;
	}

	/**
	 * Obtiene el valor de la propiedad indicada en el nodo JSON como número
	 * entero, si tiene la propiedad; de lo contrario obtiene cero.
	 *
	 * @param form
	 * @param fieldName
	 * @return
	 */
	static public int getInt(JsonNode form, String fieldName) {
		return getInt(form, fieldName, 0);
	}

	/**
	 * Obtiene el valor de la propiedad indicada en el nodo JSON como número
	 * entero, si tiene la propiedad; de lo contrario obtiene el valor por
	 * defecto indicado.
	 *
	 * @param form
	 * @param fieldName
	 * @param defaultValue
	 * @return
	 */
	static public int getInt(JsonNode form, String fieldName, int defaultValue) {
		return form.hasNonNull(fieldName) ? form.get(fieldName).asInt(defaultValue) : defaultValue;
	}

	/**
	 * Obtiene el valor la propiedad indicada en el nodo JSON, si tiene la
	 * propiedad; de lo contrario obtiene un texto vacío.
	 *
	 * @param form
	 * @param fieldName
	 * @return
	 */
	static public String getString(JsonNode form, String fieldName) {
		return getString(form, fieldName, "");
	}

	/**
	 * Obtiene el valor la propiedad indicada en el nodo JSON, si tiene la
	 * propiedad; de lo contrario obtiene el valor por defecto indicado.
	 *
	 * @param form
	 * @param fieldName
	 * @param defaultValue
	 * @return
	 */
	static public String getString(JsonNode form, String fieldName, String defaultValue) {
		if (form.hasNonNull(fieldName)) {
			JsonNode node = form.get(fieldName);
			String value = node.asText(defaultValue);
			if (!node.isValueNode()) {
				value = node.toString();
			}
			return Utils.stringNonNullOrEmpty(value) ? value : defaultValue;
		}
		return defaultValue;
	}

	/**
	 * Indica si el nodo JSON tiene una propiedad con el nombre espedificado y
	 * con valor no nulo ni vacío.
	 *
	 * @param form
	 * @param fieldName
	 * @return
	 */
	static public boolean hasValue(JsonNode form, String fieldName) {
		String fieldValue = getString(form, fieldName, "");
		return Utils.stringNonNullOrEmpty(fieldValue);
	}

	/**
	 * Clase que se registra para serializar las propiedades tipo fecha de las
	 * clases que representan los objetos de la base de datos para que no sean
	 * afectadas por la zona horaria cuando se envían al cliente y sean enviadas
	 * en formato ISO.
	 */
	public static class DateSerializer extends JsonSerializer<Date> {

		private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

		/**
		 *
		 * @param date
		 * @param gen
		 * @param provider
		 * @throws IOException
		 */
		@Override
		public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException {
			String formatted = FORMATTER.format(date);
			gen.writeString(formatted);
		}
	}

	/**
	 * Clase que se registra para serializar las propiedades tipo fecha y hora
	 * de las clases que representan los objetos de la base de datos para que no
	 * sean afectadas por la zona horaria cuando se envían al cliente y sean
	 * enviadas en formato ISO.
	 */
	public static class DateTimeSerializer extends JsonSerializer<Date> {

		private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

		/**
		 *
		 * @param date
		 * @param gen
		 * @param provider
		 * @throws IOException
		 */
		@Override
		public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException {
			String formatted = FORMATTER.format(date);
			gen.writeString(formatted);
		}

	}

	/**
	 * Clase que se registra para deserializar las propiedades tipo fecha de las
	 * clases que representan los objetos de la base de datos para que no sean
	 * afectadas por la zona horaria cuando se envían al cliente y sean enviadas
	 * en formato ISO.
	 */
	public static class DateDeserializer extends JsonDeserializer<Date> {

		private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

		/**
		 *
		 * @param jp
		 * @param ctxt
		 * @return
		 * @throws IOException
		 * @throws JsonProcessingException
		 */
		@Override
		public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
			String value = jp.getText();
			try {
				Date date = FORMATTER.parse(value);
				return date;
			} catch (Exception ex) {
				Utils.logException(DateDeserializer.class, "Value " + value + "can't be parsed to Date", ex);
				return null;
			}
		}
	}

	/**
	 * Clase que se registra para deserializar las propiedades tipo fecha y hora
	 * de las clases que representan los objetos de la base de datos para que no
	 * sean afectadas por la zona horaria cuando se envían al cliente y sean
	 * enviadas en formato ISO.
	 */
	public static class DateTimeDeserializer extends JsonDeserializer<Date> {

		/**
		 *
		 * @param jp
		 * @param ctxt
		 * @return
		 * @throws IOException
		 * @throws JsonProcessingException
		 */
		@Override
		public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
			String value = jp.getText();
			try {
				Date date = DateUtils.parseDateStrictly(value, DATE.parsePatterns);
				return date;
			} catch (Exception ex) {
				Utils.logException(DateDeserializer.class, "Value " + value + "can't be parsed to Date", ex);
				return null;
			}
		}
	}
}
