package com.mstn.scripting.core;

/**
 *
 * @author MSTN Media
 */
public class MediaType extends javax.ws.rs.core.MediaType {

	/**
	 * MediaType para contenidos JSON, espeficicando el charset UTF-8.
	 */
	public final static String APPLICATION_JSON_UTF_8 = "application/json; charset=UTF-8";
}
