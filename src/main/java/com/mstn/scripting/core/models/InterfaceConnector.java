/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.mstn.scripting.core.auth.jwt.User;

/**
 * @author amatos
 */
public interface InterfaceConnector {

	/**
	 *
	 * @return @throws Exception
	 */
	public boolean test() throws Exception;

	/**
	 *
	 * @return
	 */
	public Interface getInterface();

	/**
	 *
	 * @return @throws Exception
	 */
	public Interface getInterfaceWithChildren() throws Exception;

	/**
	 *
	 * @return
	 */
	public Interface getInterfaceWithFields();

	/**
	 *
	 * @return @throws Exception
	 */
	public Interface getInterfaceWithResults() throws Exception;

	/**
	 *
	 * @param payload
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) throws Exception;
}
