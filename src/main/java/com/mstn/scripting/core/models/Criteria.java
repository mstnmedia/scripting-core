/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import java.util.List;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author josesuero
 */
public class Criteria {

	private int id;
	private int id_center;
	@NotEmpty
	private String name;
	private String center_name;

	private List<Criteria_Condition> conditions;

	/**
	 *
	 */
	public Criteria() {

	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param center_name
	 * @param name
	 */
	public Criteria(int id, int id_center, String center_name, String name) {
		this.id = id;
		this.id_center = id_center;
		this.center_name = center_name;
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public List<Criteria_Condition> getConditions() {
		return conditions;
	}

	/**
	 *
	 * @param conditions
	 */
	public void setConditions(List<Criteria_Condition> conditions) {
		this.conditions = conditions;
	}

}
