package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.JSON;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author amatos
 */
public class Alert {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected String tags;

	/**
	 *
	 */
	protected String note;

	/**
	 *
	 */
	protected Date docdate;

	/**
	 *
	 */
	protected Date last_activation;

	/**
	 *
	 */
	protected int quantity;

	/**
	 *
	 */
	protected int time_unit;

	/**
	 *
	 */
	protected int time_quantity;

	/**
	 *
	 */
	protected int active;

	/**
	 *
	 */
	protected String emails;

	/**
	 *
	 */
	protected String email_subject;

	/**
	 *
	 */
	protected String email_text;

	/**
	 *
	 */
	protected String center_name;

	/**
	 *
	 */
	protected List<Alert_Column> columns = new ArrayList();

	/**
	 *
	 */
	protected List<Alert_Criteria> criterias = new ArrayList();

	/**
	 *
	 */
	public Alert() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param name
	 * @param tags
	 * @param note
	 * @param docdate
	 * @param last_activation
	 * @param quantity
	 * @param time_unit
	 * @param time_quantity
	 * @param active
	 * @param emails
	 * @param email_subject
	 * @param email_text
	 * @param center_name
	 */
	public Alert(int id, int id_center, String name, String tags,
			String note, Date docdate, Date last_activation,
			int quantity, int time_unit, int time_quantity,
			int active, String emails, String email_subject, String email_text,
			String center_name) {
		this.id = id;
		this.id_center = id_center;
		this.name = name;
		this.tags = tags;
		this.note = note;
		this.docdate = docdate;
		this.last_activation = last_activation;
		this.quantity = quantity;
		this.time_unit = time_unit;
		this.time_quantity = time_quantity;
		this.active = active;
		this.emails = emails;
		this.email_subject = email_subject;
		this.email_text = email_text;
		this.center_name = center_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getTags() {
		return tags;
	}

	/**
	 *
	 * @param tags
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 *
	 * @return
	 */
	public String getNote() {
		return note;
	}

	/**
	 *
	 * @param note
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getDocdate() {
		return docdate;
	}

	/**
	 *
	 * @param docdate
	 */
	public void setDocdate(Date docdate) {
		this.docdate = docdate;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getLast_activation() {
		return last_activation;
	}

	/**
	 *
	 * @param last_activation
	 */
	public void setLast_activation(Date last_activation) {
		this.last_activation = last_activation;
	}

	/**
	 *
	 * @return
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 *
	 * @param quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 *
	 * @return
	 */
	public int getTime_unit() {
		return time_unit;
	}

	/**
	 *
	 * @param time_unit
	 */
	public void setTime_unit(int time_unit) {
		this.time_unit = time_unit;
	}

	/**
	 *
	 * @return
	 */
	public int getTime_quantity() {
		return time_quantity;
	}

	/**
	 *
	 * @param time_quantity
	 */
	public void setTime_quantity(int time_quantity) {
		this.time_quantity = time_quantity;
	}

	/**
	 *
	 * @return
	 */
	public int getActive() {
		return active;
	}

	/**
	 *
	 * @param active
	 */
	public void setActive(int active) {
		this.active = active;
	}

	/**
	 *
	 * @return
	 */
	public String getEmails() {
		return emails;
	}

	/**
	 *
	 * @param emails
	 */
	public void setEmails(String emails) {
		this.emails = emails;
	}

	/**
	 *
	 * @return
	 */
	public String getEmail_subject() {
		return email_subject;
	}

	/**
	 *
	 * @param email_subject
	 */
	public void setEmail_subject(String email_subject) {
		this.email_subject = email_subject;
	}

	/**
	 *
	 * @return
	 */
	public String getEmail_text() {
		return email_text;
	}

	/**
	 *
	 * @param email_text
	 */
	public void setEmail_text(String email_text) {
		this.email_text = email_text;
	}

	/**
	 *
	 * @return
	 */
	public List<Alert_Column> getColumns() {
		return columns;
	}

	/**
	 *
	 * @param columns
	 */
	public void setColumns(List<Alert_Column> columns) {
		this.columns = columns;
	}

	/**
	 *
	 * @return
	 */
	public List<Alert_Criteria> getCriterias() {
		return criterias;
	}

	/**
	 *
	 * @param criterias
	 */
	public void setCriterias(List<Alert_Criteria> criterias) {
		this.criterias = criterias;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

}
