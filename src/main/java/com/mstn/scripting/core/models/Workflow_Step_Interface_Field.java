package com.mstn.scripting.core.models;

/**
 *
 * @author amatos
 */
public class Workflow_Step_Interface_Field {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_workflow_step;

	/**
	 *
	 */
	protected int id_interface;

	/**
	 *
	 */
	protected String id_interface_field;

	/**
	 *
	 */
	protected String id_interface_source;

	/**
	 *
	 */
	protected String id_interface_field_source;

	/**
	 *
	 */
	public Workflow_Step_Interface_Field() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_workflow_step
	 * @param id_interface
	 * @param id_interface_field
	 * @param id_interface_source
	 * @param id_interface_field_source
	 */
	public Workflow_Step_Interface_Field(int id, int id_center, int id_workflow_step, int id_interface, String id_interface_field, String id_interface_source, String id_interface_field_source) {
		this.id = id;
		this.id_center = id_center;
		this.id_workflow_step = id_workflow_step;
		this.id_interface = id_interface;
		this.id_interface_field = id_interface_field;
		this.id_interface_source = id_interface_source;
		this.id_interface_field_source = id_interface_field_source;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_step() {
		return id_workflow_step;
	}

	/**
	 *
	 * @param id_workflow_step
	 */
	public void setId_workflow_step(int id_workflow_step) {
		this.id_workflow_step = id_workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public int getId_interface() {
		return id_interface;
	}

	/**
	 *
	 * @param id_interface
	 */
	public void setId_interface(int id_interface) {
		this.id_interface = id_interface;
	}

	/**
	 *
	 * @return
	 */
	public String getId_interface_field() {
		return id_interface_field;
	}

	/**
	 *
	 * @param id_interface_field
	 */
	public void setId_interface_field(String id_interface_field) {
		this.id_interface_field = id_interface_field;
	}

	/**
	 *
	 * @return
	 */
	public String getId_interface_source() {
		return id_interface_source;
	}

	/**
	 *
	 * @param id_interface_source
	 */
	public void setId_interface_source(String id_interface_source) {
		this.id_interface_source = id_interface_source;
	}

	/**
	 *
	 * @return
	 */
	public String getId_interface_field_source() {
		return id_interface_field_source;
	}

	/**
	 *
	 * @param id_interface_field_source
	 */
	public void setId_interface_field_source(String id_interface_field_source) {
		this.id_interface_field_source = id_interface_field_source;
	}

}
