/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.HTTP;
import com.mstn.scripting.core.JSON;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.client.HttpClient;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author josesuero
 */
public class Workflow {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected String center_name;

	/**
	 *
	 */
	@NotEmpty
	protected String name;

	/**
	 *
	 */
	protected String tags;

	/**
	 *
	 */
	protected int main;

	/**
	 *
	 */
	protected boolean has_changes;

	/**
	 *
	 */
	protected boolean deleted;

	private int version_id;
	private String version_id_version;
	@JsonDeserialize(using = JSON.DateTimeDeserializer.class)
	private Date version_docdate;
	private int version_active;
	private String version_notes;

	private List<Workflow_Criteria> workflow_criteria;
	private List<Workflow_Step> workflow_step;

	/**
	 *
	 */
	public Workflow() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param center_name
	 * @param name
	 * @param tags
	 * @param main
	 * @param has_changes
	 * @param deleted
	 * @param version_id
	 * @param version_id_version
	 * @param version_docdate
	 * @param version_active
	 * @param version_notes
	 */
	public Workflow(int id, int id_center, String center_name,
			String name, String tags, int main, boolean has_changes, boolean deleted,
			int version_id, String version_id_version, Date version_docdate,
			int version_active, String version_notes) {
		this.id = id;
		this.id_center = id_center;
		this.center_name = center_name;
		this.name = name;
		this.tags = tags;
		this.main = main;
		this.has_changes = has_changes;
		this.deleted = deleted;

		this.version_id = version_id;
		this.version_id_version = version_id_version;
		this.version_docdate = version_docdate;
		this.version_active = version_active;
		this.version_notes = version_notes;
	}

	/**
	 *
	 * @param id_workflow
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Workflow getActive(int id_workflow, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(id_workflow));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/workflows/workflow/active",
				strPayload,
				bearerToken
		);
		Logger.getLogger(Workflow.class.getName()).log(Level.INFO, "getActiveWorkflow\n{0}", strResponse);
		Logger.getLogger(Workflow.class.getName()).log(Level.INFO, "\n\n");
		return JSON.toObject(strResponse, Workflow.class);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * @return the main
	 */
	public int getMain() {
		return main;
	}

	/**
	 * @param main the main to set
	 */
	public void setMain(int main) {
		this.main = main;
	}

	/**
	 *
	 * @return
	 */
	public boolean getHas_changes() {
		return has_changes;
	}

	/**
	 *
	 * @param has_changes
	 */
	public void setHas_changes(boolean has_changes) {
		this.has_changes = has_changes;
	}

	/**
	 *
	 * @return
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 *
	 * @param deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the workflow_criteria
	 */
	public List<Workflow_Criteria> getWorkflow_criteria() {
		return workflow_criteria;
	}

	/**
	 * @param workflow_criteria the workflow_criteria to set
	 */
	public void setWorkflow_criteria(List<Workflow_Criteria> workflow_criteria) {
		this.workflow_criteria = workflow_criteria;
	}

	/**
	 * @return the workflow_step
	 */
	public List<Workflow_Step> getWorkflow_step() {
		return workflow_step;
	}

	/**
	 * @param workflow_step the workflow_step to set
	 */
	public void setWorkflow_step(List<Workflow_Step> workflow_step) {
		this.workflow_step = workflow_step;
	}

	/**
	 * @return the version_id
	 */
	public int getVersion_id() {
		return version_id;
	}

	/**
	 * @param version_id the version_id to set
	 */
	public void setVersion_id(int version_id) {
		this.version_id = version_id;
	}

	/**
	 * @return the version_id_version
	 */
	public String getVersion_id_version() {
		return version_id_version;
	}

	/**
	 * @param version_id_version the version_id_version to set
	 */
	public void setVersion_id_version(String version_id_version) {
		this.version_id_version = version_id_version;
	}

	/**
	 * @return the version_docdate
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getVersion_docdate() {
		return version_docdate;
	}

	/**
	 * @param version_docdate the version_docdate to set
	 */
	public void setVersion_docdate(Date version_docdate) {
		this.version_docdate = version_docdate;
	}

	/**
	 * @return the version_active
	 */
	public int getVersion_active() {
		return version_active;
	}

	/**
	 * @param version_active the version_active to set
	 */
	public void setVersion_active(int version_active) {
		this.version_active = version_active;
	}

	/**
	 * @return the version_notes
	 */
	public String getVersion_notes() {
		return version_notes;
	}

	/**
	 * @param version_notes the version_notes to set
	 */
	public void setVersion_notes(String version_notes) {
		this.version_notes = version_notes;
	}
}
