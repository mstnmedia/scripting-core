package com.mstn.scripting.core.models;

import java.util.Date;

/**
 *
 * @author amatos
 */
public class Log {

	/**
	 *
	 */
	protected long id;

	/**
	 *
	 */
	protected long id_employee;

	/**
	 *
	 */
	protected String table_name;

	/**
	 *
	 */
	protected long id_record;

	/**
	 *
	 */
	protected String action;

	/**
	 *
	 */
	protected Date event_date;

	/**
	 *
	 */
	protected String before_value;

	/**
	 *
	 */
	protected String after_value;

	/**
	 *
	 */
	protected String ip;

	/**
	 *
	 */
	public Log() {
	}

	/**
	 *
	 * @param table_name
	 * @param action
	 * @param before_value
	 * @param after_value
	 */
	public Log(String table_name, String action,
			String before_value, String after_value) {
		this.id = 0;
		this.id_employee = 0;
		this.table_name = table_name;
		this.id_record = 0;
		this.action = action;
		this.event_date = new Date();
		this.before_value = before_value;
		this.after_value = after_value;
		this.ip = "";
	}

	/**
	 *
	 * @param id
	 * @param id_employee
	 * @param table_name
	 * @param id_record
	 * @param action
	 * @param event_date
	 * @param before_value
	 * @param after_value
	 * @param ip
	 */
	public Log(long id, long id_employee,
			String table_name, long id_record,
			String action, Date event_date,
			String before_value, String after_value,
			String ip) {
		this.id = id;
		this.id_employee = id_employee;
		this.table_name = table_name;
		this.id_record = id_record;
		this.action = action;
		this.event_date = event_date;
		this.before_value = before_value;
		this.after_value = after_value;
		this.ip = ip;
	}

	/**
	 *
	 * @return
	 */
	public long getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public long getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(long id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	public String getTable_name() {
		return table_name;
	}

	/**
	 *
	 * @param table_name
	 */
	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}

	/**
	 *
	 * @return
	 */
	public long getId_record() {
		return id_record;
	}

	/**
	 *
	 * @param id_record
	 */
	public void setId_record(long id_record) {
		this.id_record = id_record;
	}

	/**
	 *
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 *
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 *
	 * @return
	 */
	public Date getEvent_date() {
		return event_date;
	}

	/**
	 *
	 * @param event_date
	 */
	public void setEvent_date(Date event_date) {
		this.event_date = event_date;
	}

	/**
	 *
	 * @return
	 */
	public String getBefore_value() {
		return before_value;
	}

	/**
	 *
	 * @param before_value
	 */
	public void setBefore_value(String before_value) {
		this.before_value = before_value;
	}

	/**
	 *
	 * @return
	 */
	public String getAfter_value() {
		return after_value;
	}

	/**
	 *
	 * @param after_value
	 */
	public void setAfter_value(String after_value) {
		this.after_value = after_value;
	}

	/**
	 *
	 * @return
	 */
	public String getIp() {
		return ip;
	}

	/**
	 *
	 * @param ip
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

}
