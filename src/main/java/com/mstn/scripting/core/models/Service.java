package com.mstn.scripting.core.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author amatos
 */
public class Service {
    
	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	@NotEmpty
	protected int id_client;

	/**
	 *
	 */
	protected int id_type;

	/**
	 *
	 */
	protected int id_status;

	/**
	 *
	 */
	protected Boolean deleted;

	/**
	 *
	 */
	public Service() {
	}

	/**
	 *
	 * @param id
	 * @param id_client
	 * @param id_type
	 * @param id_status
	 * @param deleted
	 */
	public Service(int id, int id_client, int id_type, int id_status, Boolean deleted) {
		this.id = id;
		this.id_client = id_client;
		this.id_type = id_type;
		this.id_status = id_status;
		this.deleted = deleted;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_client() {
		return id_client;
	}

	/**
	 *
	 * @param id_client
	 */
	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	/**
	 *
	 * @return
	 */
	public int getId_type() {
		return id_type;
	}

	/**
	 *
	 * @param id_type
	 */
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}

	/**
	 *
	 * @return
	 */
	public int getId_status() {
		return id_status;
	}

	/**
	 *
	 * @param id_status
	 */
	public void setId_status(int id_status) {
		this.id_status = id_status;
	}

	/**
	 *
	 * @return
	 */
	public Boolean getDeleted() {
		return deleted;
	}

	/**
	 *
	 * @param deleted
	 */
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
    
}
