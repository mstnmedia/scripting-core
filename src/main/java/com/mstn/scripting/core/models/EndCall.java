package com.mstn.scripting.core.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author amatos
 */
public class EndCall {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	@NotEmpty
	protected String name;

	/**
	 *
	 */
	protected boolean deleted;

	/**
	 *
	 */
	public EndCall() {
	}

	/**
	 *
	 * @param id
	 * @param name
	 * @param deleted
	 */
	public EndCall(int id, String name, boolean deleted) {
		this.id = id;
		this.name = name;
		this.deleted = deleted;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public boolean getDeleted() {
		return deleted;
	}

	/**
	 *
	 * @param deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

}
