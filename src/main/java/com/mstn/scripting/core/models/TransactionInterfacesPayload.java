package com.mstn.scripting.core.models;

/**
 *
 * @author MSTN Media
 */
public class TransactionInterfacesPayload {

	/**
	 *
	 */
	protected V_Transaction transaction;

	/**
	 *
	 */
	protected Workflow_Step workflow_step;

	/**
	 *
	 */
	protected Interface_Service interface_service;

	/**
	 *
	 */
	protected String form;

	/**
	 *
	 */
	public TransactionInterfacesPayload() {
	}

	/**
	 *
	 * @param transaction
	 * @param workflow_step
	 * @param form
	 */
	public TransactionInterfacesPayload(V_Transaction transaction, Workflow_Step workflow_step, String form) {
		this.form = form;
		this.transaction = transaction;
		this.workflow_step = workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public V_Transaction getTransaction() {
		return transaction;
	}

	/**
	 *
	 * @param transaction
	 */
	public void setTransaction(V_Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 *
	 * @return
	 */
	public Workflow_Step getWorkflow_step() {
		return workflow_step;
	}

	/**
	 *
	 * @param workflow_step
	 */
	public void setWorkflow_step(Workflow_Step workflow_step) {
		this.workflow_step = workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public Interface_Service getInterface_service() {
		return interface_service;
	}

	/**
	 *
	 * @param interface_service
	 */
	public void setInterface_service(Interface_Service interface_service) {
		this.interface_service = interface_service;
	}

	/**
	 *
	 * @return
	 */
	public String getForm() {
		if (form == null || form.isEmpty()) {
			return "{}";
		}
		return form;
	}

	/**
	 *
	 * @param form
	 */
	public void setForm(String form) {
		this.form = form;
	}

}
