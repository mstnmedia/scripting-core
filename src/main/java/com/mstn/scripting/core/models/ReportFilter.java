package com.mstn.scripting.core.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author amatos
 */
public class ReportFilter {

	protected long id;
	protected long id_report;
	protected long id_employee;
	@NotEmpty
	protected String name;
	@NotEmpty
	protected String json;

	public ReportFilter() {
	}

	public ReportFilter(long id, long id_report, long id_employee, String name, String json) {
		this.id = id;
		this.id_report = id_report;
		this.id_employee = id_employee;
		this.name = name;
		this.json = json;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId_report() {
		return id_report;
	}

	public void setId_report(long id_report) {
		this.id_report = id_report;
	}

	public long getId_employee() {
		return id_employee;
	}

	public void setId_employee(long id_employee) {
		this.id_employee = id_employee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

}
