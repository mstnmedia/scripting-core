/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

/**
 * @author josesuero
 */
public class Workflow_Step_Content {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_workflow;

	/**
	 *
	 */
	protected int id_workflow_version;

	/**
	 *
	 */
	protected int id_workflow_step;

	/**
	 *
	 */
	protected int id_content;

	/**
	 *
	 */
	protected int order_index;

	private String workflow_step_name;
	private int workflow_step_order;
	private String workflow_name;
	private int workflow_main;
	private int workflow_deleted;
	private String version_id_version;
	private int version_active;

	private Content content;

	/**
	 *
	 */
	public Workflow_Step_Content() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param id_workflow_step
	 * @param id_content
	 * @param order_index
	 */
	public Workflow_Step_Content(
			int id, int id_center, int id_workflow, int id_workflow_version,
			int id_workflow_step, int id_content, int order_index
	) {
		this.id = id;
		this.id_center = id_center;
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.id_workflow_step = id_workflow_step;
		this.id_content = id_content;
		this.order_index = order_index;
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param id_workflow_step
	 * @param id_content
	 * @param order_index
	 * @param workflow_step_name
	 * @param workflow_step_order
	 * @param workflow_name
	 * @param workflow_main
	 * @param workflow_deleted
	 * @param version_id_version
	 * @param version_active
	 */
	public Workflow_Step_Content(
			int id, int id_center, int id_workflow, int id_workflow_version,
			int id_workflow_step, int id_content, int order_index,
			String workflow_step_name, int workflow_step_order,
			String workflow_name, int workflow_main,
			int workflow_deleted, String version_id_version, int version_active
	) {
		this.id = id;
		this.id_center = id_center;
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.id_workflow_step = id_workflow_step;
		this.id_content = id_content;
		this.order_index = order_index;

		this.workflow_step_name = workflow_step_name;
		this.workflow_step_order = workflow_step_order;
		this.workflow_name = workflow_name;
		this.workflow_main = workflow_main;
		this.workflow_deleted = workflow_deleted;
		this.version_id_version = version_id_version;
		this.version_active = version_active;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 *
	 * @param id_workflow
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_version() {
		return id_workflow_version;
	}

	/**
	 *
	 * @param id_workflow_version
	 */
	public void setId_workflow_version(int id_workflow_version) {
		this.id_workflow_version = id_workflow_version;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_step() {
		return id_workflow_step;
	}

	/**
	 *
	 * @param id_workflow_step
	 */
	public void setId_workflow_step(int id_workflow_step) {
		this.id_workflow_step = id_workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public int getId_content() {
		return id_content;
	}

	/**
	 *
	 * @param id_content
	 */
	public void setId_content(int id_content) {
		this.id_content = id_content;
	}

	/**
	 *
	 * @return
	 */
	public int getOrder_index() {
		return order_index;
	}

	/**
	 *
	 * @param order_index
	 */
	public void setOrder_index(int order_index) {
		this.order_index = order_index;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_step_name() {
		return workflow_step_name;
	}

	/**
	 *
	 * @param workflow_step_name
	 */
	public void setWorkflow_step_name(String workflow_step_name) {
		this.workflow_step_name = workflow_step_name;
	}

	/**
	 *
	 * @return
	 */
	public int getWorkflow_step_order() {
		return workflow_step_order;
	}

	/**
	 *
	 * @param workflow_step_order
	 */
	public void setWorkflow_step_order(int workflow_step_order) {
		this.workflow_step_order = workflow_step_order;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_name() {
		return workflow_name;
	}

	/**
	 *
	 * @param workflow_name
	 */
	public void setWorkflow_name(String workflow_name) {
		this.workflow_name = workflow_name;
	}

	/**
	 *
	 * @return
	 */
	public int getWorkflow_main() {
		return workflow_main;
	}

	/**
	 *
	 * @param workflow_main
	 */
	public void setWorkflow_main(int workflow_main) {
		this.workflow_main = workflow_main;
	}

	/**
	 *
	 * @return
	 */
	public int getWorkflow_deleted() {
		return workflow_deleted;
	}

	/**
	 *
	 * @param workflow_deleted
	 */
	public void setWorkflow_deleted(int workflow_deleted) {
		this.workflow_deleted = workflow_deleted;
	}

	/**
	 *
	 * @return
	 */
	public String getVersion_id_version() {
		return version_id_version;
	}

	/**
	 *
	 * @param version_id_version
	 */
	public void setVersion_id_version(String version_id_version) {
		this.version_id_version = version_id_version;
	}

	/**
	 *
	 * @return
	 */
	public int getVersion_active() {
		return version_active;
	}

	/**
	 *
	 * @param version_active
	 */
	public void setVersion_active(int version_active) {
		this.version_active = version_active;
	}

	/**
	 *
	 * @return
	 */
	public Content getContent() {
		return content;
	}

	/**
	 *
	 * @param content
	 */
	public void setContent(Content content) {
		this.content = content;
	}

}
