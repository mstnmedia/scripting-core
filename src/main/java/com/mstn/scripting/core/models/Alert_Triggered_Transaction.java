package com.mstn.scripting.core.models;

import java.util.Date;

/**
 *
 * @author amatos
 */
public class Alert_Triggered_Transaction {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_alert_triggered;

	/**
	 *
	 */
	protected int id_transaction;

	/**
	 *
	 */
	protected boolean triggerer;

	/**
	 *
	 */
	protected int id_alert;

	/**
	 *
	 */
	protected String alert_name;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected String center_name;

	/**
	 *
	 */
	protected int id_employee;

	/**
	 *
	 */
	protected String employee_name;

	/**
	 *
	 */
	protected String workflow_name;

	/**
	 *
	 */
	protected String version_id_version;

	/**
	 *
	 */
	protected String subscriber_no;

	/**
	 *
	 */
	protected Date start_date;

	/**
	 *
	 */
	protected Date end_date;

	/**
	 *
	 */
	protected String end_cause_name;

	/**
	 *
	 */
	public Alert_Triggered_Transaction() {
	}

	/**
	 *
	 * @param id
	 * @param id_alert_triggered
	 * @param id_transaction
	 * @param triggerer
	 */
	public Alert_Triggered_Transaction(int id, int id_alert_triggered, int id_transaction, boolean triggerer) {
		this.id = id;
		this.id_alert_triggered = id_alert_triggered;
		this.id_transaction = id_transaction;
		this.triggerer = triggerer;
	}

	/**
	 *
	 * @param id
	 * @param id_alert_triggered
	 * @param id_transaction
	 * @param triggerer
	 * @param id_center
	 * @param center_name
	 * @param id_alert
	 * @param alert_name
	 * @param id_employee
	 * @param employee_name
	 * @param workflow_name
	 * @param version_id_version
	 * @param subscriber_no
	 * @param start_date
	 * @param end_date
	 * @param end_cause_name
	 */
	public Alert_Triggered_Transaction(int id, int id_alert_triggered, int id_transaction, boolean triggerer,
			int id_center, String center_name, int id_alert, String alert_name,
			int id_employee, String employee_name, String workflow_name, String version_id_version,
			String subscriber_no, Date start_date, Date end_date, String end_cause_name) {
		this.id = id;
		this.id_alert_triggered = id_alert_triggered;
		this.id_transaction = id_transaction;
		this.triggerer = triggerer;
		this.id_center = id_center;
		this.center_name = center_name;
		this.id_alert = id_alert;
		this.alert_name = alert_name;
		this.id_employee = id_employee;
		this.employee_name = employee_name;
		this.workflow_name = workflow_name;
		this.version_id_version = version_id_version;
		this.subscriber_no = subscriber_no;
		this.start_date = start_date;
		this.end_date = end_date;
		this.end_cause_name = end_cause_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_alert_triggered() {
		return id_alert_triggered;
	}

	/**
	 *
	 * @param id_alert_triggered
	 */
	public void setId_alert_triggered(int id_alert_triggered) {
		this.id_alert_triggered = id_alert_triggered;
	}

	/**
	 *
	 * @return
	 */
	public int getId_transaction() {
		return id_transaction;
	}

	/**
	 *
	 * @param id_transaction
	 */
	public void setId_transaction(int id_transaction) {
		this.id_transaction = id_transaction;
	}

	/**
	 *
	 * @return
	 */
	public boolean isTriggerer() {
		return triggerer;
	}

	/**
	 *
	 * @param triggerer
	 */
	public void setTriggerer(boolean triggerer) {
		this.triggerer = triggerer;
	}

	/**
	 *
	 * @return
	 */
	public int getId_alert() {
		return id_alert;
	}

	/**
	 *
	 * @param id_alert
	 */
	public void setId_alert(int id_alert) {
		this.id_alert = id_alert;
	}

	/**
	 *
	 * @return
	 */
	public String getAlert_name() {
		return alert_name;
	}

	/**
	 *
	 * @param alert_name
	 */
	public void setAlert_name(String alert_name) {
		this.alert_name = alert_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	public String getEmployee_name() {
		return employee_name;
	}

	/**
	 *
	 * @param employee_name
	 */
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_name() {
		return workflow_name;
	}

	/**
	 *
	 * @param workflow_name
	 */
	public void setWorkflow_name(String workflow_name) {
		this.workflow_name = workflow_name;
	}

	/**
	 *
	 * @return
	 */
	public String getVersion_id_version() {
		return version_id_version;
	}

	/**
	 *
	 * @param version_id_version
	 */
	public void setVersion_id_version(String version_id_version) {
		this.version_id_version = version_id_version;
	}

	/**
	 *
	 * @return
	 */
	public String getSubscriber_no() {
		return subscriber_no;
	}

	/**
	 *
	 * @param subscriber_no
	 */
	public void setSubscriber_no(String subscriber_no) {
		this.subscriber_no = subscriber_no;
	}

	/**
	 *
	 * @return
	 */
	public Date getStart_date() {
		return start_date;
	}

	/**
	 *
	 * @param start_date
	 */
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	/**
	 *
	 * @return
	 */
	public Date getEnd_date() {
		return end_date;
	}

	/**
	 *
	 * @param end_date
	 */
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	/**
	 *
	 * @return
	 */
	public String getEnd_cause_name() {
		return end_cause_name;
	}

	/**
	 *
	 * @param end_cause_name
	 */
	public void setEnd_cause_name(String end_cause_name) {
		this.end_cause_name = end_cause_name;
	}

}
