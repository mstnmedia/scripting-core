package com.mstn.scripting.core.models;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.client.HttpClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.mail.MailerFactory;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Clase que define la configuración del microservicio Interfaces.
 *
 * @author MSTN Media
 */
public class ScriptingInterfacesConfiguration extends Configuration {

	private static final String DATABASE = "database";
	private static final String API_URL = "apiURL";
	private static final String API_BROWSER_URL = "apiBrowserURL";
	private static final String JARS_FOLDER = "jarsFolder";
	private static final String HTTP_CLIENT = "httpClient";
	private static final String INTERFACES = "interfaces";
	private static final String MAIL_FROM_NAME = "mailFromName";
	private static final String MAIL_FROM_EMAIL = "mailFromEmail";
	private static final String MAIL_SERVER = "mailServer";
	private static final String CONTENTS_FOLDER = "contentsFolder";
	private static final String TIME_ZONE = "timeZone";
	private static final String SHOW_INTERFACE_LOGS = "showInterfaceLogs";

	@NotEmpty
	private String apiURL;

	@NotEmpty
	private String apiBrowserURL;

	@NotEmpty
	private String jarsFolder;

	@Valid
	@NotNull
	private DataSourceFactory dataSourceFactory = new DataSourceFactory();

	@Valid
	@NotNull
	private HttpClientConfiguration httpClient = new HttpClientConfiguration();

	@NotEmpty
	private Map<String, String> interfaceConfigs;

	@NotEmpty
	private String contentsFolder;

	@NotEmpty
	private String mailFromName;

	@NotEmpty
	private String mailFromEmail;

	@NotEmpty
	private String timeZone;

	@NotNull
	private boolean showInterfaceLogs;

	@Valid
	@NotNull
	private MailerFactory mailServer = new MailerFactory();

	/**
	 *
	 * @return
	 */
	@JsonProperty(DATABASE)
	public DataSourceFactory getDataSourceFactory() {
		return dataSourceFactory;
	}

	/**
	 *
	 * @param dataSourceFactory
	 */
	@JsonProperty(DATABASE)
	public void setDataSourceFactory(final DataSourceFactory dataSourceFactory) {
		this.dataSourceFactory = dataSourceFactory;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(API_URL)
	public String getApiURL() {
		return apiURL;
	}

	/**
	 *
	 * @param apiURL
	 */
	@JsonProperty(API_URL)
	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(API_BROWSER_URL)
	public String getApiBrowserURL() {
		return apiBrowserURL;
	}

	/**
	 *
	 * @param apiBrowserURL
	 */
	@JsonProperty(API_BROWSER_URL)
	public void setApiBrowserURL(String apiBrowserURL) {
		this.apiBrowserURL = apiBrowserURL;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(JARS_FOLDER)
	public String getJarsFolder() {
		return jarsFolder;
	}

	/**
	 *
	 * @param jarsFolder
	 */
	@JsonProperty(JARS_FOLDER)
	public void setJarsFolder(String jarsFolder) {
		this.jarsFolder = jarsFolder;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(HTTP_CLIENT)
	public HttpClientConfiguration getHttpClientConfiguration() {
		return httpClient;
	}

	/**
	 *
	 * @param httpClient
	 */
	@JsonProperty(HTTP_CLIENT)
	public void setHttpClientConfiguration(HttpClientConfiguration httpClient) {
		this.httpClient = httpClient;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(INTERFACES)
	public Map<String, String> getInterfaces() {
		return interfaceConfigs;
	}

	/**
	 *
	 * @param interfaceConfigs
	 */
	@JsonProperty(INTERFACES)
	public void setInterfaces(Map<String, String> interfaceConfigs) {
		this.interfaceConfigs = interfaceConfigs;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(MAIL_FROM_NAME)
	public String getMailFromName() {
		return mailFromName;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(MAIL_FROM_EMAIL)
	public String getMailFromEmail() {
		return mailFromEmail;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(MAIL_SERVER)
	public MailerFactory getMailServer() {
		return mailServer;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(CONTENTS_FOLDER)
	public String getContentsFolder() {
		return contentsFolder;
	}

	/**
	 *
	 * @param contentsFolder
	 */
	@JsonProperty(CONTENTS_FOLDER)
	public void setContentsFolder(String contentsFolder) {
		this.contentsFolder = contentsFolder;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(TIME_ZONE)
	public String getTimeZone() {
		return timeZone;
	}

	@NotEmpty
	private String locale;

	/**
	 *
	 * @return
	 */
	@JsonProperty("locale")
	public String getLocale() {
		return locale;
	}

	/**
	 *
	 * @return
	 */
	@JsonProperty(SHOW_INTERFACE_LOGS)
	public boolean getShowInterfaceLogs() {
		return showInterfaceLogs;
	}
}
