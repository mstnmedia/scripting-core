/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.enums.WorkflowStepInterfaceFieldSource;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;

/**
 *
 * @author amatos
 */
public class InterfaceBase {

	/**
	 *
	 */
	static public final String SEPARATOR = "_";

	/**
	 *
	 */
	static public final String LIST_TEMPLATE = "{#}";

	/**
	 *
	 */
	static public final String FORM_GENERATIONDATE = "__generationDate";

	private int ID;
	@NotEmpty
	@NotBlank
	private String NAME;

	/**
	 *
	 */
	@NotEmpty
	protected Object INSTANCE;

	/**
	 *
	 */
	protected boolean showBaseLogs;

	/**
	 *
	 */
	protected V_Transaction transaction;

	/**
	 *
	 */
	protected List<Transaction_Result> results = new ArrayList();

	/**
	 *
	 */
	protected Workflow_Step step;

	/**
	 *
	 */
	protected Interface_Service service;

	/**
	 *
	 */
	protected Interface inter;

	/**
	 *
	 */
	protected boolean formIsFilledOrValid;

	/**
	 *
	 */
	protected TransactionInterfacesResponse response = new TransactionInterfacesResponse();

	/**
	 *
	 */
	protected ObjectNode form = JSON.newObjectNode();

	/**
	 *
	 */
	protected User user;

	/**
	 *
	 */
	protected HashMap<String, Interface_Field> fieldMap = new HashMap();

	/**
	 *
	 */
	protected HashMap<String, Interface_Field> resultMap = new HashMap();

	/**
	 *
	 */
	protected HashMap<String, Object> mapppedResults = new HashMap<>();

	/**
	 *
	 */
	protected HashMap<String, Integer> INTERFACES_ID = new HashMap<>();

	/**
	 *
	 */
	protected List<String> IGNORED_METHODS = new ArrayList(Arrays.asList(
			"hashCode", "clone", "close", "equals", "getClass", "wait",
			"registerNatives", "notifyAll", "notify", "finalize",
			"getBinding", "getComponents", "getEndpointReference", "getInboundHeaders",
			"getManagedObjectManager", "getPortInfo", "getRequestContext", "getResponseContext",
			"getSPI", "getWSEndpointReference", "setOutboundHeaders", "setAddress", "toString"
	));

	/**
	 *
	 * @param ID
	 * @param NAME
	 */
	public InterfaceBase(int ID, String NAME) {
		this(ID, NAME, null);
	}

	/**
	 *
	 * @param ID ID de la interfaz de esta base. En desuso. Siempre enviar '0'
	 * (cero);
	 * @param NAME Nombre único
	 * @param INSTANCE Instancia del endpoint del web service o de la clase de
	 * la que se crearán sus métodos como interfaces disponibles al usuario.
	 */
	@SuppressWarnings("OverridableMethodCallInConstructor")
	public InterfaceBase(int ID, String NAME, Object INSTANCE) {
		this.ID = ID;
		this.NAME = NAME;
		this.INSTANCE = processINSTANCE(INSTANCE);
	}

	/**
	 *
	 * @param INSTANCE
	 * @return
	 */
	protected Object processINSTANCE(Object INSTANCE) {
		return INSTANCE;
	}

	/**
	 *
	 * @return {@code List} of all declared methods in the class of INSTANCE
	 * object
	 */
	public List<Method> getMethods() {
		List<Method> methods = Arrays.asList(getCLASS().getDeclaredMethods())
				.stream()
				.filter(i -> !IGNORED_METHODS.contains(i.getName()))
				.sorted((a, b) -> a.getName().compareTo(b.getName()))
				.collect(Collectors.toList());
		return methods;
	}

	/**
	 *
	 * @param value
	 * @param inter
	 * @param paramName
	 * @return
	 * @throws Exception
	 */
	protected List<Transaction_Result> getTransactionResultsFromListResponse(Object value, Interface inter, String paramName) throws Exception {
		List<Transaction_Result> transactionResults = new ArrayList<>();
		List list = null;
		if (value.getClass().isArray()) {
			list = new ArrayList(Arrays.asList((Object[]) value));
		} else {
			list = (List) value;
		}

		transactionResults.add(getTransactionResultFromResponse(JSON.toString(list), inter, paramName + "_items"));
		transactionResults.add(getTransactionResultFromResponse(list.size(), inter, paramName + "_size"));
		for (int j = 0; j < list.size(); j++) {
			Object item = list.get(j);
			String itemName = paramName + j;
			transactionResults.addAll(getTransactionResultsFromResponse(item, inter, itemName));
		}
		return transactionResults;
	}

	/**
	 *
	 * @param value
	 * @param inter
	 * @param paramName
	 * @return
	 * @throws Exception
	 */
	protected List<Transaction_Result> getTransactionResultsFromJsonNodeResponse(JsonNode value, Interface inter, String paramName) throws Exception {
		List<Transaction_Result> transactionResults = new ArrayList<>();
		baseLog("ResultsFromJsonNodeResponse: " + paramName);
		if (value == null || value.isNull() || value.isMissingNode()) {
			Transaction_Result result = getTransactionResultFromResponse(Transaction_Result.NULL_VALUE, inter, paramName);
			transactionResults.add(result);
		} else if (value.isValueNode()) {
			Transaction_Result result = getTransactionResultFromResponse(value.asText(), inter, paramName);
			transactionResults.add(result);
		} else if (value.isArray()) {
			for (int i = 0; i < value.size(); i++) {
				JsonNode child = value.get(i);
				String childName = paramName + i;
				transactionResults.addAll(getTransactionResultsFromJsonNodeResponse(child, inter, childName));
			}
		} else if (value.isObject()) {
			for (Iterator<Map.Entry<String, JsonNode>> it = value.fields(); it.hasNext();) {
				Map.Entry<String, JsonNode> entry = it.next();
				String childName = paramName + "_" + entry.getKey();
				JsonNode child = entry.getValue();
				transactionResults.addAll(getTransactionResultsFromJsonNodeResponse(child, inter, childName));
			}
		}
		return transactionResults;
	}

	/**
	 *
	 * @param value
	 * @param inter
	 * @param paramName
	 * @return
	 */
	protected Transaction_Result getTransactionResultFromResponse(Object value, Interface inter, String paramName) {
		String strValue = SimpleClassToString(value);
		Transaction_Result result = new Transaction_Result(0, inter.getId(), paramName, paramName, strValue, null);
		Transaction_Result mapResult = inter.getMapResults()
				.getOrDefault(paramName, result);
		result.setId(mapResult.getId());
		result.setLabel(mapResult.getLabel());
		result.setProps(mapResult.getProps());
		return result;
	}

	/**
	 *
	 * @param value
	 * @param inter
	 * @param paramName
	 * @return
	 * @throws Exception
	 */
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter, String paramName) throws Exception {
		List<Transaction_Result> transactionResults = new ArrayList<>();
		baseLog("ResultsFromResponse: " + paramName);
		Class valueClass = value == null ? null : value.getClass();
		if (value == null || isSimpleClass(valueClass)) {
			Transaction_Result result = getTransactionResultFromResponse(value, inter, paramName);
			transactionResults.add(result);
		} else if (List.class.isAssignableFrom(valueClass) || valueClass.isArray()) {
			transactionResults.addAll(getTransactionResultsFromListResponse(value, inter, paramName));
		} else if (JsonNode.class.isAssignableFrom(valueClass)) {
			transactionResults.addAll(getTransactionResultsFromJsonNodeResponse((JsonNode) value, inter, paramName));
		} else {
			List<Field> fields = getClassFields(valueClass);
			for (int i = 0; i < fields.size(); i++) {
				Field field = fields.get(i);
				field.setAccessible(true);
				String fieldName = paramName + "_" + field.getName();
				Type type = field.getGenericType();
				Object fieldValue = field.get(value);
				if (fieldValue != null) {
					if (type instanceof ParameterizedType) {
						Type pType = ((ParameterizedType) type).getRawType();
						if (pType == JAXBElement.class) {
							fieldValue = ((JAXBElement) fieldValue).getValue();
						} else if (List.class.isAssignableFrom((Class) pType)) {
							transactionResults.addAll(getTransactionResultsFromListResponse(fieldValue, inter, fieldName));
							continue;
						}
					} else if (fieldValue.getClass().isArray()) {
						transactionResults.addAll(getTransactionResultsFromListResponse(fieldValue, inter, fieldName));
						continue;
					}
				}
				transactionResults.addAll(getTransactionResultsFromResponse(fieldValue, inter, fieldName));
			}
		}
		transactionResults.removeIf(item -> item == null);
		return transactionResults;
	}

	/**
	 *
	 * @param value
	 * @param inter
	 * @return
	 * @throws Exception
	 */
	protected List<Transaction_Result> getTransactionResultsFromResponse(Object value, Interface inter) throws Exception {
		List<Transaction_Result> transactionResults = new ArrayList<>();
		String paramName = inter.getName();
		transactionResults.addAll(getTransactionResultsFromResponse(value, inter, paramName));
		return transactionResults;
	}

	/**
	 *
	 * @param inter
	 * @param payload
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public TransactionInterfacesResponse callMethod(Interface inter, TransactionInterfacesPayload payload, User user) throws Exception {
		return generateResponse(inter, payload, user);
	}

	/**
	 *
	 * @param inter
	 * @param payload
	 * @param user
	 * @return
	 * @throws Exception
	 */
	protected TransactionInterfacesResponse generateResponse(Interface inter, TransactionInterfacesPayload payload, User user) throws Exception {
		try {
			initializeFromPayload(payload, inter, user);
			baseLog(NAME + " formIsFilledOrValid: " + formIsFilledOrValid);
			if (!formIsFilledOrValid) {
				response.setAction(TransactionInterfaceActions.SHOW_FORM);
				return setAsShowFormResponse();
			} else {
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
				return setAsNextStepResponse();
			}
		} catch (Exception ex) {
			Utils.logException(InterfaceBase.class, ex.getMessage(), ex);
			return TransactionInterfacesResponse.fromException(response, ex, user);
		}
	}

	/**
	 *
	 * @return Campos que se mostrarán al usuario en el formulario de la
	 * interfaz.
	 * @throws java.lang.Exception
	 */
	protected List<Interface_Field> getStepFields() throws Exception {
		List<Interface_Field> stepFields = new ArrayList();
		List<Interface_Field> fields = getFields();
		for (int i = 0; i < fields.size(); i++) {
			Interface_Field field = fields.get(i);
			Workflow_Step_Interface_Field wsif = wsifByFieldName(field.getName());
			if (wsif != null) {
				if (!WorkflowStepInterfaceFieldSource.USER.equals(wsif.getId_interface_source())) {
					// Si el valor del campo no lo asigna el usuario, entonces ocultárselo
					field.setId_type(InterfaceFieldTypes.HIDDEN);
					// Pero si el valor del campo se asigna desde una variable de una lista de otra interfaz,
					// mostar el campo como un dropdown con todos los valores encontrados para esa lista
					String fieldSource = Utils.coalesce(wsif.getId_interface_field_source(), "");
					if (!WorkflowStepInterfaceFieldSource.FIXED.equals(wsif.getId_interface_source())
							&& fieldSource.contains(LIST_TEMPLATE)) {
						field.setId_type(InterfaceFieldTypes.OPTIONS);
						List<String> items = new ArrayList();
						int index = 0;
						Transaction_Result result = null;
						do {
							String name = String.valueOf(fieldSource).replace(LIST_TEMPLATE, String.valueOf(index));
							result = transactionResultByName(name);
							if (result != null) {
								items.add(result.getValue());
							}
							index++;
						} while (result != null);

						JsonNode source = JSON.newObjectNode()
								.put("emptyOption", "SelectDisabled")
								.set("items", JSON.toJsonNode(items));
						field.setSource(source.toString());
					}
				}
			}
			stepFields.add(field);
		}
		return stepFields;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	protected TransactionInterfacesResponse setAsShowFormResponse() throws Exception {
		// LLenar formulario inicial desde el formulario enviado, los valores fijos del paso, 
		//		valores default de los campos, y variables
		List<Interface_Field> fields = getFields();
		fields.forEach(field -> {
			String fieldName = field.getName();
			if (!JSON.hasValue(form, fieldName) && Utils.stringNonNullOrEmpty(field.getDefault_value())) {
				form.put(field.getName(), field.getDefault_value());
			}
		});
		//	Si cambia el nombre de esta propiedad, debe actualizarse también en el componente
		//	TransactionStepInterface de la interfaz web
		form.put(FORM_GENERATIONDATE, DATE.nowAsLong());
		inter.setForm(JSON.toString(form));

		// Llenar aquí los campos que el usuario debe llenar, y los campos
		//		informativos con readonly = true
		List stepFields = getStepFields();
		inter.setInterface_field(stepFields);
		return response;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	protected TransactionInterfacesResponse setAsNextStepResponse() throws Exception {
		Object responseObj = invokeMethod();
		baseLog("Response: " + JSON.toString(responseObj));
		results = getTransactionResultsFromResponse(responseObj, inter);
		results.forEach(result -> {
			result.setId_interface(inter.getId());
		});
		response.setResults(results);

		String value = getValueFromJavascript();
		response.setValue(value);
		return response;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	protected Object invokeMethod() throws Exception {
		// Obtiene los datos necesarios para formar las variables de la transacción.
		String[] nameParts = inter.getName().split(SEPARATOR, 2);
		String name = nameParts[1];
		List<Object> params = new ArrayList();
		Method method = null;
		List<Method> methods = getMethods();
		for (Method m : methods) {
			if (m.getName().equals(name)) {
				method = m;
				break;
			}
		}

		Parameter[] parameters = method.getParameters();
		for (Parameter param : parameters) {
			Class paramType = param.getType();
			String paramName = inter.getName() + "_" + param.getName();
			List<Object> paramValues = getParamValues(paramType, paramName);
			for (int i = 0; i < paramValues.size(); i++) {
				Object value = paramValues.get(i);
				params.add(value);
			}
		}
		Object responseObj = sendParamsToMethod(inter, method, params);
		return responseObj;
	}

	/**
	 *
	 * @param inter
	 * @param method
	 * @param params
	 * @return
	 * @throws Exception
	 */
	protected Object sendParamsToMethod(Interface inter, Method method, List<Object> params) throws Exception {
		Object responseObj;
		if (method.getReturnType() == Void.TYPE) {
			try {
				method.invoke(INSTANCE, params.toArray());
				responseObj = true;
			} catch (Exception ex) {
				Utils.logException(this.getCLASS(), "Error while invoke method type java.lang.Void", ex);
				responseObj = false;
			}
		} else {
			responseObj = method.invoke(INSTANCE, params.toArray());
		}
		return responseObj;
	}

	/**
	 *
	 * @param paramType
	 * @param paramName
	 * @return
	 * @throws Exception
	 */
	protected List<Object> getParamValues(Class paramType, String paramName) throws Exception {
		List<Object> params = new ArrayList();
		if (isSimpleClass(paramType)) {
			Object value = getParamValueFromForm(paramType, paramName);
			params.add(value);
		} else {
			Object instance = getParamObjectFromForm(paramName, paramType, null, null);
			params.add(instance);
		}
		return params;
	}

	/**
	 *
	 * @param paramName
	 * @param paramClass
	 * @param paramInstance
	 * @param paramField
	 * @return
	 * @throws Exception
	 */
	protected Object getParamObjectFromForm(String paramName, Class paramClass, Object paramInstance, Field paramField) throws Exception {
		Object param = null;
		if (isSimpleClass(paramClass) || Object.class == paramClass) {
			param = getParamValueFromForm(paramClass, paramName);
		} else if (List.class.isAssignableFrom(paramClass) || paramClass.isArray()) {
			Class itemClass = Object.class;
			if (paramClass.isArray()) {
				itemClass = paramClass.getComponentType();
			}
			param = getParamListFromForm(itemClass, paramName, paramInstance, paramField);
			if (paramClass.isArray()) {
				param = ((List) param).toArray();
			}
		} else {
			param = paramClass.newInstance();
			List<Field> classFields = getClassFields(paramClass);
			for (int i = 0; i < classFields.size(); i++) {
				Field field = classFields.get(i);
				field.setAccessible(true);
				String fieldName = paramName + "_" + field.getName();
				baseLog(fieldName);
				Class fieldClass = field.getType();
				if (isSimpleClass(fieldClass)) {
					Object value = getParamValueFromForm(fieldClass, fieldName);
					field.set(param, value);
				} else {
					Object value = null;
					Type type = field.getGenericType();
					if (type instanceof ParameterizedType) {
						Type pType = ((ParameterizedType) type).getRawType();
						Type[] types = ((ParameterizedType) type).getActualTypeArguments();
						if (pType == JAXBElement.class) {
							XmlElementRef annotation = field.getAnnotation(XmlElementRef.class);
							Object jaxbValue = getParamObjectFromForm(fieldName, (Class) types[0], param, field);
							value = new JAXBElement(new QName(annotation.namespace(), annotation.name()), (Class) types[0], jaxbValue);
						} else if (List.class.isAssignableFrom((Class) pType)) {
							value = getParamListFromForm((Class) types[0], fieldName, param, field);
						}
					} else if (fieldClass.isArray()) {
						Class itemClass = fieldClass.getComponentType();
						value = getParamListFromForm(itemClass, fieldName, param, field).toArray();
					} else {
						value = getParamObjectFromForm(fieldName, fieldClass, param, field);
					}
					field.set(param, value);
				}
			}
		}
		return param;
	}

	/**
	 *
	 * @param <T>
	 * @param paramClass
	 * @param paramName
	 * @param paramInstance
	 * @param paramField
	 * @return
	 * @throws Exception
	 */
	protected <T> List<T> getParamListFromForm(Class<T> paramClass, String paramName, Object paramInstance, Field paramField) throws Exception {
		String jsonArray = JSON.getString(form, paramName, "[]");
		List<T> items = JSON.toList(jsonArray, paramClass);
		return items;
	}

	/**
	 *
	 * @param paramType
	 * @param paramName
	 * @return
	 * @throws Exception
	 */
	protected Object getParamValueFromForm(Class paramType, String paramName) throws Exception {
		Object value;
		if (JSON.hasValue(form, paramName)) {
			value = StringToSimpleClass(JSON.getString(form, paramName), paramType);
		} else {
			value = DefaultSimpleValue.get(paramType);
		}
		return value;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	protected String getValueFromJavascript() throws Exception {
		baseLog(">>> Executing Javascript code");
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
		try {
			String serviceCode = service == null ? null : service.getCode();
			List<Transaction_Result> transactionResults = getTransactionResults();
			HashMap<String, Object> map = mapResults(results, mapppedResults);
			StringBuilder js = new StringBuilder();
			js.append("var getServiceValue = function("
					+ "transaction, step, inter, service, "
					+ "results, currents, objResults"
					+ "){\n");
			js.append(serviceCode);
			js.append("\n}");
			String executableCode = js.toString();
			baseLog(executableCode);

			engine.eval(executableCode);
			Invocable invocable = (Invocable) engine;
			Object result = invocable.invokeFunction("getServiceValue",
					transaction, step, inter, service,
					results, transactionResults, map
			);
			baseLog(result);

			String value = String.valueOf(result);
			List<String> values = Utils.splitAsList(service == null ? "" : service.getReturned_values());
			if (!values.contains(value)) {
				return InterfaceOptions.ERROR;
			}
			return value;
		} catch (Exception ex) {
			Utils.logException(this.getClass(), "Error at getValueFromJavascript", ex);
			return InterfaceOptions.ERROR;
		}
	}

	/**
	 *
	 * @param _class
	 * @param inter
	 * @param field
	 * @return
	 * @throws Exception
	 */
	protected Interface_Field getInterfaceFieldType(Class _class, Interface inter, Interface_Field field) throws Exception {
		if (Float.class == _class || Float.TYPE == _class) {
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_FLOAT);
		} else if (Double.class == _class || Double.TYPE == _class
				|| BigDecimal.class == _class) {
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_DOUBLE);
		} else if (Long.class == _class || Long.TYPE == _class
				|| BigInteger.class == _class) {
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_LONG);
		} else if (Integer.class == _class || Integer.TYPE == _class) {
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_INT);
		} else if (Short.class == _class || Short.TYPE == _class) {
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_SHORT);
		} else if (Byte.class == _class || Byte.TYPE == _class) {
			field.setId_type(InterfaceFieldTypes.NUMBER);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_BYTE);
		} else if (Date.class.isAssignableFrom(_class)
				|| DateTime.class.isAssignableFrom(_class)
				|| XMLGregorianCalendar.class.isAssignableFrom(_class)) {
			field.setId_type(InterfaceFieldTypes.DATETIME);
		} else if (Boolean.class == _class || Boolean.TYPE == _class) {
			field.setId_type(InterfaceFieldTypes.BOOLEAN);
		} else if (Character.class == _class || Character.TYPE == _class) {
			field.setId_type(InterfaceFieldTypes.TEXT);
			field.setOnvalidate(InterfaceFieldTypes.VALIDATE_CHAR);
		} else if (List.class.isAssignableFrom(_class) || _class.isArray()) {
			field.setId_type(InterfaceFieldTypes.TABLE);
		} else if (Enum.class.isAssignableFrom(_class)) {
			field.setId_type(InterfaceFieldTypes.OPTIONS);
			Object[] items = _class.getEnumConstants();
			JsonNode source = JSON.newObjectNode()
					.put("emptyOption", "SelectDisabled")
					.put("enumClass", _class.getCanonicalName())
					.set("items", JSON.toJsonNode(items));
			field.setSource(source.toString());
		} else {
			field.setId_type(InterfaceFieldTypes.TEXT);
		}
		return field;
	}

	/**
	 *
	 */
	protected List<Class> SimpleClasses = Arrays.asList(
			Boolean.class,
			String.class, Character.class,
			Double.class, Float.class,
			Long.class, Integer.class, Short.class,
			BigInteger.class, BigDecimal.class,
			Byte.class,
			Date.class, DateTime.class, Timestamp.class,
			XMLGregorianCalendar.class,
			Locale.class
	);

	/**
	 *
	 */
	protected HashMap<Class, Object> DefaultSimpleValue = new HashMap() {
		{
			put(Boolean.TYPE, false);
			put(Integer.TYPE, 0);
			put(Long.TYPE, 0);
			put(Double.TYPE, 0);
			put(Float.TYPE, 0);
			put(Short.TYPE, 0);
			put(Byte.TYPE, 0);
			put(Byte.TYPE, 0);
			put(String.class, null);
		}
	};

	/**
	 *
	 * @param value
	 * @return
	 */
	protected String SimpleClassToString(Object value) {
		if (value == null) {
			return Transaction_Result.NULL_VALUE;
		}
		Class _class = value.getClass();
		if (Date.class.isAssignableFrom(_class)) {
			return DATE.toString((Date) value);
		} else if (DateTime.class.isAssignableFrom(_class)) {
			return DATE.toString((DateTime) value);
		} else if (XMLGregorianCalendar.class.isAssignableFrom(_class)) {
			return DATE.toString((XMLGregorianCalendar) value);
		}

		if (Enum.class.isAssignableFrom(_class)) {
			return ((Enum) value).name();
		} else if (Exception.class.isAssignableFrom(_class)) {
			return ((Exception) value).getMessage();
		}
		return String.valueOf(value);
	}

	/**
	 *
	 * @param value
	 * @param _class
	 * @return
	 * @throws Exception
	 */
	protected Object StringToSimpleClass(String value, Class _class) throws Exception {
		if (Boolean.TYPE == _class || Boolean.class.equals(_class)) {
			if (value == null && _class.isPrimitive()) {
				return false;
			}
			return Boolean.parseBoolean(value);
		} else if (Character.TYPE == _class || Character.class.equals(_class)) {
			if (value == null && _class.isPrimitive()) {
				return Character.MIN_VALUE;
			}
			return value.charAt(0);
		}
		if (value == null && _class.isPrimitive()) {
			return 0;
		} else if (Integer.TYPE == _class || Integer.class.equals(_class)) {
			return Integer.parseInt(value);
		} else if (Double.TYPE == _class || Double.class.equals(_class)) {
			return Double.parseDouble(value);
		} else if (Long.TYPE == _class || Long.class.equals(_class)) {
			return Long.parseLong(value);
		} else if (Float.TYPE == _class || Float.class.equals(_class)) {
			return Float.parseFloat(value);
		} else if (Short.TYPE == _class || Short.class.equals(_class)) {
			return Short.parseShort(value);
		} else if (Byte.TYPE == _class || Byte.class.equals(_class)) {
			return Byte.parseByte(value);
		}

		if (BigInteger.class.equals(_class)) {
			return value == null ? null : new BigInteger(value);
		} else if (BigDecimal.class.equals(_class)) {
			return value == null ? null : new BigDecimal(value);
		}

		if (Utils.isNullOrWhiteSpaces(value) && Arrays.asList(Date.class, DateTime.class, DateTime.class, Timestamp.class, XMLGregorianCalendar.class, XMLGregorianCalendarImpl.class)
				.contains(_class)) {
			return null;
		} else if (Date.class == _class) {
			return DATE.toDate(value);
		} else if (DateTime.class == _class) {
			return DateTime.parse(value);
		} else if (Timestamp.class == _class) {
			return Timestamp.valueOf(value);
		} else if (XMLGregorianCalendar.class == _class) {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(value);
		} else if (XMLGregorianCalendarImpl.class == _class) {
			return XMLGregorianCalendarImpl.parse(value);
		}

		if (Locale.class == _class) {
			return value == null ? null : Locale.forLanguageTag(value);
		}

		if (Enum.class.isAssignableFrom(_class)) {
			return value == null ? null : Enum.valueOf(_class, value);
		} else if (Exception.class.isAssignableFrom(_class)) {
			return value == null ? null : new Exception(value);
		}
		return value;
	}

	/**
	 *
	 * @param _class
	 * @return
	 */
	protected boolean isSimpleClass(Class _class) {
		return _class.isPrimitive()
				|| Date.class.isAssignableFrom(_class)
				|| Exception.class.isAssignableFrom(_class)
				|| Enum.class.isAssignableFrom(_class)
				|| XMLGregorianCalendar.class.isAssignableFrom(_class)
				|| SimpleClasses.contains(_class)
				|| false;
	}

	/**
	 *
	 * @param _class
	 * @param inter
	 * @param fieldName
	 * @param parentName
	 * @param prefix
	 * @return
	 * @throws Exception
	 */
	protected Interface_Field getInterfaceField(Class _class, Interface inter, String fieldName, String parentName, boolean prefix) throws Exception {
		String name = prefix ? fieldName : fieldName.replace(parentName, "");
		String label = prefix ? fieldName : fieldName.replace(parentName, "");
		Interface_Field field = new Interface_Field(0, inter.getId(), name, label, InterfaceFieldTypes.TEXT, true);
		field = getInterfaceFieldType(_class, inter, field);
		return field;
	}

	/**
	 *
	 * @param itemClass
	 * @param inter
	 * @param fieldName
	 * @return
	 * @throws Exception
	 */
	protected Interface_Field getInterfaceFieldList(Class itemClass, Interface inter, String fieldName) throws Exception {
		Interface_Field field = getInterfaceField(itemClass, inter, fieldName, fieldName, true);
		field.setId_type(InterfaceFieldTypes.TABLE);
		List<Interface_Field> children = getInterfaceFieldFromClass(inter, fieldName + LIST_TEMPLATE, fieldName, itemClass, false);
		field.setChildren(children);
		return field;
	}

	/**
	 *
	 * @param _class
	 * @return
	 */
	protected List<Field> getClassFields(Class _class) {
		return Arrays.asList(_class.getDeclaredFields());
	}

	// Este método no debe retornar lista nula.
	/**
	 *
	 * @param inter
	 * @param paramName
	 * @param parentName
	 * @param _class
	 * @param prefix
	 * @return
	 * @throws Exception
	 */
	protected List<Interface_Field> getInterfaceFieldFromClass(Interface inter, String paramName, String parentName, Class _class, boolean prefix) throws Exception {
		baseLog("FieldFromClass: " + paramName + " - " + prefix);
		List<Interface_Field> interfaceFields = new ArrayList<>();
		if (isSimpleClass(_class) || Object.class == _class) {
			Interface_Field interfaceField = getInterfaceField(_class, inter, paramName, parentName, prefix);
			interfaceFields.add(interfaceField);
		} else if (List.class.isAssignableFrom(_class) || _class.isArray()) {
			Class itemClass = _class.getComponentType();
			if (List.class.isAssignableFrom(_class)) {
				itemClass = Object.class;
			}
			Interface_Field interfaceField = getInterfaceFieldList(itemClass, inter, paramName);
			interfaceFields.add(interfaceField);
		} else {
			List<Field> classFields = getClassFields(_class);
			for (int i = 0; i < classFields.size(); i++) {
				Field field = classFields.get(i);
				String fieldName = field.getName();
				if (prefix) {
					fieldName = paramName + "_" + fieldName;
				}
				Type type = field.getGenericType();
				Class fieldClass = field.getType();
				if (type instanceof ParameterizedType) {
					Type pType = ((ParameterizedType) type).getRawType();
					Type[] types = ((ParameterizedType) type).getActualTypeArguments();
					// TODO: Add support to Map fields 
					// TODO: Add support to List<List<T>> fields by handled casting of types[0] 
					if (pType == JAXBElement.class) {
						fieldClass = (Class) types[0];
					} else if (List.class.isAssignableFrom((Class) pType)) {
						fieldClass = (Class) types[0];
						Interface_Field interfaceField = getInterfaceFieldList(fieldClass, inter, fieldName);
						interfaceFields.add(interfaceField);
						continue;
					}
				} else if (fieldClass.isArray()) {
					fieldClass = fieldClass.getComponentType();
					Interface_Field interfaceField = getInterfaceFieldList(fieldClass, inter, fieldName);
					interfaceFields.add(interfaceField);
					continue;
				}
				interfaceFields.addAll(getInterfaceFieldFromClass(inter, fieldName, parentName, fieldClass, true));
			}
		}
		interfaceFields.removeIf(f -> f == null);
		return interfaceFields;
	}

	/**
	 *
	 * @param method
	 * @param inter
	 * @return
	 * @throws Exception
	 */
	protected List<Interface_Field> getInterfaceFields(Method method, Interface inter) throws Exception {
		List<Interface_Field> interfaceFields = new ArrayList();
		Parameter[] parameters = method.getParameters();
		for (Parameter param : parameters) {
			String paramName = inter.getName() + "_" + param.getName();
			Class paramType = param.getType();
			interfaceFields.addAll(getInterfaceFieldFromClass(inter, paramName, paramName, paramType, true));
		}
		interfaceFields.removeIf(item -> item == null);
		return interfaceFields;
	}

	/**
	 *
	 * @param _class
	 * @param inter
	 * @param paramName
	 * @return
	 */
	protected Transaction_Result getTransactionResultFromClass(Class _class, Interface inter, String paramName) {
		Transaction_Result result = new Transaction_Result(inter.getId(), paramName, paramName);
		return result;
	}

	/**
	 *
	 * @param method
	 * @param inter
	 * @param paramName
	 * @param inspectedClases
	 * @return
	 */
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter, String paramName, List<Class> inspectedClases) {
		List<Transaction_Result> transactionResults = new ArrayList<>();
		baseLog("ResultsFromClass: " + paramName);
		Class _class = inspectedClases.get(inspectedClases.size() - 1);
		if (isSimpleClass(_class) || Object.class == _class) {
			Transaction_Result result = getTransactionResultFromClass(_class, inter, paramName);
			transactionResults.add(result);
		} else if (_class.isArray() || List.class.isAssignableFrom(_class)) {
			transactionResults.add(getTransactionResultFromClass(String.class, inter, paramName + "_items"));
			transactionResults.add(getTransactionResultFromClass(Integer.class, inter, paramName + "_size"));
			Class fieldClass = Object.class;
			if (_class.isArray()) {
				fieldClass = fieldClass.getComponentType();
			}
			List<Class> localInspectedClases = new ArrayList(inspectedClases);
			localInspectedClases.add(fieldClass);
			transactionResults.addAll(getTransactionResultsFromClass(method, inter, paramName + LIST_TEMPLATE, localInspectedClases));
		} else {
			List<Field> fields = getClassFields(_class);
			for (int i = 0; i < fields.size(); i++) {
				Field field = fields.get(i);
				String fieldName = paramName + "_" + field.getName();
				Type type = field.getGenericType();
				Class fieldClass = field.getType();
				if (type instanceof ParameterizedType) {
					Type pType = ((ParameterizedType) type).getRawType();
					Type[] types = ((ParameterizedType) type).getActualTypeArguments();
					if (pType == JAXBElement.class) {
						fieldClass = (Class) types[0];
					} else if (List.class.isAssignableFrom((Class) pType)) {
						transactionResults.add(getTransactionResultFromClass(String.class, inter, fieldName + "_items"));
						transactionResults.add(getTransactionResultFromClass(Integer.class, inter, fieldName + "_size"));
						fieldName += LIST_TEMPLATE;
						fieldClass = (Class) types[0];
					}
				} else if (fieldClass.isArray()) {
					transactionResults.add(getTransactionResultFromClass(String.class, inter, fieldName + "_items"));
					transactionResults.add(getTransactionResultFromClass(Integer.class, inter, fieldName + "_size"));
					fieldName += LIST_TEMPLATE;
					fieldClass = fieldClass.getComponentType();
				}
				if (inspectedClases.contains(fieldClass)) {
					baseLog(fieldName + " Ignoring field by duplicates class " + fieldClass.getName());
				} else {
					List<Class> localInspectedClases = new ArrayList(inspectedClases);
					localInspectedClases.add(fieldClass);
					transactionResults.addAll(getTransactionResultsFromClass(method, inter, fieldName, localInspectedClases));
				}
			}
		}
		return transactionResults;
	}

	/**
	 *
	 * @param method
	 * @param inter
	 * @return
	 */
	protected List<Transaction_Result> getTransactionResultsFromClass(Method method, Interface inter) {
		List<Transaction_Result> transactionResults = new ArrayList<>();
		String paramName = inter.getName();
		Class returnType = method.getReturnType();
		if (returnType == Void.TYPE) {
			returnType = Boolean.TYPE;
		}
		List<Class> inspectedClases = new ArrayList();
		inspectedClases.add(returnType);
		transactionResults.addAll(getTransactionResultsFromClass(method, inter, paramName, inspectedClases));

		return transactionResults;
	}

	/**
	 *
	 * @param method
	 * @param interID
	 * @param interName
	 * @param withChildren
	 * @return
	 * @throws Exception
	 */
	protected Interface getInterface(Method method, int interID, String interName, boolean withChildren) throws Exception {
		//	- Sólo se modifica el label de una interfaz después de que ha sido publicada actualizando 
		//	el valor en la base de datos y reiniciando el microservicio Interfaces.
		//	- Si se modifica el nombre de una interfaz será re-publicada con el nuevo nombre.
		Interface item = new Interface(interID, interName, interName, "", "", false, false);
		if (withChildren) {
			item.setInterface_field(getInterfaceFields(method, item));
			item.setInterface_results(getTransactionResultsFromClass(method, item));
		}
		return item;
	}

	/**
	 *
	 * @param withChildren
	 * @return
	 * @throws Exception
	 */
	public List<Interface> getInterfaces(boolean withChildren) throws Exception {
		List interfaces = new ArrayList();
		List<Method> methods = getMethods();
		for (int i = 0; i < methods.size(); i++) {
			Method method = methods.get(i);
			String interName = NAME + SEPARATOR + method.getName();
			int interID = INTERFACES_ID.getOrDefault(interName, 0);
			baseLog(interName + ": " + interID);
			Interface item = getInterface(method, interID, interName, withChildren);
			interfaces.add(item);
		}
		return interfaces;
	}

	/**
	 *
	 * @param payload
	 * @param _inter
	 * @param user
	 * @throws Exception
	 */
	protected void initializeFromPayload(TransactionInterfacesPayload payload, Interface _inter, User user) throws Exception {
		this.inter = JSON.clone(_inter);
		this.user = user;
		response.setInterface(inter);
		transaction = payload.getTransaction();
		response.setId_transaction(transaction == null ? 0 : transaction.getId());

		step = payload.getWorkflow_step();
		service = payload.getInterface_service();

		form = JSON.toNode(payload.getForm());
		List<Transaction_Result> transactionResults = getTransactionResults();
		mapppedResults = mapResults(transactionResults);
		prepareFormValues();

		formIsFilledOrValid = isValidForm();
	}

	/**
	 *
	 * @return
	 */
	protected List<Interface_Field> getFields() {
		return inter.getInterface_field();
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public String getTransactionResultValue(String name) {
		return getTransactionResultValue(name, "");
	}

	/**
	 *
	 * @param name
	 * @param defValue
	 * @return
	 */
	protected String getTransactionResultValue(String name, String defValue) {
		return String.valueOf(mapppedResults.getOrDefault(name, defValue));
	}

	/**
	 *
	 * @return
	 */
	protected List<Transaction_Result> getTransactionResults() {
		return transaction == null
				? Arrays.asList()
				: Utils.coalesce(transaction.getTransaction_results(), new ArrayList());
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	protected Transaction_Result transactionResultByName(String name) {
		List<Transaction_Result> transactionResults = getTransactionResults();
		for (int i = 0; i < transactionResults.size(); i++) {
			Transaction_Result result = transactionResults.get(i);
			if (Utils.stringAreEquals(result.getName(), name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 *
	 * @return
	 */
	protected List<Workflow_Step_Interface_Field> getWSIFs() {
		return Utils.coalesce(step.getWorkflow_step_interface_field(), new ArrayList());
	}

	/**
	 *
	 * @param fieldName
	 * @return
	 */
	protected Workflow_Step_Interface_Field wsifByFieldName(String fieldName) {
		List<Workflow_Step_Interface_Field> WSIFs = getWSIFs();
		for (int j = 0; j < WSIFs.size(); j++) {
			Workflow_Step_Interface_Field wsif = WSIFs.get(j);
			if (Utils.stringAreEquals(wsif.getId_interface_field(), fieldName)) {
				return wsif;
			}
		}
		return null;
	}

	/**
	 *
	 */
	protected void prepareFormValues() {
		getFields().forEach(field -> {
			String fieldName = field.getName();
			if (!JSON.hasValue(form, fieldName)) {
				Workflow_Step_Interface_Field wsif = wsifByFieldName(fieldName);
				if (wsif != null) {
					if (Utils.stringAreEquals(wsif.getId_interface_source(), WorkflowStepInterfaceFieldSource.FIXED)) {
						form.put(fieldName, wsif.getId_interface_field_source());
					} else if (!Utils.stringAreEquals(wsif.getId_interface_source(), WorkflowStepInterfaceFieldSource.USER)) {
						String fieldSourceName = wsif.getId_interface_field_source();
						if (!fieldSourceName.contains(LIST_TEMPLATE)) {
							String resultValue = getTransactionResultValue(fieldSourceName);
							form.put(fieldName, resultValue);
						}
					}
				}
				if (!JSON.hasValue(form, fieldName)) {
					form.put(fieldName, field.getDefault_value());
				}
			}
		});
	}

	/**
	 *
	 * @param fields
	 * @param form
	 * @return
	 */
	protected boolean areAllFieldsFilled(List<Interface_Field> fields, ObjectNode form) {
		for (Interface_Field field : fields) {
			boolean hasValue = JSON.hasValue(form, field.getName());
			if (field.isRequired() && !hasValue) {
				return false;
			}
			if (field.getId_type() == InterfaceFieldTypes.TABLE) {
				String jsonArray = JSON.getString(form, field.getName());
				try {
					ArrayNode array = JSON.toArrayNode(jsonArray);
					if (field.isRequired() && array.size() == 0) {
						return false;
					}
					List<Interface_Field> children = Utils.coalesce(field.children, new ArrayList());
					for (JsonNode row : array) {
						for (Interface_Field child : children) {
							if (LIST_TEMPLATE.equals(child.getName())) {
								if (child.isRequired() && !row.isValueNode()) {
									return false;
								}
							} else {
								// TODO: Los campos con los que el usuario puede no interactuar,
								//		como los tipo checkbox, deben agregar su propiedad al row en el navegador
								if (child.isRequired() && !JSON.hasValue(row, child.getName())) {
									return false;
								}
							}
						}
					}
				} catch (Exception ex) {
					Utils.logException(this.getClass(),
							"Error validating form: Can't parse form value for '" + field.getName() + "' to ArrayNode",
							ex);
				}
			}
		}
		return true;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	protected boolean isValidForm() throws Exception {
		boolean isValid;
		List<Interface_Field> fields = getStepFields();
//		List<Interface_Field> requireds = fields.stream().filter(i -> i.isRequired()).collect(Collectors.toList());
		List<Workflow_Step_Interface_Field> WSIFs = getWSIFs();

		List<Interface_Field> fieldsFilledByUser = new ArrayList<>();
		if (WSIFs.isEmpty()) {
			fieldsFilledByUser.addAll(fields);
		} else {
			for (int i = 0; i < fields.size(); i++) {
				Interface_Field field = fields.get(i);
				Workflow_Step_Interface_Field wsif = null;
				for (int j = 0; j < WSIFs.size(); j++) {
					wsif = WSIFs.get(j);
					if (Utils.stringAreEquals(wsif.getId_interface_field(), field.getName())
							&& Utils.stringAreEquals(WorkflowStepInterfaceFieldSource.USER, wsif.getId_interface_source())) {
						fieldsFilledByUser.add(field);
						break;
					}
					wsif = null;
				}
				if (wsif == null) {
					fieldsFilledByUser.add(field);
				}
			}
		}
		baseLog("form: " + form.toString());
		baseLog("fieldsFilledByUser.size(): " + fieldsFilledByUser.size());
		if (fieldsFilledByUser.isEmpty()) {
			//isValid = areAllFieldsFilled(requireds, form);
			isValid = true;
			baseLog(NAME + " isFormValid empty: " + Boolean.toString(isValid));
		} else {
			isValid = false;
			if (form.size() > 0) {
				//isValid = areAllFieldsFilled(requireds, form) && areAllFieldsFilled(fieldsFilledByUser, form);
				isValid = areAllFieldsFilled(fieldsFilledByUser, form);
				baseLog(NAME + " isFormValid size: " + Boolean.toString(isValid));
			}
		}
		return isValid;
	}

	/**
	 *
	 * @param results
	 * @return
	 */
	protected HashMap<String, Object> mapResults(List<Transaction_Result> results) {
		return mapResults(results, false);
	}

	/**
	 *
	 * @param results
	 * @param addPOJOs
	 * @return
	 */
	protected HashMap<String, Object> mapResults(List<Transaction_Result> results, boolean addPOJOs) {
		return mapResults(results, null, addPOJOs);
	}

	/**
	 *
	 * @param results
	 * @param map
	 * @return
	 */
	protected HashMap<String, Object> mapResults(List<Transaction_Result> results, HashMap<String, Object> map) {
		return mapResults(results, map, false);
	}

	/**
	 *
	 * @param map
	 * @param addPOJOs
	 * @return
	 */
	protected HashMap<String, Object> mapResults(HashMap<String, Object> map, boolean addPOJOs) {
		return mapResults(null, map, addPOJOs);
	}

	/**
	 *
	 * @param results
	 * @param map
	 * @param addPOJOs
	 * @return
	 */
	protected HashMap<String, Object> mapResults(List<Transaction_Result> results, HashMap<String, Object> map, boolean addPOJOs) {
		if (map == null) {
			map = new HashMap<>();
		}
		if (results == null) {
			results = Arrays.asList();
		}
		for (Transaction_Result result : results) {
			map.put(result.getName(), Utils.coalesce(result.getValue(), Transaction_Result.NULL_VALUE));
		}
		if (addPOJOs) {
			map.put("transaction", transaction);
			map.put("transactionResults", getTransactionResults());
			map.put("step", step);
			map.put("inter", inter);
			map.put("service", service);
		}
		return map;
	}

	/**
	 *
	 * @throws Exception
	 */
	public void test() throws Exception {

	}

	/**
	 *
	 * @param log
	 */
	public void baseLog(Object log) {
		if (showBaseLogs) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO, JSON.toString(log));
		}
	}

	/**
	 *
	 * @return
	 */
	public Class getCLASS() {
		return INSTANCE.getClass();
	}

	/**
	 *
	 * @return
	 */
	public int getID() {
		return ID;
	}

	/**
	 *
	 * @param ID
	 */
	public void setID(int ID) {
		this.ID = ID;
	}

	/**
	 *
	 * @return
	 */
	public String getNAME() {
		return NAME;
	}

	/**
	 *
	 * @param NAME
	 */
	public void setNAME(String NAME) {
		this.NAME = NAME;
	}

	/**
	 *
	 * @return
	 */
	public Object getINSTANCE() {
		return INSTANCE;
	}

	/**
	 *
	 * @param INSTANCE
	 */
	public void setINSTANCE(Object INSTANCE) {
		this.INSTANCE = INSTANCE;
	}

	/**
	 *
	 * @param showBaseLogs
	 */
	public void setShowBaseLogs(boolean showBaseLogs) {
		this.showBaseLogs = showBaseLogs;
	}

}
