package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.JSON;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author amatos
 */
public class Alert_Triggered {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_alert;

	/**
	 *
	 */
	@JsonDeserialize(using = JSON.DateTimeDeserializer.class)
	protected Date start_date;

	/**
	 *
	 */
	@JsonDeserialize(using = JSON.DateTimeDeserializer.class)
	protected Date end_date;

	/**
	 *
	 */
	@JsonDeserialize(using = JSON.DateTimeDeserializer.class)
	protected Date first_transaction_date;

	/**
	 *
	 */
	protected int state;

	/**
	 *
	 */
	protected long transactions_count;

	/**
	 *
	 */
	protected String description;

	/**
	 *
	 */
	protected String parent_ticket;

	/**
	 *
	 */
	protected String action;

	/**
	 *
	 */
	protected int id_employee;

	/**
	 *
	 */
	protected String center_name;

	/**
	 *
	 */
	protected String alert_name;

	/**
	 *
	 */
	protected Alert alert;

	/**
	 *
	 */
	protected List<Alert_Triggered_Data> data = new ArrayList();

	/**
	 *
	 */
	protected List<Alert_Triggered_Transaction> transaction = new ArrayList();

	/**
	 *
	 */
	public Alert_Triggered() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param center_name
	 * @param id_alert
	 * @param alert_name
	 * @param start_date
	 * @param end_date
	 * @param state
	 * @param first_transaction_date
	 * @param transactions_count
	 * @param description
	 * @param parent_ticket
	 * @param action
	 * @param id_employee
	 */
	public Alert_Triggered(int id,
			int id_center, String center_name, int id_alert, String alert_name,
			Date start_date, Date end_date, int state,
			Date first_transaction_date, long transactions_count,
			String description, String parent_ticket, String action, int id_employee) {
		this.id = id;
		this.id_center = id_center;
		this.center_name = center_name;
		this.id_alert = id_alert;
		this.alert_name = alert_name;
		this.start_date = start_date;
		this.end_date = end_date;
		this.state = state;
		this.first_transaction_date = first_transaction_date;
		this.transactions_count = transactions_count;
		this.description = description;
		this.parent_ticket = parent_ticket;
		this.action = action;
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_alert() {
		return id_alert;
	}

	/**
	 *
	 * @param id_alert
	 */
	public void setId_alert(int id_alert) {
		this.id_alert = id_alert;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getStart_date() {
		return start_date;
	}

	/**
	 *
	 * @param start_date
	 */
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getEnd_date() {
		return end_date;
	}

	/**
	 *
	 * @param end_date
	 */
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	/**
	 *
	 * @return
	 */
	public int getState() {
		return state;
	}

	/**
	 *
	 * @param state
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getFirst_transaction_date() {
		return first_transaction_date;
	}

	/**
	 *
	 * @param first_transaction_date
	 */
	public void setFirst_transaction_date(Date first_transaction_date) {
		this.first_transaction_date = first_transaction_date;
	}

	/**
	 *
	 * @return
	 */
	public long getTransactions_count() {
		return transactions_count;
	}

	/**
	 *
	 * @param transactions_count
	 */
	public void setTransactions_count(long transactions_count) {
		this.transactions_count = transactions_count;
	}

	/**
	 *
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 *
	 * @return
	 */
	public String getParent_ticket() {
		return parent_ticket;
	}

	/**
	 *
	 * @param parent_ticket
	 */
	public void setParent_ticket(String parent_ticket) {
		this.parent_ticket = parent_ticket;
	}

	/**
	 *
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 *
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 *
	 * @return
	 */
	public int getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

	/**
	 *
	 * @return
	 */
	public String getAlert_name() {
		return alert_name;
	}

	/**
	 *
	 * @param alert_name
	 */
	public void setAlert_name(String alert_name) {
		this.alert_name = alert_name;
	}

	/**
	 *
	 * @return
	 */
	public Alert getAlert() {
		return alert;
	}

	/**
	 *
	 * @param alert
	 */
	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	/**
	 *
	 * @return
	 */
	public List<Alert_Triggered_Data> getData() {
		return data;
	}

	/**
	 *
	 * @param data
	 */
	public void setData(List<Alert_Triggered_Data> data) {
		this.data = data;
	}

	/**
	 *
	 * @return
	 */
	public List<Alert_Triggered_Transaction> getTransaction() {
		return transaction;
	}

	/**
	 *
	 * @param transaction
	 */
	public void setTransaction(List<Alert_Triggered_Transaction> transaction) {
		this.transaction = transaction;
	}

}
