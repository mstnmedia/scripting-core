/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.mstn.scripting.core.HTTP;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import java.util.Arrays;
import java.util.List;
import org.apache.http.client.HttpClient;

/**
 *
 * @author josesuero
 */
public class Interface_Service {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_interface;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected String code;

	/**
	 *
	 */
	protected String returned_values;

	/**
	 *
	 */
	protected int order_index;

	private Interface Interface;

	/**
	 *
	 */
	public Interface_Service() {
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param code
	 * @param returned_values
	 */
	public Interface_Service(int id, int id_interface, String name, String code, String returned_values) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.code = code;
		this.returned_values = returned_values;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param code
	 * @param returned_values
	 * @param order_index
	 */
	public Interface_Service(int id, int id_interface, String name, String code, String returned_values, int order_index) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.code = code;
		this.returned_values = returned_values;
		this.order_index = order_index;
	}

	/**
	 *
	 * @param id_service
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Interface_Service getInterfaceService(int id_service, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(id_service));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/interfaces/interfaceService/getService",
				strPayload,
				bearerToken
		);
		System.err.println(strResponse);
		return JSON.toObject(strResponse, Interface_Service.class);
	}

	/**
	 *
	 * @param id_service
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public List<String> getValues(int id_service, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		Interface_Service service = getInterfaceService(id_service, httpClient, apiURL, bearerToken);
		List<String> valueList = Utils.splitAsList(service.getReturned_values());
		return valueList;
	}

	/**
	 *
	 * @param where
	 * @param loadInterfaces
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public List<Interface_Service> getInterfaceServices(String where, boolean loadInterfaces, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(loadInterfaces ? 1 : 0, where));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/interfaces/interfaceService/getServices",
				strPayload,
				bearerToken
		);
		System.err.println(strResponse);
		return Arrays.asList(JSON.toObject(strResponse, Interface_Service[].class));
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_interface() {
		return id_interface;
	}

	/**
	 *
	 * @param id_interface
	 */
	public void setId_interface(int id_interface) {
		this.id_interface = id_interface;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 *
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 *
	 * @return
	 */
	public String getReturned_values() {
		return returned_values;
	}

	/**
	 *
	 * @param returned_values
	 */
	public void setReturned_values(String returned_values) {
		this.returned_values = returned_values;
	}

	/**
	 *
	 * @return
	 */
	public int getOrder_index() {
		return order_index;
	}

	/**
	 *
	 * @param order_index
	 */
	public void setOrder_index(int order_index) {
		this.order_index = order_index;
	}

	/**
	 *
	 * @return
	 */
	public Interface getInterface() {
		return Interface;
	}

	/**
	 *
	 * @param Interface
	 */
	public void setInterface(Interface Interface) {
		this.Interface = Interface;
	}

}
