/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.mstn.scripting.core.HTTP;
import com.mstn.scripting.core.JSON;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.HttpClient;

/**
 *
 * @author josesuero
 */
public class Workflow_Step {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_workflow;

	/**
	 *
	 */
	protected int id_workflow_version;

	/**
	 *
	 */
	protected String guid;

	/**
	 *
	 */
	protected int id_type;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected int order_index;

	/**
	 *
	 */
	protected int id_workflow_external; //used as value for workflow and interface ids

	/**
	 *
	 */
	protected int id_interface_service;

	/**
	 *
	 */
	protected boolean initial_step;

	/**
	 *
	 */
	protected String workflow_name;

	/**
	 *
	 */
	protected boolean workflow_main;

	/**
	 *
	 */
	protected boolean workflow_deleted;

	/**
	 *
	 */
	protected String version_id_version;

	/**
	 *
	 */
	protected int version_active;

	/**
	 *
	 */
	protected String interface_service_name;

	private List<Workflow_Step_Content> workflow_step_content = new ArrayList<>();
	private List<Workflow_Step_Option> workflow_step_option = new ArrayList<>();
	private List<Workflow_Step_Interface_Field> workflow_step_interface_field = new ArrayList<>();
	private Interface Interface;
	private Workflow Workflow;

	/**
	 *
	 */
	public Workflow_Step() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param guid
	 * @param id_type
	 * @param name
	 * @param order_index
	 * @param id_workflow_external
	 * @param id_interface_service
	 * @param initial
	 */
	public Workflow_Step(
			int id, int id_center, int id_workflow, int id_workflow_version,
			String guid, int id_type, String name, int order_index,
			int id_workflow_external, int id_interface_service, boolean initial) {
		this.id = id;
		this.id_center = id_center;
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.guid = guid;
		this.id_type = id_type;
		this.name = name;
		this.order_index = order_index;
		this.id_workflow_external = id_workflow_external;
		this.id_interface_service = id_interface_service;
		this.initial_step = initial;
		this.workflow_step_content = new ArrayList<>();
		this.workflow_step_option = new ArrayList<>();
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param guid
	 * @param id_type
	 * @param name
	 * @param order_index
	 * @param id_workflow_external
	 * @param id_interface_service
	 * @param initial_step
	 * @param workflow_name
	 * @param workflow_main
	 * @param workflow_deleted
	 * @param version_id_version
	 * @param version_active
	 * @param interface_service_name
	 */
	public Workflow_Step(
			int id, int id_center, int id_workflow, int id_workflow_version,
			String guid, int id_type, String name, int order_index,
			int id_workflow_external, int id_interface_service, boolean initial_step,
			String workflow_name, boolean workflow_main, boolean workflow_deleted,
			String version_id_version, int version_active,
			String interface_service_name
	) {
		this.id = id;
		this.id_center = id_center;
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.guid = guid;
		this.id_type = id_type;
		this.name = name;
		this.order_index = order_index;
		this.id_workflow_external = id_workflow_external;
		this.id_interface_service = id_interface_service;
		this.initial_step = initial_step;
		this.workflow_name = workflow_name;
		this.workflow_main = workflow_main;
		this.workflow_deleted = workflow_deleted;
		this.version_id_version = version_id_version;
		this.version_active = version_active;
		this.interface_service_name = interface_service_name;
		this.workflow_step_content = new ArrayList<>();
		this.workflow_step_option = new ArrayList<>();
	}

	/**
	 *
	 * @param id_step
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Workflow_Step getStep(int id_step, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		return getStep(id_step, false, httpClient, apiURL, bearerToken);
	}

	/**
	 *
	 * @param id_step
	 * @param loadChildren
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Workflow_Step getStep(int id_step, boolean loadChildren, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(id_step, Boolean.toString(loadChildren)));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/workflows/workflowstep/getStep",
				strPayload,
				bearerToken
		);
		return JSON.toObject(strResponse, Workflow_Step.class);
	}

	/**
	 *
	 * @param id_version
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Workflow_Step getFirstStep(int id_version, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(id_version));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/workflows/workflowstep/getFirstStep",
				strPayload,
				bearerToken
		);
		return JSON.toObject(strResponse, Workflow_Step.class);
	}

	/**
	 *
	 * @param id_step
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Workflow_Step getStepWithOptions(int id_step, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(id_step));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/workflows/workflowstep/getStepWithOptions",
				strPayload,
				bearerToken
		);
		return JSON.toObject(strResponse, Workflow_Step.class);
	}

	/**
	 *
	 * @param service
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public String updateInterfaceServiceReferers(Interface_Service service, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(0, JSON.toString(service)));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/workflows/workflowstep/updateInterfaceServiceReferers",
				strPayload,
				bearerToken
		);
		return strResponse;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 * @return the id_workflow
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 * @param id_workflow the id_workflow to set
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 * @return the id_workflow_version
	 */
	public int getId_workflow_version() {
		return id_workflow_version;
	}

	/**
	 * @param id_workflow_version the id_workflow_version to set
	 */
	public void setId_workflow_version(int id_workflow_version) {
		this.id_workflow_version = id_workflow_version;
	}

	/**
	 *
	 * @return
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 *
	 * @param guid
	 */
	public void setGuid(String guid) {
		this.guid = guid;
	}

	/**
	 * @return the id_type
	 */
	public int getId_type() {
		return id_type;
	}

	/**
	 * @param id_type the id_type to set
	 */
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}

	/**
	 * @return the order_index
	 */
	public int getOrder_index() {
		return order_index;
	}

	/**
	 * @param order_index the order_index to set
	 */
	public void setOrder_index(int order_index) {
		this.order_index = order_index;
	}

	/**
	 * @return the id_workflow_external
	 */
	public int getId_workflow_external() {
		return id_workflow_external;
	}

	/**
	 * @param id_workflow_external the id_workflow_external to set
	 */
	public void setId_workflow_external(int id_workflow_external) {
		this.id_workflow_external = id_workflow_external;
	}

	/**
	 *
	 * @return
	 */
	public int getId_interface_service() {
		return id_interface_service;
	}

	/**
	 *
	 * @param id_interface_service
	 */
	public void setId_interface_service(int id_interface_service) {
		this.id_interface_service = id_interface_service;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the workflow_step_content
	 */
	public List<Workflow_Step_Content> getWorkflow_step_content() {
		return workflow_step_content;
	}

	/**
	 * @param workflow_step_content the workflow_step_content to set
	 */
	public void setWorkflow_step_content(List<Workflow_Step_Content> workflow_step_content) {
		this.workflow_step_content = workflow_step_content;
	}

	/**
	 * @return the workflow_step_option
	 */
	public List<Workflow_Step_Option> getWorkflow_step_option() {
		return workflow_step_option;
	}

	/**
	 * @param workflow_step_option the workflow_step_option to set
	 */
	public void setWorkflow_step_option(List<Workflow_Step_Option> workflow_step_option) {
		this.workflow_step_option = workflow_step_option;
	}

	/**
	 *
	 * @return
	 */
	public List<Workflow_Step_Interface_Field> getWorkflow_step_interface_field() {
		return workflow_step_interface_field;
	}

	/**
	 *
	 * @param workflow_step_interface_field
	 */
	public void setWorkflow_step_interface_field(List<Workflow_Step_Interface_Field> workflow_step_interface_field) {
		this.workflow_step_interface_field = workflow_step_interface_field;
	}

	/**
	 *
	 * @return
	 */
	public Interface getInterface() {
		return Interface;
	}

	/**
	 *
	 * @param Interface
	 */
	public void setInterface(Interface Interface) {
		this.Interface = Interface;
	}

	/**
	 *
	 * @return
	 */
	public Workflow getWorkflow() {
		return Workflow;
	}

	/**
	 *
	 * @param Workflow
	 */
	public void setWorkflow(Workflow Workflow) {
		this.Workflow = Workflow;
	}

	/**
	 *
	 * @return
	 */
	public boolean isInitial_step() {
		return initial_step;
	}

	/**
	 *
	 * @param initial_step
	 */
	public void setInitial_step(boolean initial_step) {
		this.initial_step = initial_step;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_name() {
		return workflow_name;
	}

	/**
	 *
	 * @param workflow_name
	 * @return
	 */
	public Workflow_Step setWorkflow_name(String workflow_name) {
		this.workflow_name = workflow_name;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public boolean isWorkflow_main() {
		return workflow_main;
	}

	/**
	 *
	 * @param workflow_main
	 * @return
	 */
	public Workflow_Step setWorkflow_main(boolean workflow_main) {
		this.workflow_main = workflow_main;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public boolean isWorkflow_deleted() {
		return workflow_deleted;
	}

	/**
	 *
	 * @param workflow_deleted
	 * @return
	 */
	public Workflow_Step setWorkflow_deleted(boolean workflow_deleted) {
		this.workflow_deleted = workflow_deleted;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getVersion_id_version() {
		return version_id_version;
	}

	/**
	 *
	 * @param version_id_version
	 */
	public void setVersion_id_version(String version_id_version) {
		this.version_id_version = version_id_version;
	}

	/**
	 *
	 * @return
	 */
	public int getVersion_active() {
		return version_active;
	}

	/**
	 *
	 * @param version_active
	 */
	public void setVersion_active(int version_active) {
		this.version_active = version_active;
	}

	/**
	 *
	 * @return
	 */
	public String getInterface_service_name() {
		return interface_service_name;
	}

	/**
	 *
	 * @param interface_service_name
	 * @return
	 */
	public Workflow_Step setInterface_service_name(String interface_service_name) {
		this.interface_service_name = interface_service_name;
		return this;
	}

}
