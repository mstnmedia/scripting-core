package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.JSON;
import java.util.Date;
import java.util.List;

/**
 *
 * @author amatos
 */
public class Transaction {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected String id_call;

	/**
	 *
	 */
	protected int id_category;

	/**
	 *
	 */
	protected int id_indicator;

	/**
	 *
	 */
	protected int id_workflow;

	/**
	 *
	 */
	protected int id_workflow_version;

	/**
	 *
	 */
	protected int id_employee;

	/**
	 *
	 */
	protected String id_customer;

	/**
	 *
	 */
	protected String id_subscription;

	/**
	 *
	 */
	protected int id_current_step;

	/**
	 *
	 */
	protected Date start_date;

	/**
	 *
	 */
	protected Date end_date;

	/**
	 *
	 */
	protected int end_cause;

	/**
	 *
	 */
	protected int end_id_employee;

	/**
	 *
	 */
	protected int state;

	/**
	 *
	 */
	protected String subscriber_no;

	/**
	 *
	 */
	protected String caller_alt_phone;

	/**
	 *
	 */
	protected String note;

	private Category category;
	private Workflow workflow;
	private Employee employee;
	private Workflow_Step workflow_step;
	private List<Transaction_Result> transaction_results;
	private List<V_Transaction_Step> transaction_steps;

	private Client client;
	private Service service;

	/**
	 *
	 */
	public Transaction() {

	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_call
	 * @param id_category
	 * @param id_indicator
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param id_employee
	 * @param id_customer
	 * @param id_subscription
	 * @param id_current_step
	 * @param start_date
	 * @param end_date
	 * @param end_cause
	 * @param end_id_employee
	 * @param state
	 * @param subscriber_no
	 * @param caller_alt_phone
	 * @param note
	 */
	public Transaction(int id, int id_center, String id_call, int id_category, int id_indicator,
			int id_workflow, int id_workflow_version, int id_employee,
			String id_customer, String id_subscription, int id_current_step, Date start_date,
			Date end_date, int end_cause, int end_id_employee, int state, String subscriber_no, String caller_alt_phone, String note) {
		this.id = id;
		this.id_center = id_center;
		this.id_call = id_call;
		this.id_category = id_category;
		this.id_indicator = id_indicator;
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.id_employee = id_employee;
		this.id_customer = id_customer;
		this.id_subscription = id_subscription;
		this.id_current_step = id_current_step;
		this.start_date = start_date;
		this.end_date = end_date;
		this.end_cause = end_cause;
		this.end_id_employee = end_id_employee;
		this.state = state;
		this.subscriber_no = subscriber_no;
		this.caller_alt_phone = caller_alt_phone;
		this.note = note;
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_call
	 * @param id_category
	 * @param id_indicator
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param id_employee
	 * @param id_customer
	 * @param id_subscription
	 * @param id_current_step
	 * @param start_date
	 * @param end_date
	 * @param end_cause
	 * @param end_id_employee
	 * @param state
	 * @param subscriber_no
	 * @param caller_alt_phone
	 * @param note
	 * @param category
	 * @param workflow
	 * @param employee
	 * @param workflow_step
	 * @param transaction_results
	 */
	public Transaction(
			int id, int id_center, String id_call, int id_category, int id_indicator, int id_workflow, int id_workflow_version,
			int id_employee, String id_customer, String id_subscription, int id_current_step,
			Date start_date, Date end_date, int end_cause, int end_id_employee, int state, String subscriber_no, String caller_alt_phone,
			String note, Category category, Workflow workflow, Employee employee,
			Workflow_Step workflow_step, List<Transaction_Result> transaction_results
	) {
		this.id = id;
		this.id_center = id_center;
		this.id_call = id_call;
		this.id_category = id_category;
		this.id_indicator = id_indicator;
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.id_employee = id_employee;
		this.id_customer = id_customer;
		this.id_subscription = id_subscription;
		this.id_current_step = id_current_step;
		this.start_date = start_date;
		this.end_date = end_date;
		this.end_cause = end_cause;
		this.end_id_employee = end_id_employee;
		this.state = state;
		this.subscriber_no = subscriber_no;
		this.caller_alt_phone = caller_alt_phone;
		this.note = note;
		this.category = category;
		this.workflow = workflow;
		this.employee = employee;
		this.workflow_step = workflow_step;
		this.transaction_results = transaction_results;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getId_call() {
		return id_call;
	}

	/**
	 *
	 * @param id_call
	 */
	public void setId_call(String id_call) {
		this.id_call = id_call;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 *
	 * @param id_workflow
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_version() {
		return id_workflow_version;
	}

	/**
	 *
	 * @param id_workflow_version
	 */
	public void setId_workflow_version(int id_workflow_version) {
		this.id_workflow_version = id_workflow_version;
	}

	/**
	 *
	 * @return
	 */
	public int getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	public String getId_customer() {
		return id_customer;
	}

	/**
	 *
	 * @param id_customer
	 */
	public void setId_customer(String id_customer) {
		this.id_customer = id_customer;
	}

	/**
	 *
	 * @return
	 */
	public int getId_current_step() {
		return id_current_step;
	}

	/**
	 *
	 * @param id_current_step
	 */
	public void setId_current_step(int id_current_step) {
		this.id_current_step = id_current_step;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getStart_date() {
		return start_date;
	}

	/**
	 *
	 * @param start_date
	 */
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getEnd_date() {
		return end_date;
	}

	/**
	 *
	 * @param end_date
	 */
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	/**
	 *
	 * @return
	 */
	public int getEnd_cause() {
		return end_cause;
	}

	/**
	 *
	 * @param end_cause
	 */
	public void setEnd_cause(int end_cause) {
		this.end_cause = end_cause;
	}

	/**
	 *
	 * @return
	 */
	public int getEnd_id_employee() {
		return end_id_employee;
	}

	/**
	 *
	 * @param end_id_employee
	 */
	public void setEnd_id_employee(int end_id_employee) {
		this.end_id_employee = end_id_employee;
	}

	/**
	 *
	 * @return
	 */
	public int getState() {
		return state;
	}

	/**
	 *
	 * @param state
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 *
	 * @return
	 */
	public Workflow_Step getWorkflow_step() {
		return workflow_step;
	}

	/**
	 *
	 * @param workflow_step
	 */
	public void setWorkflow_step(Workflow_Step workflow_step) {
		this.workflow_step = workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public int getId_indicator() {
		return id_indicator;
	}

	/**
	 *
	 * @param id_indicator
	 */
	public void setId_indicator(int id_indicator) {
		this.id_indicator = id_indicator;
	}

	/**
	 *
	 * @return
	 */
	public int getId_category() {
		return id_category;
	}

	/**
	 *
	 * @param id_category
	 */
	public void setId_category(int id_category) {
		this.id_category = id_category;
	}

	/**
	 *
	 * @return
	 */
	public String getId_subscription() {
		return id_subscription;
	}

	/**
	 *
	 * @param id_subscription
	 */
	public void setId_subscription(String id_subscription) {
		this.id_subscription = id_subscription;
	}

	/**
	 *
	 * @return
	 */
	public String getSubscriber_no() {
		return subscriber_no;
	}

	/**
	 *
	 * @param subscriber_no
	 */
	public void setSubscriber_no(String subscriber_no) {
		this.subscriber_no = subscriber_no;
	}

	/**
	 *
	 * @return
	 */
	public String getCaller_alt_phone() {
		return caller_alt_phone;
	}

	/**
	 *
	 * @param caller_alt_phone
	 */
	public void setCaller_alt_phone(String caller_alt_phone) {
		this.caller_alt_phone = caller_alt_phone;
	}

	/**
	 *
	 * @return
	 */
	public String getNote() {
		return note;
	}

	/**
	 *
	 * @param note
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 *
	 * @return
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 *
	 * @param category
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 *
	 * @return
	 */
	public Workflow getWorkflow() {
		return workflow;
	}

	/**
	 *
	 * @param workflow
	 */
	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	/**
	 *
	 * @return
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 *
	 * @param employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 *
	 * @return
	 */
	public List<Transaction_Result> getTransaction_results() {
		return transaction_results;
	}

	/**
	 *
	 * @param transaction_results
	 */
	public void setTransaction_results(List<Transaction_Result> transaction_results) {
		this.transaction_results = transaction_results;
	}

	/**
	 *
	 * @return
	 */
	public List<V_Transaction_Step> getTransaction_steps() {
		return transaction_steps;
	}

	/**
	 *
	 * @param transaction_steps
	 */
	public void setTransaction_steps(List<V_Transaction_Step> transaction_steps) {
		this.transaction_steps = transaction_steps;
	}

	/**
	 *
	 * @return
	 */
	public Client getClient() {
		return client;
	}

	/**
	 *
	 * @param client
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 *
	 * @return
	 */
	public Service getService() {
		return service;
	}

	/**
	 *
	 * @param service
	 */
	public void setService(Service service) {
		this.service = service;
	}

}
