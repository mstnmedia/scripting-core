/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.mstn.scripting.core.models.Interface;


/**
 *
 * @author josesuero
 */
public class Interface_Option {

	private int id;
	private int id_interface;
	private String name;
	private int id_type;
	private String value;
	private String color;
	private int order_index;

	private Interface Interface;

	/**
	 *
	 */
	public Interface_Option() {}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param id_type
	 * @param value
	 * @param color
	 * @param order_index
	 */
	public Interface_Option(int id, int id_interface, String name, int id_type, String value, String color, int order_index) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.id_type = id_type;
		this.value = value;
		this.color = color;
		this.order_index = order_index;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param id_type
	 * @param value
	 * @param color
	 * @param order_index
	 * @param Interface
	 */
	public Interface_Option(int id, int id_interface, String name, int id_type, String value, String color, int order_index, Interface Interface) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.id_type = id_type;
		this.value = value;
		this.color = color;
		this.order_index = order_index;
		this.Interface = Interface;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_interface() {
		return id_interface;
	}

	/**
	 *
	 * @param id_interface
	 */
	public void setId_interface(int id_interface) {
		this.id_interface = id_interface;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public int getId_type() {
		return id_type;
	}

	/**
	 *
	 * @param id_type
	 */
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public String getColor() {
		return color;
	}

	/**
	 *
	 * @param color
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 *
	 * @return
	 */
	public int getOrder_index() {
		return order_index;
	}

	/**
	 *
	 * @param order_index
	 */
	public void setOrder_index(int order_index) {
		this.order_index = order_index;
	}

	/**
	 *
	 * @return
	 */
	public Interface getInterface() {
		return Interface;
	}

	/**
	 *
	 * @param Interface
	 */
	public void setInterface(Interface Interface) {
		this.Interface = Interface;
	}

}
