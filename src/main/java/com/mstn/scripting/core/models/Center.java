package com.mstn.scripting.core.models;

import com.mstn.scripting.core.HTTP;
import com.mstn.scripting.core.JSON;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.HttpClient;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author amatos
 */
public class Center {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	@NotEmpty
	protected String name;

	/**
	 *
	 */
	protected int id_employee;

	/**
	 *
	 */
	protected boolean deleted;

	/**
	 *
	 */
	protected String notes;

	/**
	 *
	 */
	protected String employee_name;

	/**
	 *
	 */
	protected List<Center_Manager> managers = new ArrayList();

	/**
	 *
	 */
	public Center() {
	}

	/**
	 *
	 * @param id
	 * @param name
	 * @param id_employee
	 * @param deleted
	 * @param notes
	 * @param employee_name
	 */
	public Center(int id, String name, int id_employee, boolean deleted, String notes, String employee_name) {
		this.id = id;
		this.name = name;
		this.id_employee = id_employee;
		this.deleted = deleted;
		this.notes = notes;
		this.employee_name = employee_name;
	}

	/**
	 *
	 * @param id
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Center getCenter(int id, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strResponse = HTTP.getResource(
				httpClient,
				apiURL + "/centers/center/" + id,
				bearerToken
		);
		System.err.println(strResponse);
		return JSON.toObject(strResponse, Center.class);
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public int getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 *
	 * @param deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 *
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 *
	 * @param notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 *
	 * @return
	 */
	public String getEmployee_name() {
		return employee_name;
	}

	/**
	 *
	 * @param employee_name
	 */
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	/**
	 *
	 * @return
	 */
	public List<Center_Manager> getManagers() {
		return managers;
	}

	/**
	 *
	 * @param managers
	 */
	public void setManagers(List<Center_Manager> managers) {
		this.managers = managers;
	}

}
