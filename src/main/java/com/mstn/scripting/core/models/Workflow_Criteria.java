/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

/**
 *
 * @author josesuero
 */
public class Workflow_Criteria {

	private int id;

	/**
	 *
	 */
	protected int id_center;
	private int id_workflow;
	private int id_criteria;
	private String name;
	private String value;
	private String workflow_name;
	private String criteria_name;

	private Criteria criteria;

	/**
	 *
	 */
	public Workflow_Criteria() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_workflow
	 * @param id_criteria
	 * @param name
	 * @param value
	 * @param workflow_name
	 * @param criteria_name
	 */
	public Workflow_Criteria(
			int id, int id_center,
			int id_workflow, int id_criteria,
			String name, String value,
			String workflow_name, String criteria_name) {
		this.id = id;
		this.id_center = id_center;
		this.id_workflow = id_workflow;
		this.id_criteria = id_criteria;
		this.name = name;
		this.value = value;
		this.workflow_name = workflow_name;
		this.criteria_name = criteria_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 *
	 * @param id_workflow
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 *
	 * @return
	 */
	public int getId_criteria() {
		return id_criteria;
	}

	/**
	 *
	 * @param id_criteria
	 */
	public void setId_criteria(int id_criteria) {
		this.id_criteria = id_criteria;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_name() {
		return workflow_name;
	}

	/**
	 *
	 * @param workflow_name
	 */
	public void setWorkflow_name(String workflow_name) {
		this.workflow_name = workflow_name;
	}

	/**
	 *
	 * @return
	 */
	public String getCriteria_name() {
		return criteria_name;
	}

	/**
	 *
	 * @param criteria_name
	 */
	public void setCriteria_name(String criteria_name) {
		this.criteria_name = criteria_name;
	}

	/**
	 *
	 * @return
	 */
	public Criteria getCriteria() {
		return criteria;
	}

	/**
	 *
	 * @param criteria
	 */
	public void setCriteria(Criteria criteria) {
		this.criteria = criteria;
	}

}
