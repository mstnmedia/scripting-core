/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author josesuero
 */
public class Criteria_Condition {

	private int id;
	private int id_center;
	private int id_criteria;
	@NotEmpty
	private String name;
	private String operator;
	private String value;

	/**
	 *
	 */
	public Criteria_Condition() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_criteria
	 * @param name
	 * @param operator
	 * @param value
	 */
	public Criteria_Condition(int id, int id_center, int id_criteria, String name, String operator, String value) {
		this.id = id;
		this.id_center = id_center;
		this.id_criteria = id_criteria;
		this.name = name;
		this.operator = operator;
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_criteria() {
		return id_criteria;
	}

	/**
	 *
	 * @param id_criteria
	 */
	public void setId_criteria(int id_criteria) {
		this.id_criteria = id_criteria;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 *
	 * @param operator
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
