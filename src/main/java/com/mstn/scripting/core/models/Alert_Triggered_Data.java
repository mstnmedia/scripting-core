package com.mstn.scripting.core.models;

/**
 *
 * @author amatos
 */
public class Alert_Triggered_Data {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_alert_triggered;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected String value;

	/**
	 *
	 */
	public Alert_Triggered_Data() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public Alert_Triggered_Data(String name, String value) {
		this.name = name;
		this.value = value;
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_alert_triggered
	 * @param name
	 * @param value
	 */
	public Alert_Triggered_Data(int id, int id_center, int id_alert_triggered, String name, String value) {
		this.id = id;
		this.id_center = id_center;
		this.id_alert_triggered = id_alert_triggered;
		this.name = name;
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_alert_triggered() {
		return id_alert_triggered;
	}

	/**
	 *
	 * @param id_alert_triggered
	 */
	public void setId_alert_triggered(int id_alert_triggered) {
		this.id_alert_triggered = id_alert_triggered;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
