/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.mstn.scripting.core.HTTP;
import com.mstn.scripting.core.JSON;
import java.util.List;
import org.apache.http.client.HttpClient;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author josesuero
 */
public class Content {

	private int id;
	private int id_center;
	@NotEmpty
	private String name;
	private String center_name;
	private String tags;
	private int id_type;
	private String value;
	private String filename;
	private boolean deleted;

	private List<Content_Language> content_language;

	/**
	 *
	 */
	public Content() {

	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param center_name
	 * @param name
	 * @param tags
	 * @param id_type
	 * @param value
	 * @param filename
	 * @param deleted
	 */
	public Content(int id, int id_center, String center_name, String name, String tags,
			int id_type, String value, String filename, boolean deleted) {
		this.id = id;
		this.id_center = id_center;
		this.center_name = center_name;
		this.name = name;
		this.tags = tags;
		this.id_type = id_type;
		this.value = value;
		this.filename = filename;
		this.deleted = deleted;
	}

	/**
	 *
	 * @param id_content
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Content getContent(int id_content, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(id_content));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/workflows/content/getContent",
				strPayload,
				bearerToken
		);
		return JSON.toObject(strResponse, Content.class);
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getTags() {
		return tags;
	}

	/**
	 *
	 * @param tags
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 *
	 * @return
	 */
	public int getId_type() {
		return id_type;
	}

	/**
	 *
	 * @param id_type
	 */
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 *
	 * @param filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 *
	 * @return
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 *
	 * @param deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 *
	 * @return
	 */
	public List<Content_Language> getContent_language() {
		return content_language;
	}

	/**
	 *
	 * @param content_language
	 */
	public void setContent_language(List<Content_Language> content_language) {
		this.content_language = content_language;
	}

}
