package com.mstn.scripting.core.models;

/**
 *
 * @author amatos
 */
public class Alert_Column {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_alert;

	/**
	 *
	 */
	protected String column;

	/**
	 *
	 */
	public Alert_Column() {
	}

	/**
	 *
	 * @param id
	 * @param id_alert
	 * @param column
	 */
	public Alert_Column(int id, int id_alert, String column) {
		this.id = id;
		this.id_alert = id_alert;
		this.column = column;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_alert() {
		return id_alert;
	}

	/**
	 *
	 * @param id_alert
	 */
	public void setId_alert(int id_alert) {
		this.id_alert = id_alert;
	}

	/**
	 *
	 * @return
	 */
	public String getColumn() {
		return column;
	}

	/**
	 *
	 * @param column
	 */
	public void setColumn(String column) {
		this.column = column;
	}

}
