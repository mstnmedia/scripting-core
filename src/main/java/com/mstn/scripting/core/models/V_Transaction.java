package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.enums.TransactionStates;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 *
 * @author amatos
 */
public class V_Transaction {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected String center_name;

	/**
	 *
	 */
	protected String id_call;

	/**
	 *
	 */
	protected int id_category;

	/**
	 *
	 */
	protected int id_indicator;

	/**
	 *
	 */
	protected int id_workflow;

	/**
	 *
	 */
	protected int id_workflow_version;

	/**
	 *
	 */
	protected int id_employee;

	/**
	 *
	 */
	protected String id_customer;

	/**
	 *
	 */
	protected String id_subscription;

	/**
	 *
	 */
	protected int id_current_step;

	/**
	 *
	 */
	protected Date start_date;

	/**
	 *
	 */
	protected Date end_date;

	/**
	 *
	 */
	protected int end_cause;

	/**
	 *
	 */
	protected int end_id_employee;

	/**
	 *
	 */
	protected int state;

	/**
	 *
	 */
	protected String subscriber_no;

	/**
	 *
	 */
	protected String caller_alt_phone;

	/**
	 *
	 */
	protected String note;

	/**
	 *
	 */
	protected String workflow_name;

	/**
	 *
	 */
	protected String version_id_version;

	/**
	 *
	 */
	protected String workflow_step_name;

	/**
	 *
	 */
	protected Date workflow_step_entry_date;

	/**
	 *
	 */
	protected String employee_name;

	/**
	 *
	 */
	protected String category_name;

	/**
	 *
	 */
	protected String end_cause_name;

	/**
	 *
	 */
	protected String end_employee_name;

	private List<Transaction_Result> transaction_results;
	private List<V_Transaction_Step> transaction_steps;

	private Employee employee;
	private Workflow workflow;
	private Workflow_Step workflow_step;
	private Category category;
	private Client client;
	private Service service;

	/**
	 *
	 */
	public V_Transaction() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param center_name
	 * @param id_call
	 * @param id_category
	 * @param id_indicator
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param id_employee
	 * @param id_customer
	 * @param id_subscription
	 * @param id_current_step
	 * @param start_date
	 * @param end_date
	 * @param end_cause
	 * @param end_id_employee
	 * @param state
	 * @param subscriber_no
	 * @param caller_alt_phone
	 * @param note
	 * @param workflow_name
	 * @param version_id_version
	 * @param workflow_step_name
	 * @param workflow_step_entry_date
	 * @param employee_name
	 * @param category_name
	 * @param end_cause_name
	 * @param end_employee_name
	 */
	public V_Transaction(int id, int id_center, String center_name, String id_call, int id_category,
			int id_indicator, int id_workflow, int id_workflow_version, int id_employee,
			String id_customer, String id_subscription, int id_current_step,
			Date start_date, Date end_date, int end_cause, int end_id_employee, int state,
			String subscriber_no, String caller_alt_phone, String note,
			String workflow_name, String version_id_version, String workflow_step_name,
			Date workflow_step_entry_date, String employee_name, String category_name, 
			String end_cause_name, String end_employee_name) {
		this.id = id;
		this.id_center = id_center;
		this.center_name = center_name;
		this.id_call = id_call;
		this.id_category = id_category;
		this.id_indicator = id_indicator;
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.id_employee = id_employee;
		this.id_customer = id_customer;
		this.id_subscription = id_subscription;
		this.id_current_step = id_current_step;
		this.start_date = start_date;
		this.end_date = end_date;
		this.end_cause = end_cause;
		this.end_id_employee = end_id_employee;
		this.state = state;
		this.subscriber_no = subscriber_no;
		this.caller_alt_phone = caller_alt_phone;
		this.note = note;
		this.workflow_name = workflow_name;
		this.version_id_version = version_id_version;
		this.workflow_step_name = workflow_step_name;
		this.workflow_step_entry_date = workflow_step_entry_date;
		this.employee_name = employee_name;
		this.category_name = category_name;
		this.end_cause_name = end_cause_name;
		this.end_employee_name = end_employee_name;
	}

	/**
	 *
	 * @param transaction
	 * @return
	 */
	static public List<Transaction_Result> getResults(V_Transaction transaction) {
		int ID = Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE;
		String prefix = Interfaces.TRANSACTION_INFO_INTERFACE_NAME + "_";

		boolean empty = transaction == null;
		String id = empty ? "" : String.valueOf(transaction.getId());
		String id_call = empty ? "" : String.valueOf(transaction.getId_call());
		String id_center = empty ? "" : String.valueOf(transaction.getId_center());
		String center_name = empty ? "" : String.valueOf(transaction.getCenter_name());
		String id_employee = empty ? "" : String.valueOf(transaction.getId_employee());
		String employee_name = empty ? "" : String.valueOf(transaction.getEmployee_name());
		String id_workflow = empty ? "" : String.valueOf(transaction.getId_workflow());
		String workflow_name = empty ? "" : String.valueOf(transaction.getWorkflow_name());
		String id_workflow_version = empty ? "" : String.valueOf(transaction.getId_workflow_version());
		String version_id_version = empty ? "" : String.valueOf(transaction.getVersion_id_version());
		String id_category = empty ? "" : String.valueOf(transaction.getId_category());
		String category_name = empty ? "" : String.valueOf(transaction.getCategory_name());
		String id_indicator = empty ? "" : String.valueOf(transaction.getId_indicator());
		String start_date = empty ? "" : DATE.toString(transaction.getStart_date());

		List<Transaction_Result> results = new ArrayList(Arrays.asList(
				//(id, id_interface, name, label, value, props)
				new Transaction_Result(0, ID, prefix + "id_transaction", "ID", id, ""),
				new Transaction_Result(0, ID, prefix + "id_call", "ID de llamada", id_call, ""),
				new Transaction_Result(0, ID, prefix + "id_center", "ID de centro", id_center, ""),
				new Transaction_Result(0, ID, prefix + "center_name", "Nombre de centro", center_name, ""),
				new Transaction_Result(0, ID, prefix + "id_employee", "ID de empleado", id_employee, ""),
				new Transaction_Result(0, ID, prefix + "employee_name", "Nombre de empleado", employee_name, ""),
				new Transaction_Result(0, ID, prefix + "workflow_id", "ID de flujo", id_workflow, ""),
				new Transaction_Result(0, ID, prefix + "workflow_name", "Nombre del flujo", workflow_name, ""),
				new Transaction_Result(0, ID, prefix + "version_id", "ID de la versión del flujo", id_workflow_version, ""),
				new Transaction_Result(0, ID, prefix + "version_name", "Versión del flujo", version_id_version, ""),
				new Transaction_Result(0, ID, Interfaces.ID_CATEGORY, "ID de la categoría", id_category, ""),
				new Transaction_Result(0, ID, Interfaces.CATEGORY_NAME, "Nombre de la categoría", category_name, ""),
				new Transaction_Result(0, ID, prefix + "id_indicator", "ID del indicador", id_indicator, ""),
				new Transaction_Result(0, ID, prefix + "start_date", "Fecha de inicio", start_date, "")
		));
		results.addAll(getStepResults(transaction));
		results.addAll(getEndResults(transaction));
		return results;
	}

	/**
	 *
	 * @param transaction
	 * @return
	 */
	static public List<Transaction_Result> getStepResults(V_Transaction transaction) {
		int ID = Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE;
		String prefix = Interfaces.TRANSACTION_INFO_INTERFACE_NAME + "_";

		boolean empty = transaction == null;
		String id_current_step = empty ? "" : String.valueOf(transaction.getId_current_step());
		String workflow_step_name = empty ? "" : String.valueOf(transaction.getWorkflow_step_name());
		String workflow_step_entry_date = empty ? "" : DATE.toString(transaction.getWorkflow_step_entry_date());

		return Arrays.asList(
				//(id, id_interface, name, label, value, props)
				new Transaction_Result(8, ID, prefix + "id_current_step", "ID del paso actual", id_current_step, ""),
				new Transaction_Result(9, ID, prefix + "workflow_step_name", "Nombre del paso actual", workflow_step_name, ""),
				new Transaction_Result(10, ID, prefix + "workflow_step_entry_date", "Fecha de entrada al paso", workflow_step_entry_date, "")
		);
	}

	/**
	 *
	 * @param transaction
	 * @return
	 */
	static public List<Transaction_Result> getEndResults(V_Transaction transaction) {
		int ID = Interfaces.TRANSACTION_INFO_INTERFACE_ID_BASE;
		String prefix = Interfaces.TRANSACTION_INFO_INTERFACE_NAME + "_";

		boolean empty = transaction == null;
		String id_state = empty ? "" : String.valueOf(transaction.getState());
		String state_name = "";
		if (!empty) {
			TransactionStates[] states = TransactionStates.values();
			for (TransactionStates tState : states) {
				if (tState.getValue() == transaction.getState()) {
					state_name = tState.name();
					break;
				}
			}
		}
		String end_date = empty ? "" : DATE.toString(transaction.getEnd_date());
		String id_end_cause = empty ? "" : String.valueOf(transaction.getEnd_cause());
		String end_cause_name = empty ? "" : String.valueOf(transaction.getEnd_cause_name());
		String end_id_employee = empty ? "" : String.valueOf(transaction.getEnd_id_employee());
		String end_employee_name = empty ? "" : String.valueOf(transaction.getEnd_employee_name());
		String note = empty ? "" : String.valueOf(transaction.getNote());
		return Arrays.asList(
				//(id, id_interface, name, label, value, props)
				new Transaction_Result(0, ID, prefix + "id_state", "ID del estado", id_state, ""),
				new Transaction_Result(0, ID, prefix + "state_name", "Nombre del estado", state_name, ""),
				new Transaction_Result(0, ID, prefix + "end_date", "Fecha de fin", end_date, ""),
				new Transaction_Result(0, ID, prefix + "id_end_cause", "ID de la razón de fin", id_end_cause, ""),
				new Transaction_Result(0, ID, prefix + "end_cause_name", "Nombre de la razón de fin", end_cause_name, ""),
				new Transaction_Result(0, ID, prefix + "end_id_employee", "Tarjeta del empleado que finalizó", end_id_employee, ""),
				new Transaction_Result(0, ID, prefix + "end_employee_name", "Nombre del empleado que finalizó", end_employee_name, ""),
				new Transaction_Result(0, ID, prefix + "end_cause_name", "Nombre de la razón de fin", end_cause_name, ""),
				new Transaction_Result(0, ID, prefix + "note", "Nota", note, "{ \"cssStyles\": \"white-space: pre-wrap;\" }")
		);
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

	/**
	 *
	 * @return
	 */
	public String getId_call() {
		return id_call;
	}

	/**
	 *
	 * @param id_call
	 */
	public void setId_call(String id_call) {
		this.id_call = id_call;
	}

	/**
	 *
	 * @return
	 */
	public int getId_category() {
		return id_category;
	}

	/**
	 *
	 * @param id_category
	 */
	public void setId_category(int id_category) {
		this.id_category = id_category;
	}

	/**
	 *
	 * @return
	 */
	public int getId_indicator() {
		return id_indicator;
	}

	/**
	 *
	 * @param id_indicator
	 */
	public void setId_indicator(int id_indicator) {
		this.id_indicator = id_indicator;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 *
	 * @param id_workflow
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_version() {
		return id_workflow_version;
	}

	/**
	 *
	 * @param id_workflow_version
	 */
	public void setId_workflow_version(int id_workflow_version) {
		this.id_workflow_version = id_workflow_version;
	}

	/**
	 *
	 * @return
	 */
	public int getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	public String getId_customer() {
		return id_customer;
	}

	/**
	 *
	 * @param id_customer
	 */
	public void setId_customer(String id_customer) {
		this.id_customer = id_customer;
	}

	/**
	 *
	 * @return
	 */
	public String getId_subscription() {
		return id_subscription;
	}

	/**
	 *
	 * @param id_subscription
	 */
	public void setId_subscription(String id_subscription) {
		this.id_subscription = id_subscription;
	}

	/**
	 *
	 * @return
	 */
	public int getId_current_step() {
		return id_current_step;
	}

	/**
	 *
	 * @param id_current_step
	 */
	public void setId_current_step(int id_current_step) {
		this.id_current_step = id_current_step;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getStart_date() {
		return start_date;
	}

	/**
	 *
	 * @param start_date
	 */
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getEnd_date() {
		return end_date;
	}

	/**
	 *
	 * @param end_date
	 */
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	/**
	 *
	 * @return
	 */
	public int getEnd_cause() {
		return end_cause;
	}

	/**
	 *
	 * @param end_cause
	 */
	public void setEnd_cause(int end_cause) {
		this.end_cause = end_cause;
	}

	/**
	 *
	 * @return
	 */
	public int getState() {
		return state;
	}

	/**
	 *
	 * @param state
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 *
	 * @return
	 */
	public String getSubscriber_no() {
		return subscriber_no;
	}

	/**
	 *
	 * @param subscriber_no
	 */
	public void setSubscriber_no(String subscriber_no) {
		this.subscriber_no = subscriber_no;
	}

	/**
	 *
	 * @return
	 */
	public String getCaller_alt_phone() {
		return caller_alt_phone;
	}

	/**
	 *
	 * @param caller_alt_phone
	 */
	public void setCaller_alt_phone(String caller_alt_phone) {
		this.caller_alt_phone = caller_alt_phone;
	}

	/**
	 *
	 * @return
	 */
	public String getNote() {
		return note;
	}

	/**
	 *
	 * @param note
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_name() {
		return workflow_name;
	}

	/**
	 *
	 * @param workflow_name
	 */
	public void setWorkflow_name(String workflow_name) {
		this.workflow_name = workflow_name;
	}

	/**
	 *
	 * @return
	 */
	public String getVersion_id_version() {
		return version_id_version;
	}

	/**
	 *
	 * @param version_id_version
	 */
	public void setVersion_id_version(String version_id_version) {
		this.version_id_version = version_id_version;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_step_name() {
		return workflow_step_name;
	}

	/**
	 *
	 * @param workflow_step_name
	 */
	public void setWorkflow_step_name(String workflow_step_name) {
		this.workflow_step_name = workflow_step_name;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getWorkflow_step_entry_date() {
		return workflow_step_entry_date;
	}

	/**
	 *
	 * @param workflow_step_entry_date
	 */
	public void setWorkflow_step_entry_date(Date workflow_step_entry_date) {
		this.workflow_step_entry_date = workflow_step_entry_date;
	}

	/**
	 *
	 * @return
	 */
	public String getEmployee_name() {
		return employee_name;
	}

	/**
	 *
	 * @param employee_name
	 */
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	/**
	 *
	 * @return
	 */
	public String getCategory_name() {
		return category_name;
	}

	/**
	 *
	 * @param category_name
	 */
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	/**
	 *
	 * @return
	 */
	public String getEnd_cause_name() {
		return end_cause_name;
	}

	/**
	 *
	 * @param end_cause_name
	 */
	public void setEnd_cause_name(String end_cause_name) {
		this.end_cause_name = end_cause_name;
	}

	/**
	 *
	 * @return
	 */
	public int getEnd_id_employee() {
		return end_id_employee;
	}

	/**
	 *
	 * @param end_id_employee
	 */
	public void setEnd_id_employee(int end_id_employee) {
		this.end_id_employee = end_id_employee;
	}

	/**
	 *
	 * @return
	 */
	public String getEnd_employee_name() {
		return end_employee_name;
	}

	/**
	 *
	 * @param end_employee_name
	 */
	public void setEnd_employee_name(String end_employee_name) {
		this.end_employee_name = end_employee_name;
	}

	/**
	 *
	 * @return
	 */
	public List<Transaction_Result> getTransaction_results() {
		return transaction_results;
	}

	/**
	 *
	 * @param transaction_results
	 */
	public void setTransaction_results(List<Transaction_Result> transaction_results) {
		this.transaction_results = transaction_results;
	}

	/**
	 *
	 * @return
	 */
	public List<V_Transaction_Step> getTransaction_steps() {
		return transaction_steps;
	}

	/**
	 *
	 * @param transaction_steps
	 */
	public void setTransaction_steps(List<V_Transaction_Step> transaction_steps) {
		this.transaction_steps = transaction_steps;
	}

	/**
	 *
	 * @return
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 *
	 * @param employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 *
	 * @return
	 */
	public Workflow getWorkflow() {
		return workflow;
	}

	/**
	 *
	 * @param workflow
	 */
	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	/**
	 *
	 * @return
	 */
	public Workflow_Step getWorkflow_step() {
		return workflow_step;
	}

	/**
	 *
	 * @param workflow_step
	 */
	public void setWorkflow_step(Workflow_Step workflow_step) {
		this.workflow_step = workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 *
	 * @param category
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 *
	 * @return
	 */
	public Client getClient() {
		return client;
	}

	/**
	 *
	 * @param client
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 *
	 * @return
	 */
	public Service getService() {
		return service;
	}

	/**
	 *
	 * @param service
	 */
	public void setService(Service service) {
		this.service = service;
	}

}
