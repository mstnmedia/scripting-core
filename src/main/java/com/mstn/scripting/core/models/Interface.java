package com.mstn.scripting.core.models;

import com.mstn.scripting.core.HTTP;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import java.util.HashMap;
import java.util.List;
import org.apache.http.client.HttpClient;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author amatos
 */
public class Interface {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	@NotEmpty
	protected String name;

	/**
	 *
	 */
	protected String label;

	/**
	 *
	 */
	protected String server_url;

	/**
	 *
	 */
	protected String value_step;

	/**
	 *
	 */
	protected boolean keep_results;

	/**
	 *
	 */
	protected boolean deleted;

	private List<Interface_Option> interface_option;
	private List<Interface_Field> interface_field;
	private List<Transaction_Result> interface_results;
	private List<Interface_Service> interface_services;
	private String form;
	private HashMap<String, Transaction_Result> mapResults;

	/**
	 *
	 */
	public Interface() {
	}
//
//	public Interface(int id, String name, String label, String server_url, String value_step, boolean deleted) {
//		this.id = id;
//		this.name = name;
//		this.label = label;
//		this.server_url = server_url;
//		this.value_step = value_step;
//		this.deleted = deleted;
//	}

	/**
	 *
	 * @param id
	 * @param name
	 * @param label
	 * @param server_url
	 * @param value_step
	 * @param keepResults
	 * @param deleted
	 */
	public Interface(int id, String name, String label, String server_url, String value_step, boolean keepResults, boolean deleted) {
		this.id = id;
		this.name = name;
		this.label = label;
		this.server_url = server_url;
		this.value_step = value_step;
		this.keep_results = keepResults;
		this.deleted = deleted;
	}

	/**
	 *
	 * @param id
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public Interface getInterface(int id, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(id));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/interfaces/interfaceBase/getInterface",
				strPayload,
				bearerToken
		);
		System.err.println(strResponse);
		return JSON.toObject(strResponse, Interface.class);
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 *
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 *
	 * @return
	 */
	public String getServer_url() {
		return server_url;
	}

	/**
	 *
	 * @param server_url
	 */
	public void setServer_url(String server_url) {
		this.server_url = server_url;
	}

	/**
	 *
	 * @return
	 */
	public String getValue_step() {
		return value_step;
	}

	/**
	 *
	 * @param value_step
	 */
	public void setValue_step(String value_step) {
		this.value_step = value_step;
	}

	/**
	 *
	 * @return
	 */
	public boolean isKeep_results() {
		return keep_results;
	}

	/**
	 *
	 * @param keep_results
	 */
	public void setKeep_results(boolean keep_results) {
		this.keep_results = keep_results;
	}

	/**
	 *
	 * @return
	 */
	public Boolean getDeleted() {
		return deleted;
	}

	/**
	 *
	 * @param deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 *
	 * @return
	 */
	public List<Interface_Option> getInterface_option() {
		return interface_option;
	}

	/**
	 *
	 * @param interface_option
	 */
	public void setInterface_option(List<Interface_Option> interface_option) {
		this.interface_option = interface_option;
	}

	/**
	 *
	 * @return
	 */
	public List<Interface_Field> getInterface_field() {
		return interface_field;
	}

	/**
	 *
	 * @param interface_field
	 */
	public void setInterface_field(List<Interface_Field> interface_field) {
		this.interface_field = interface_field;
	}

	/**
	 *
	 * @return
	 */
	public List<Transaction_Result> getInterface_results() {
		return interface_results;
	}

	/**
	 *
	 * @param interface_results
	 */
	public void setInterface_results(List<Transaction_Result> interface_results) {
		this.interface_results = interface_results;
		mapResults = new HashMap();
		if (Utils.listNonNullOrEmpty(interface_results)) {
			interface_results.forEach(result -> mapResults.put(result.getName(), result));
		}
	}

	/**
	 *
	 * @return
	 */
	public List<Interface_Service> getInterface_services() {
		return interface_services;
	}

	/**
	 *
	 * @param interface_services
	 */
	public void setInterface_services(List<Interface_Service> interface_services) {
		this.interface_services = interface_services;
	}

	/**
	 *
	 * @return
	 */
	public String getForm() {
		return form;
	}

	/**
	 *
	 * @param form
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 *
	 * @return
	 */
	public HashMap<String, Transaction_Result> getMapResults() {
		return mapResults;
	}

	/**
	 *
	 * @param mapResults
	 */
	public void setMapResults(HashMap<String, Transaction_Result> mapResults) {
		this.mapResults = mapResults;
	}

}
