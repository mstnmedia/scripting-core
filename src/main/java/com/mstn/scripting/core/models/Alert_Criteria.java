package com.mstn.scripting.core.models;

/**
 *
 * @author amatos
 */
public class Alert_Criteria {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_alert;

	/**
	 *
	 */
	protected String column;

	/**
	 *
	 */
	protected String value;

	/**
	 *
	 */
	public Alert_Criteria() {
	}

	/**
	 *
	 * @param id
	 * @param id_alert
	 * @param column
	 * @param value
	 */
	public Alert_Criteria(int id, int id_alert, String column, String value) {
		this.id = id;
		this.id_alert = id_alert;
		this.column = column;
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_alert() {
		return id_alert;
	}

	/**
	 *
	 * @param id_alert
	 */
	public void setId_alert(int id_alert) {
		this.id_alert = id_alert;
	}

	/**
	 *
	 * @return
	 */
	public String getColumn() {
		return column;
	}

	/**
	 *
	 * @param column
	 */
	public void setColumn(String column) {
		this.column = column;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
