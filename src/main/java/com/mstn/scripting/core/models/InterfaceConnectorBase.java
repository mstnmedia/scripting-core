/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.DATE;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import com.mstn.scripting.core.enums.WorkflowStepInterfaceFieldSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author amatos
 */
public abstract class InterfaceConnectorBase implements InterfaceConnector {

	@NotEmpty
	private final int ID;
	@NotEmpty
	private final String NAME;
	@NotEmpty
	private final String LABEL;
	private final String SERVER_URL;
	private final String VALUE_STEP;
	private final boolean KEEP_RESULTS;
	private final boolean DELETED;

	/**
	 *
	 */
	protected final String prefix;

	/**
	 *
	 * @param ID
	 * @param NAME
	 * @param LABEL
	 * @param SERVER_URL
	 * @param VALUE_STEP
	 * @param DELETED
	 */
	public InterfaceConnectorBase(int ID, String NAME, String LABEL, String SERVER_URL, String VALUE_STEP, boolean DELETED) {
		this.ID = ID;
		this.NAME = NAME;
		this.LABEL = LABEL;
		this.SERVER_URL = SERVER_URL;
		this.VALUE_STEP = VALUE_STEP;
		this.KEEP_RESULTS = false;
		this.DELETED = DELETED;

		this.prefix = NAME + "_";
		this.configFileName = NAME + "-config.properties";
	}

	/**
	 *
	 * @return
	 */
	public List<Interface_Field> getFields() {
		return Arrays.asList();
	}

	/**
	 *
	 * @return @throws Exception
	 */
	public abstract List<Transaction_Result> getResults() throws Exception;

	/**
	 *
	 * @return
	 */
	@Override
	public Interface getInterface() {
		return new Interface(ID, NAME, LABEL, SERVER_URL, VALUE_STEP, KEEP_RESULTS, DELETED);
	}

	/**
	 *
	 * @return @throws Exception
	 */
	@Override
	public Interface getInterfaceWithChildren() throws Exception {
		inter = getInterface();
		inter.setInterface_field(getFields());
		inter.setInterface_results(getResults());
		return inter;
	}

	/**
	 *
	 * @return
	 */
	@Override
	public Interface getInterfaceWithFields() {
		inter = getInterface();
		inter.setInterface_field(getFields());
		return inter;
	}

	@Override
	public Interface getInterfaceWithResults() throws Exception {
		inter = getInterface();
		inter.setInterface_results(getResults());
		return inter;
	}

	/**
	 *
	 */
	protected V_Transaction transaction;

	/**
	 *
	 */
	protected List<Transaction_Result> results;

	/**
	 *
	 */
	protected Workflow_Step step;

	/**
	 *
	 */
	protected Interface_Service service;

	/**
	 *
	 */
	protected Interface inter;

	/**
	 *
	 */
	protected boolean formIsFilledOrValid;

	/**
	 *
	 */
	protected TransactionInterfacesResponse response = new TransactionInterfacesResponse();

	/**
	 *
	 */
	protected ObjectNode form = JSON.newObjectNode();

	protected User user = null;

	/**
	 *
	 * @param payload
	 * @param user
	 * @throws IOException
	 */
	protected void initializeFromPayload(TransactionInterfacesPayload payload, User user) throws IOException {
		this.user = user;
		transaction = payload.getTransaction();
		response.setId_transaction(transaction == null ? 0 : transaction.getId());

		step = payload.getWorkflow_step();
		service = payload.getInterface_service();

		inter = getInterface();
		response.setInterface(inter);

		form = JSON.toNode(payload.getForm());

		// Llamar estas dos funciones delante para preparar validaciones
		mapResults(transaction == null ? Arrays.asList() : transaction.getTransaction_results(), mapppedResults);
		prepareFormValues(form, step);
		formIsFilledOrValid = isValidForm(form, step);
	}

	/**
	 *
	 * @param step
	 * @return
	 */
	protected List<Workflow_Step_Interface_Field> getWSIFs(Workflow_Step step) {
		return step == null ? new ArrayList<>() : step.getWorkflow_step_interface_field();
	}

	/**
	 *
	 * @param WSIFs
	 * @param fieldID
	 * @return
	 */
	protected Workflow_Step_Interface_Field wsifByFieldID(List<Workflow_Step_Interface_Field> WSIFs, int fieldID) {
		for (int j = 0; j < WSIFs.size(); j++) {
			Workflow_Step_Interface_Field wsif = WSIFs.get(j);
			if (Utils.stringAreEquals(wsif.getId_interface_field(), String.valueOf(fieldID))) {
				return wsif;
			}
		}
		return null;
	}

	/**
	 *
	 * @param form
	 * @param step
	 */
	protected void prepareFormValues(ObjectNode form, Workflow_Step step) {
		//	LLenar formulario desde el formulario enviado, los valores fijos del paso, 
		//	valores default de los campos, y variables
		List<Workflow_Step_Interface_Field> WSIFs = getWSIFs(step);
		getFields().forEach(field -> {
			String fieldName = field.getName();
			if (!JSON.hasValue(form, fieldName)) {
				Workflow_Step_Interface_Field wsif = wsifByFieldID(WSIFs, field.getId());
				if (wsif != null) {
					if (Utils.stringAreEquals(wsif.getId_interface_source(), WorkflowStepInterfaceFieldSource.FIXED)) {
						form.put(fieldName, wsif.getId_interface_field_source());
					} else if (Utils.stringAreEquals(wsif.getId_interface_source(), WorkflowStepInterfaceFieldSource.USER)) {
						String fieldSourceName = wsif.getId_interface_field_source();
						String resultValue = mapppedResults.getOrDefault(fieldSourceName, "");
						form.put(fieldName, resultValue);
					}
				}
				if (!JSON.hasValue(form, fieldName)) {
					form.put(fieldName, field.getDefault_value());
				}
			}
		});
	}

	/**
	 *
	 * @param fields
	 * @param form
	 * @return
	 */
	protected boolean areAllFieldsFilled(List<Interface_Field> fields, ObjectNode form) {
		boolean filled = true;
		for (int i = 0; i < fields.size(); i++) {
			Interface_Field field = fields.get(i);
			String fieldName = field.getName();
			if (!JSON.hasValue(form, fieldName)) {
				filled = false;
				break;
			}
		}
		return filled;
	}

	/**
	 *
	 * @param form
	 * @param step
	 * @return
	 */
	protected boolean isValidForm(ObjectNode form, Workflow_Step step) {
		boolean isValid;
		List<Interface_Field> fields = getFields();
		List<Interface_Field> requireds = fields.stream().filter(i -> i.isRequired()).collect(Collectors.toList());
		List<Workflow_Step_Interface_Field> WSIFs = getWSIFs(step);

		List<Interface_Field> fieldsFilledByUser = new ArrayList<>();
		for (int i = 0; i < fields.size() && !WSIFs.isEmpty(); i++) {
			Interface_Field field = fields.get(i);
			Workflow_Step_Interface_Field wsif;
			for (int j = 0; j < WSIFs.size(); j++) {
				wsif = WSIFs.get(j);
				if (Utils.stringAreEquals(wsif.getId_interface_field(), String.valueOf(field.getId()))
						&& Utils.stringAreEquals(WorkflowStepInterfaceFieldSource.USER, wsif.getId_interface_source())) {
					fieldsFilledByUser.add(field);
					break;
				}
			}
		}
		if (fieldsFilledByUser.isEmpty()) {
			//	Validar si tiene datos que le faltan entre las variables o formulario 
			//	antes de enviar al sistema externo
			isValid = areAllFieldsFilled(requireds, form);
			baseLog(NAME + "isFormValid empty: " + Boolean.toString(isValid));
		} else {
			isValid = false;
			if (form.size() > 0) {//El formulario está siendo enviado con datos
				//	Validar que los campos que el usuario debe llenar están llenos
				isValid = areAllFieldsFilled(requireds, form) && areAllFieldsFilled(fieldsFilledByUser, form);
				baseLog(NAME + " isFormValid size: " + Boolean.toString(isValid));
			}
		}
		return isValid;
	}

	/**
	 *
	 * @param payload
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Override
	public TransactionInterfacesResponse getForm(TransactionInterfacesPayload payload, User user) throws Exception {
		try {
			baseLog(NAME + " payload: " + JSON.toString(payload));
			// Validar si el formulario está listo para enviarlo al webservice.
			// Si no está listo, se le enviará una lista de campos que el usuario debe llenar en la 
			// pantalla en una respuesta tipo SHOW_FORM; de lo contrario enviará el formulario al
			// webservice y enviará los Transaction_Result para guardarlos para la transacción en una
			// respuesta tipo NEXT_STEP. Si ocurre un error, la respuesta debe ser tipo ERROR.

			// Llamar esta función para leer los valores del payload, inicializar variables y
			// verificar la validez del formulario enviado
			initializeFromPayload(payload, user);

			baseLog(NAME + " formIsFilledOrValid: " + formIsFilledOrValid);
			if (!formIsFilledOrValid) {
				response.setAction(TransactionInterfaceActions.SHOW_FORM);

				return setAsShowFormResponse(response, inter, form);
			} else {
				response.setAction(TransactionInterfaceActions.NEXT_STEP);
				response.setInterface(inter);

				return setAsNextStepResponse();
			}
		} catch (Exception ex) {
			// Cuando haya una excepción en el proceso las interfaces deberán retornar un
			// TransactionInterfacesResponse que contenga el error mediante esta función.
			return setAsExceptionResponse(response, ex);
		}
	}

	/**
	 *
	 * @param response
	 * @param inter
	 * @param form
	 * @return
	 * @throws Exception
	 */
	protected TransactionInterfacesResponse setAsShowFormResponse(TransactionInterfacesResponse response, Interface inter, ObjectNode form) throws Exception {

		// LLenar formulario inicial desde el formulario enviado, los valores fijos del paso, 
		//		valores default de los campos, y variables
		List<Interface_Field> fields = getFields();
		fields.forEach(field -> {
			String fieldName = field.getName();
			if (!JSON.hasValue(form, fieldName) && Utils.stringNonNullOrEmpty(field.getDefault_value())) {
				form.put(field.getName(), field.getDefault_value());
			}
		});
		//	Si cambia el nombre de esta propiedad, debe actualizarse también en el componente
		//	TransactionStepInterface de la interfaz web
		form.put("__generationDate", DATE.nowAsLong());
		inter.setForm(JSON.toString(form));

		// Llenar aquí los campos que el usuario debe llenar, y los campos
		//		informativos con readonly = true
		inter.setInterface_field(fields);
		return response;
	}

	/**
	 *
	 * @throws Exception
	 */
	protected void getDataForResults() throws Exception {
		// Enviar aquí el formulario al webservice del sistema externo para
		// obtener los datos con que se guardarán como Transaction_Result.
	}

	/**
	 *
	 * @return @throws Exception
	 */
	protected TransactionInterfacesResponse setAsNextStepResponse() throws Exception {
		// Obtiene los datos necesarios para formar las variables de la transacción.
		getDataForResults();

		// Llenar aquí los resultados con name, label y value
		results = getResults();
		results.forEach(result -> {// Cada interfaz debe devolver resultados con su propio ID
			// Transactions asignará valor a las demás propiedades IDs que referencian los resultados
			result.setId_interface(inter.getId());
		});
		// Agregar los resultados al TransactionInterfacesResponse para guardarlos.
		// (migrar: anteriormente se guardaban en la propiedad de la interfaz)
		response.setResults(results);

		// Obtener aquí el valor de respuesta correspondiente para  mover el paso invocando
		// la función getValueFromJavascript() para ejecutar código javascript del servicio.
		// (migrar: Inicialmente se respondía de los valores de la interfaz en getOptions());
		String value = getValue();
		response.setValue(value);
		return response;
	}

	/**
	 *
	 * @param response
	 * @param form
	 * @return
	 */
	protected TransactionInterfacesResponse setAsMissingRequest(TransactionInterfacesResponse response, ObjectNode form) {
		return setAsMissingRequest(response, response.getJson(), form);
	}

	/**
	 *
	 * @param response
	 * @param json
	 * @param form
	 * @return
	 */
	protected TransactionInterfacesResponse setAsMissingRequest(TransactionInterfacesResponse response, ObjectNode json, ObjectNode form) {
		response.setAction(TransactionInterfaceActions.INVALID_REQUEST);
		response.setValue(InterfaceOptions.MISSING_DATA);
		ArrayNode missing_fields = json.putArray("missing_fields");
		getFields().forEach(field -> {
			if (field.isRequired() && !form.hasNonNull(field.getName())) {
				missing_fields.add(field.getName());
			}
		});
		return response;
	}

	/**
	 *
	 * @param response
	 * @param ex
	 * @return
	 */
	protected TransactionInterfacesResponse setAsExceptionResponse(TransactionInterfacesResponse response, Exception ex) {
		return setAsExceptionResponse(this.getClass(), response, ex);
	}

	/**
	 *
	 * @param clazz
	 * @param response
	 * @param ex
	 * @return
	 */
	static public TransactionInterfacesResponse setAsExceptionResponse(Class clazz, TransactionInterfacesResponse response, Exception ex) {
		Utils.logException(clazz, "setAsExceptionResponse", ex);
		response.setAction(TransactionInterfaceActions.ERROR);
		response.setValue(InterfaceOptions.ERROR);
		JsonNode error = JSON.toJsonNode(ex);
		response.getJson().set("error", error);
		response.getLogs().add(new Log("InterfaceConnectorBase", "error", "Error en la clase " + clazz.getName(), error.toString()));
		return response;
	}

	/**
	 *
	 */
	protected final String GROUP_ID = "group_id";

	/**
	 *
	 */
	protected final String ENTITY_ID = "entity_id";

	/**
	 *
	 */
	protected final String CUSTOMER_ID = "customer_id";

	/**
	 *
	 * @param form
	 * @return
	 */
	protected String getCustomerIDFromForm(ObjectNode form) {
		String customerID;
		if (form.hasNonNull(GROUP_ID) && form.hasNonNull(ENTITY_ID)) {
			customerID = JSON.getString(form, GROUP_ID) + "_" + JSON.getString(form, ENTITY_ID);
		} else {
			customerID = JSON.getString(form, CUSTOMER_ID);
		}
		return customerID;
	}

	/**
	 *
	 * @return
	 */
	protected List<Interface_Field> getCustomerFields() {
		return Arrays.asList(//(id, id_interface, name, label, id_type, order_index, default_value)
				new Interface_Field(1, ID, CUSTOMER_ID, "ID de Cliente", InterfaceFieldTypes.TEXT, 1, null),
				new Interface_Field(2, ID, GROUP_ID, "ID de Grupo", InterfaceFieldTypes.TEXT, 2, null),
				new Interface_Field(3, ID, ENTITY_ID, "ID de Entidad", InterfaceFieldTypes.TEXT, 3, null)
		);
	}

	/**
	 *
	 */
	protected final String PAGE_NUMBER = "page_number";

	/**
	 *
	 */
	protected int pageNum = 1;

	/**
	 *
	 * @param form
	 * @return
	 */
	protected int getPageNumFromForm(ObjectNode form) {
		return form.hasNonNull(PAGE_NUMBER) ? form.get(PAGE_NUMBER).asInt(1) : 1;
	}

	/**
	 *
	 */
	protected boolean showBaseLogs;

	/**
	 *
	 * @param log
	 */
	protected void baseLog(Object log) {
		if (showBaseLogs) {
			System.out.println(log);
		}
	}

	/**
	 *
	 * @return @throws Exception
	 */
	protected String getValue() throws Exception {
		return service == null ? "true" : getValueFromJavascript();
	}

	// Para invocar esta función se debería ya haber invocado initializeFromPayload o
	// haberle asignado valor a las propiedades transaction, results y service
	/**
	 *
	 * @return @throws Exception
	 */
	protected String getValueFromJavascript() throws Exception {
		baseLog(">>> Executing Javascript code");
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
		try {
			String serviceCode = service == null ? null : service.getCode();
			List<Transaction_Result> transaction_results = transaction == null
					? Arrays.asList() : transaction.getTransaction_results();
			HashMap<String, String> map = mapResults(transaction_results);
			map = mapResults(results, map);

			StringBuilder js = new StringBuilder();
			js.append("var getServiceValue = function(transaction, results, objResults){\n");
			js.append(serviceCode);
			js.append("\n}");
			String executableCode = js.toString();
			baseLog(executableCode);

			engine.eval(executableCode);
			Invocable invocable = (Invocable) engine;
			Object result = invocable.invokeFunction("getServiceValue", transaction, results, map);
			baseLog(result);

			return String.valueOf(result);
		} catch (Exception ex) {
			Utils.logException(this.getClass(), "Error at getValueFromJavascript", ex);
			return InterfaceOptions.ERROR;
		}
	}

	/**
	 *
	 */
	protected HashMap<String, String> mapppedResults = new HashMap<>();

	/**
	 *
	 * @param results
	 * @return
	 */
	protected HashMap<String, String> mapResults(List<Transaction_Result> results) {
		return mapResults(results, new HashMap<>());
	}

	/**
	 *
	 * @param results
	 * @param map
	 * @return
	 */
	protected HashMap<String, String> mapResults(List<Transaction_Result> results, HashMap<String, String> map) {
		if (map == null) {
			map = new HashMap<>();
		}
		if (results == null) {
			return map;
		}
		for (int i = 0; i < results.size(); i++) {
			Transaction_Result result = results.get(i);
			map.put(result.getName(), result.getValue());
		}
		return map;
	}

	/**
	 *
	 * @return
	 */
	public int getID() {
		return ID;
	}

	/**
	 *
	 * @return
	 */
	public String getNAME() {
		return NAME;
	}

	/**
	 *
	 * @return
	 */
	public String getLABEL() {
		return LABEL;
	}

	/**
	 *
	 * @return
	 */
	public String getSERVER_URL() {
		return SERVER_URL;
	}

	/**
	 *
	 * @return
	 */
	public String getVALUE_STEP() {
		return VALUE_STEP;
	}

	/**
	 *
	 * @return
	 */
	public boolean isKEEP_RESULTS() {
		return KEEP_RESULTS;
	}

	/**
	 *
	 * @return
	 */
	public boolean isDELETED() {
		return DELETED;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	@Override
	public boolean test() throws Exception {
		try {
			testFields();
			testResults();
		} catch (Exception ex) {
			Utils.logException(this.getClass(), NAME + " test", ex);
			return false;
		}
		return true;
	}

	/**
	 *
	 */
	protected HashMap<Integer, String> testMap;

	/**
	 *
	 * @return
	 */
	protected boolean testFields() {
		testMap = new HashMap<>();
		List<Interface_Field> fields = getFields();
		for (int i = 0; i < fields.size(); i++) {
			Interface_Field field = fields.get(i);
			if (testMap.containsKey(field.getId()) || testMap.containsValue(field.getName())) {
				return false;
			}
			testMap.put(field.getId(), field.getName());
		}
		return true;
	}

	/**
	 *
	 * @return @throws Exception
	 */
	protected boolean testResults() throws Exception {
		testMap = new HashMap<>();
		List<Transaction_Result> returnedResults = getResults();
		for (int i = 0; i < returnedResults.size(); i++) {
			Transaction_Result result = returnedResults.get(i);
			if (testMap.containsKey(result.getId()) || testMap.containsValue(result.getName())) {
				return false;
			}
			testMap.put(result.getId(), result.getName());
		}
		return true;
	}

	/**
	 *
	 */
	protected Properties config = new Properties();

	/**
	 *
	 */
	protected InputStream inputStream;

	/**
	 *
	 */
	protected String configFileName;

	/**
	 *
	 * @throws Exception
	 */
	protected void loadConfigProps() throws Exception {
		File configFile = new File(configFileName);
		try {
			if (inputStream == null) {
				inputStream = new FileInputStream(configFileName);
				config.load(inputStream);
			}
		} catch (Exception ex) {
			FileNotFoundException exNF = new FileNotFoundException(
					"Properties file '" + configFile.getAbsolutePath() + "' not found"
			);
			exNF.initCause(ex);
			throw exNF;
		}
	}

}
