package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.JSON;
import java.util.Date;

/**
 *
 * @author amatos
 */
public class Alert_Result {

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_alert;

	/**
	 *
	 */
	protected Date first_transaction_date;

	/**
	 *
	 */
	protected String columns;

	/**
	 *
	 */
	protected int quantity;

	/**
	 *
	 */
	public Alert_Result() {
	}

	/**
	 *
	 * @param id_center
	 * @param id_alert
	 * @param first_transaction_date
	 * @param columns
	 * @param quantity
	 */
	public Alert_Result(int id_center, int id_alert, Date first_transaction_date, String columns, int quantity) {
		this.id_center = id_center;
		this.id_alert = id_alert;
		this.first_transaction_date = first_transaction_date;
		this.columns = columns;
		this.quantity = quantity;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_alert() {
		return id_alert;
	}

	/**
	 *
	 * @param id_alert
	 */
	public void setId_alert(int id_alert) {
		this.id_alert = id_alert;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getFirst_transaction_date() {
		return first_transaction_date;
	}

	/**
	 *
	 * @param first_transaction_date
	 */
	public void setFirst_transaction_date(Date first_transaction_date) {
		this.first_transaction_date = first_transaction_date;
	}

	/**
	 *
	 * @return
	 */
	public String getColumns() {
		return columns;
	}

	/**
	 *
	 * @param columns
	 */
	public void setColumns(String columns) {
		this.columns = columns;
	}

	/**
	 *
	 * @return
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 *
	 * @param quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
