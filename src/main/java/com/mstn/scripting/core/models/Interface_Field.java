/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.enums.InterfaceFieldTypes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author josesuero
 */
public class Interface_Field {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_interface;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected String label;

	/**
	 *
	 */
	protected int id_type;

	/**
	 *
	 */
	protected int order_index;

	/**
	 *
	 */
	protected boolean required;

	/**
	 *
	 */
	protected boolean readonly;

	/**
	 *
	 */
	protected String default_value;

	/**
	 *
	 */
	protected String grid;

	/**
	 *
	 */
	protected String source;

	/**
	 *
	 */
	protected String onvalidate;

	/**
	 *
	 */
	protected String onchange;

	/**
	 *
	 */
	protected String onfocus;

	/**
	 *
	 */
	protected String onblur;

	/**
	 *
	 */
	protected List<Interface_Field> children = new ArrayList();

	/**
	 *
	 */
	protected Interface Interface;

	/**
	 *
	 */
	public Interface_Field() {
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param id_type
	 * @param required
	 */
	public Interface_Field(int id, int id_interface, String name, String label, int id_type, boolean required) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		setId_type(id_type);
		this.required = required;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param id_type
	 * @param order_index
	 * @param default_value
	 */
	public Interface_Field(int id, int id_interface, String name, String label, int id_type, int order_index, String default_value) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		this.setId_type(id_type);
		this.order_index = order_index;
		this.default_value = default_value;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param id_type
	 * @param source
	 * @param order_index
	 * @param default_value
	 */
	public Interface_Field(int id, int id_interface, String name, String label, int id_type, String source, int order_index, String default_value) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		setId_type(id_type);
		this.source = source;
		this.order_index = order_index;
		this.default_value = default_value;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param id_type
	 * @param source
	 * @param order_index
	 * @param required
	 */
	public Interface_Field(int id, int id_interface, String name, String label, int id_type, String source, int order_index, boolean required) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		this.setId_type(id_type);
		this.source = source;
		this.order_index = order_index;
		this.required = required;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param id_type
	 * @param order_index
	 * @param default_value
	 * @param required
	 * @param readonly
	 */
	public Interface_Field(int id, int id_interface, String name, String label, int id_type, int order_index, String default_value, boolean required, boolean readonly) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		setId_type(id_type);
		this.order_index = order_index;
		this.default_value = default_value;
		this.required = required;
		this.readonly = readonly;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param id_type
	 * @param order_index
	 * @param required
	 * @param readonly
	 * @param default_value
	 * @param grid
	 * @param source
	 * @param onvalidate
	 * @param onchange
	 * @param onfocus
	 * @param onblur
	 */
	public Interface_Field(int id, int id_interface, String name, String label, int id_type, int order_index, boolean required, boolean readonly, String default_value, String grid, String source, String onvalidate, String onchange, String onfocus, String onblur) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		this.id_type = id_type;
		this.order_index = order_index;
		this.required = required;
		this.readonly = readonly;
		this.default_value = default_value;
		this.grid = grid;
		this.source = source;
		this.onvalidate = onvalidate;
		this.onchange = onchange;
		this.onfocus = onfocus;
		this.onblur = onblur;
	}

	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public Interface_Field copy() throws Exception {
		return Interface_Field.copy(this);
	}

	@SuppressWarnings("UnnecessaryBoxing")
	static Interface_Field copy(Interface_Field field) throws Exception {
		if (field == null) {
			return null;
		}
		return JSON.clone(field);
//		return new Interface_Field(
//				Integer.valueOf(field.getId()),
//				Integer.valueOf(field.getId_interface()),
//				String.valueOf(field.getName()),
//				String.valueOf(field.getLabel()),
//				Integer.valueOf(field.getId_type()),
//				Integer.valueOf(field.getOrder_index()),
//				Boolean.valueOf(field.isRequired()),
//				Boolean.valueOf(field.isReadonly()),
//				String.valueOf(field.getDefault_value()),
//				String.valueOf(field.getGrid()),
//				String.valueOf(field.getSource()),
//				String.valueOf(field.getOnvalidate()),
//				String.valueOf(field.getOnchange()),
//				String.valueOf(field.getOnfocus()),
//				String.valueOf(field.getOnblur())
//		);
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_interface() {
		return id_interface;
	}

	/**
	 *
	 * @param id_interface
	 */
	public void setId_interface(int id_interface) {
		this.id_interface = id_interface;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public Interface_Field setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 *
	 * @param label
	 * @return
	 */
	public Interface_Field setLabel(String label) {
		this.label = label;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public int getId_type() {
		return id_type;
	}

	/**
	 *
	 * @param id_type
	 * @return
	 */
	public Interface_Field setId_type(int id_type) {
		this.id_type = id_type;
		switch (id_type) {
			case InterfaceFieldTypes.BOOLEAN:
			case InterfaceFieldTypes.CHECKBOX:
				this.onvalidate = InterfaceFieldTypes.VALIDATE_BOOLEAN;
				break;
			case InterfaceFieldTypes.NUMBER:
				this.onvalidate = InterfaceFieldTypes.VALIDATE_NUMBER;
				break;
			case InterfaceFieldTypes.DATE:
				this.onvalidate = InterfaceFieldTypes.VALIDATE_DATE;
				break;
			case InterfaceFieldTypes.DATETIME:
				this.onvalidate = InterfaceFieldTypes.VALIDATE_DATETIME;
				break;
			case InterfaceFieldTypes.TIME:
				this.onvalidate = InterfaceFieldTypes.VALIDATE_TIME;
				break;
			case InterfaceFieldTypes.EMAIL:
				this.onvalidate = InterfaceFieldTypes.VALIDATE_EMAIL;
				break;
			case InterfaceFieldTypes.TABLE:
				this.onvalidate = InterfaceFieldTypes.VALIDATE_TABLE;
				break;
			default:
				this.onvalidate = InterfaceFieldTypes.VALIDATE_TEXT;
				break;
		}
		return this;
	}

	/**
	 *
	 * @return
	 */
	public int getOrder_index() {
		return order_index;
	}

	/**
	 *
	 * @param order_index
	 * @return
	 */
	public Interface_Field setOrder_index(int order_index) {
		this.order_index = order_index;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 *
	 * @param required
	 * @return
	 */
	public Interface_Field setRequired(boolean required) {
		this.required = required;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public boolean isReadonly() {
		return readonly;
	}

	/**
	 *
	 * @param readonly
	 * @return
	 */
	public Interface_Field setReadonly(boolean readonly) {
		this.readonly = readonly;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getDefault_value() {
		return default_value;
	}

	/**
	 *
	 * @param default_value
	 * @return
	 */
	public Interface_Field setDefault_value(String default_value) {
		this.default_value = default_value;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getGrid() {
		return grid;
	}

	/**
	 *
	 * @param grid
	 */
	public void setGrid(String grid) {
		this.grid = grid;
	}

	/**
	 *
	 * @return
	 */
	public String getSource() {
		return source;
	}

	/**
	 *
	 * @param source
	 * @return
	 */
	public Interface_Field setSource(String source) {
		this.source = source;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getOnvalidate() {
		return onvalidate;
	}

	/**
	 *
	 * @param onvalidate
	 */
	public void setOnvalidate(String onvalidate) {
		this.onvalidate = onvalidate;
	}

	/**
	 *
	 * @return
	 */
	public String getOnchange() {
		return onchange;
	}

	/**
	 *
	 * @param onchange
	 */
	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

	/**
	 *
	 * @return
	 */
	public String getOnfocus() {
		return onfocus;
	}

	/**
	 *
	 * @param onfocus
	 */
	public void setOnfocus(String onfocus) {
		this.onfocus = onfocus;
	}

	/**
	 *
	 * @return
	 */
	public String getOnblur() {
		return onblur;
	}

	/**
	 *
	 * @param onblur
	 */
	public void setOnblur(String onblur) {
		this.onblur = onblur;
	}

	/**
	 *
	 * @return
	 */
	public Interface getInterface() {
		return Interface;
	}

	/**
	 *
	 * @param Interface
	 */
	public void setInterface(Interface Interface) {
		this.Interface = Interface;
	}

	/**
	 *
	 * @return
	 */
	public List<Interface_Field> getChildren() {
		return children;
	}

	/**
	 *
	 * @param children
	 * @return
	 */
	public Interface_Field setChildren(List<Interface_Field> children) {
		this.children = children;
		return this;
	}

}
