package com.mstn.scripting.core.models;

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.setup.Environment;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.client.HttpClient;
import org.simplejavamail.mailer.Mailer;
import org.skife.jdbi.v2.DBI;

/**
 * @author amatos
 */
final public class InterfaceConfigs {

	/**
	 *
	 */
	static public boolean showLogs;

	/**
	 *
	 */
	static public Map<String, String> configs = new HashMap<>();

	static public Map<String, Object> wsClients = new HashMap<>();

	/**
	 *
	 */
	static public DBI dbi;

	/**
	 *
	 */
	static public DataSourceFactory dataSourceFactory;

	/**
	 *
	 */
	static public ScriptingInterfacesConfiguration microserviceConfig;

	/**
	 *
	 */
	static public Environment environment;

	/**
	 *
	 */
	static public HttpClient httpClient;

	/**
	 *
	 */
	static public Mailer mailer;

	/**
	 *
	 * @param key
	 * @return
	 */
	static public String get(String key) {
		return InterfaceConfigs.configs.get(key);
	}

	/**
	 *
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	static public String get(String key, String defaultValue) {
		return InterfaceConfigs.configs.getOrDefault(key, defaultValue);
	}

	/**
	 *
	 * @param _class
	 * @param msg
	 * @param params
	 */
	static public void log(Class _class, String msg, Object... params) {
		if (showLogs) {
			Logger.getLogger(_class.getName()).log(Level.FINE, msg, params);
		}
	}

	/**
	 *
	 * @param _class
	 * @param msg
	 * @param params
	 */
	static public void logException(Class _class, String msg, Object... params) {
		Logger.getLogger(_class.getName()).log(Level.SEVERE, msg, params);
	}

	/**
	 *
	 */
	static public void setConfig() {
		//CRM
		InterfaceConfigs.configs.put("crm_esb", "http://soaqaenv3:8010/soa-infra/services/crm/CRM_ESB!1.0/CRM.CRM_RS_ep?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomerbycustomerid", "http://SOAQAENV3:6010/services/customer/GetCustomerByCustomerID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomersandcontactsbycontactidentitycardid", "http://SOAQAENV3:6010/services/customer/GetCustomersAndContactsByContactIdentityCardID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomersandcontactsbycontactpassportid", "http://SOAQAENV3:6010/services/customer/GetCustomersAndContactsByContactPassportID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomersandcontactsbytaxid", "http://SOAQAENV3:6010/services/customer/GetCustomersAndContactsByTaxID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomersandcontactsbycontactnames", "http://SOAQAENV3:6010/services/customer/GetCustomersAndContactsByContactNames?WSDL");
		InterfaceConfigs.configs.put("crm_getbusinesscustomersandcontactsbycustomername", "http://SOAQAENV3:6010/services/customer/GetBusinessCustomersAndContactsByCustomerName?WSDL");
		InterfaceConfigs.configs.put("crm_getcaseandsubcaseattachmentsbyid", "http://SOAQAENV3:6010/services/customer/GetCaseAndSubCaseAttachmentsByID?WSDL");
		InterfaceConfigs.configs.put("crm_getcaseattachmentbyfilepath", "http://SOAQAENV3:6010/services/customer/GetCaseAttachmentByFilePath?WSDL");
		InterfaceConfigs.configs.put("crm_getcaseattributesbycaseid", "http://SOAQAENV3:6010/services/customer/GetCaseAttributesByCaseID?WSDL");
		InterfaceConfigs.configs.put("crm_getcasediagnosesbycaseid", "http://SOAQAENV3:6010/services/customer/GetCaseDiagnosesByCaseID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomercasesbycustomerid", "http://SOAQAENV3:6010/services/customer/GetCustomerCasesByCustomerID?WSDL");
		InterfaceConfigs.configs.put("crm_getsubcasesbycaseid", "http://SOAQAENV3:6010/services/customer/GetSubCasesByCaseID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomeractionitemsbycustomerid", "http://SOAQAENV3:6010/services/customer/GetCustomerActionItemsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("crm_getcustomerinteractionsbycustomerid", "http://SOAQAENV3:6010/services/customer/GetCustomerInteractionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("crm_ticketbycredentials", "http://soaqaenv3:6010/services/customer/GetCrmTicketByCredentials?wsdl");
		InterfaceConfigs.configs.put("crm_createcustomerinteraction", "http://soaqaenv3:6010/services/customer/CreateCustomerInteraction?wsdl");
//		InterfaceConfigs.configs.put("crm_createcustomerinteraction", "http://osbprod2.corp.codetel.com.do/services/customer/CreateCustomerInteraction?wsdl");
		InterfaceConfigs.configs.put("crm_casemgmt", "http://soaqaenv3:7020/crm/casemgmt/CaseManagementServiceSoapHttpPort?WSDL");
		InterfaceConfigs.configs.put("crm_casehistory_connectionString", "jdbc:oracle:thin:@172.27.17.50:1521:CRMUT17");
		InterfaceConfigs.configs.put("crm_casehistory_user", "CRM_SCRIPTING");
		InterfaceConfigs.configs.put("crm_casehistory_password", "tdc351er");

		//Ensamble
		InterfaceConfigs.configs.put("ensamble_ejb", "http://soaqaenv1:7020/EJB-EnsembleServicesWrapper-context-root/EnWrapperServiceSoapHttpPort?wsdl");
		InterfaceConfigs.configs.put("ensamble_getbilldetailsbyban", "http://SOAQAENV3:6010/services/billing/GetBillDetailsByBAN?WSDL");
//		InterfaceConfigs.configs.put("ensamble_getbilldetailsbyban", "http://osbprod2.corp.codetel.com.do/services/billing/GetBillDetailsByBAN?WSDL");
		InterfaceConfigs.configs.put("ensamble_getbillingaccountbyban", "http://SOAQAENV3:6010/services/billing/GetBillingAccountByBAN?WSDL");
		InterfaceConfigs.configs.put("ensamble_getbillingaccountsbycustomerid", "http://SOAQAENV3:6010/services/billing/GetBillingAccountsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("ensamble_getbillshistorybybillingaccountno", "http://SOAQAENV3:6010/services/billing/GetBillsHistoryByBillingAccountNo?WSDL");
		InterfaceConfigs.configs.put("ensamble_getpaymentshistorybybillingaccountno", "http://SOAQAENV3:6010/services/billing/GetPaymentsHistoryByBillingAccountNo?WSDL");
		InterfaceConfigs.configs.put("ensamble_getpostpaidsubscriptionsbybillingaccountno", "http://SOAQAENV3:6010/services/billing/GetPostPaidSubscriptionsByBillingAccountNo?WSDL");
		InterfaceConfigs.configs.put("ensamble_getpostpaidsubscriptionsbycustomerid", "http://SOAQAENV3:6010/services/billing/GetPostpaidSubscriptionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("ensamble_getsubscriptionmemos", "http://SOAQAENV3:6010/services/billing/GetSubscriptionMemos?WSDL");
		InterfaceConfigs.configs.put("ensamble_getsubscriptionsbysubscriberno", "http://SOAQAENV3:6010/services/billing/GetSubscriptionsBySubscriberNo?WSDL");
//		InterfaceConfigs.configs.put("ensamble_getsubscriptionsbysubscriberno", "http://osbprod2.corp.codetel.com.do/services/billing/GetSubscriptionsBySubscriberNo?WSDL");
		InterfaceConfigs.configs.put("ensamble_getsubscriptionsbycustomerid", "http://SOAQAENV3:6010/services/billing/GetSubscriptionsByCustomerID?WSDL");
		//ClaroVideo
		InterfaceConfigs.configs.put("claro_video", "http://172.27.17.47:7001/ott-ws/OTTEndpointService?WSDL");
		//Diagnostico
		InterfaceConfigs.configs.put("diagnostico_web", "http://nttappsweb0010.corp.codetel.com.do/SACSWS/DWServices.svc?wsdl");
		InterfaceConfigs.configs.put("diagnostico_sacs", "http://nttappsweb0009/SACSWS");
		InterfaceConfigs.configs.put("sacsApplication", "SCRIPTING");
		InterfaceConfigs.configs.put("sacsSecretCode", "Dy0t8W3TcNekrD+");
		InterfaceConfigs.configs.put("sacsSessionTimeMinutes", "20");
		InterfaceConfigs.configs.put("sacsToken", "");
		InterfaceConfigs.configs.put("sacs_rest_api", "http://nttappsweb0010/sacsapi/api");
		InterfaceConfigs.configs.put("sacs_rest_systemname", "SCRIPTING");
		InterfaceConfigs.configs.put("sacs_rest_systempassword", "Dy0t8W3TcNekrD+");
		InterfaceConfigs.configs.put("dth_satsservices", "http://nttappsweb0009/SATSWS/SATSServices.asmx?wsdl");
		InterfaceConfigs.configs.put("dth_rest_api", "http://nttappsweb0009/satsws/api");
		InterfaceConfigs.configs.put("dth_rest_systemname", "Scripting");
		InterfaceConfigs.configs.put("dth_rest_systempassword", "DR&eeNz4<3yVClaroxGfn");
		// IPTV
//		InterfaceConfigs.configs.put("iptv", "http://172.29.80.22:7001/SPC/services/SPCRouter?wsdl");
		InterfaceConfigs.configs.put("iptv", "file:/C:/MSTN/Scripting/scripting-interfaces-crm/SPCRouter.xml");
		InterfaceConfigs.configs.put("iptv_system", "SCRIPTING");

		InterfaceConfigs.configs.put("fixed", "http://SOAQAENV3:6010/services/product/GetFixedProduct?wsdl");
//		InterfaceConfigs.configs.put("fixed", "http://osbprod2.corp.codetel.com.do/services/product/GetFixedProduct?wsdl");
		InterfaceConfigs.configs.put("fixed_rollover", "http://SOAQAENV3:6010/services/billing/GetFixedPostpaidRollover?wsdl");
//		InterfaceConfigs.configs.put("fixed_rollover", "http://osbprod2.corp.codetel.com.do/services/billing/GetFixedPostpaidRollover?WSDL");
		InterfaceConfigs.configs.put("fixed_comments", "http://SOAQAENV3:6010/services/product/GetProductComments?wsdl");
		InterfaceConfigs.configs.put("miclaro", "https://lxtwlcluts01:7015/agpws/ws/miclaroproviderservices?wsdl");
		InterfaceConfigs.configs.put("ppg_getprepaidsubscriptionsbycustomerid", "http://SOAQAENV3:6010/services/billing/GetPrepaidSubscriptionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("ppg_getmixedrentshistorybysubno", "http://SOAQAENV3:6010/services/billing/GetMixedRentsHistoryBySubNo?WSDL");
		InterfaceConfigs.configs.put("ppg_getsubscriptionrefilltransactions", "http://SOAQAENV3:6010/services/billing/GetSubscriptionRefillTransactions?WSDL");
		InterfaceConfigs.configs.put("oms_getorderactionsbycustomerid", "http://SOAQAENV3:6010/services/ordering/GetOrderActionsByCustomerID?WSDL");
		InterfaceConfigs.configs.put("oms_getorderactionsbyproduct", "http://SOAQAENV3:6010/services/ordering/GetOrderActionsByProduct?WSDL");
		InterfaceConfigs.configs.put("oms_getorderbyorderid", "http://SOAQAENV3:6010/services/ordering/GetOrderByOrderID?WSDL");
//		InterfaceConfigs.configs.put("oms_orderdetails", "http://172.27.17.245:7210/OmsOrderDetails-1.0/OrderDetailsOmsService?WSDL");
		InterfaceConfigs.configs.put("oms_orderdetails", "http://172.27.17.245:7210/Scripting-1.0/OrderDetailsOmsService?WSDL");
		//NETCRACKER
		InterfaceConfigs.configs.put("netcracker", "http://soaqaenv1:6010/services/integration/customerordermanagement/customerorder?wsdl");
		//COMPLEX
		InterfaceConfigs.configs.put("complex_hlr", "http://172.27.17.161:7501/hlr-wservice/HLREndPointService?WSDL");
		InterfaceConfigs.configs.put("complex_hlr_user", "scripting");
		InterfaceConfigs.configs.put("complex_hlr_password", "scripting");
		InterfaceConfigs.configs.put("complex_hlr_centralHlr", "1");
		//SMSC
		InterfaceConfigs.configs.put("smscHost", "172.18.247.90");
		InterfaceConfigs.configs.put("smscPort", "5016");
		InterfaceConfigs.configs.put("smscUsername", "Scripting");
		InterfaceConfigs.configs.put("smscPassword", "Scr!p7w");
		InterfaceConfigs.configs.put("smscSourceAddress", "CLARO");
		//SAYDOTQ
		InterfaceConfigs.configs.put("saydot", "http://nttsaydotwb0001/SaydotQ_02/SaydotQService.asmx?wsdl");
		//SAD
		InterfaceConfigs.configs.put("sad_v2", "http://nttwlogic0002:7095/SADService/v2/DireccionService?WSDL");
	}
}
