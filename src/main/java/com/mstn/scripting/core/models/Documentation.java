/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author josesuero
 */
public class Documentation {

	private int id;
	private int id_center;
	private String id_type;
	@NotEmpty
	private String name;
	private String tags;
	private String value;

	private String center_name;

	/**
	 *
	 */
	public Documentation() {

	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param center_name
	 * @param id_type
	 * @param name
	 * @param tags
	 * @param value
	 */
	public Documentation(int id, 
			int id_center, String center_name, 
			String id_type, String name, String tags, String value) {
		this.id = id;
		this.id_center = id_center;
		this.center_name = center_name;
		this.id_type = id_type;
		this.name = name;
		this.tags = tags;
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getTags() {
		return tags;
	}

	/**
	 *
	 * @param tags
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 *
	 * @return
	 */
	public String getId_type() {
		return id_type;
	}

	/**
	 *
	 * @param id_type
	 */
	public void setId_type(String id_type) {
		this.id_type = id_type;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

}
