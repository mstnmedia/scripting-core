package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mstn.scripting.core.HTTP;
import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.InterfaceOptions;
import com.mstn.scripting.core.enums.Interfaces;
import com.mstn.scripting.core.enums.TransactionInterfaceActions;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.client.HttpClient;

/**
 *
 * @author MSTN Media
 */
public class TransactionInterfacesResponse {

	/**
	 *
	 */
	protected int id_transaction;

	/**
	 *
	 */
	protected Interface Interface;

	/**
	 *
	 */
	protected List<Transaction_Result> results = new ArrayList();

	/**
	 *
	 */
	protected TransactionInterfaceActions action;

	/**
	 *
	 */
	protected String value;

	/**
	 *
	 */
	protected ObjectNode json = JSON.newObjectNode();

	/**
	 *
	 */
	protected HashMap<String, String> transactionChanges = new HashMap<>();

	/**
	 *
	 */
	protected List<Log> logs = new ArrayList();

	/**
	 *
	 */
	public TransactionInterfacesResponse() {
	}

	/**
	 *
	 * @param id_transaction
	 * @param action
	 * @param Interface
	 * @param results
	 */
	public TransactionInterfacesResponse(int id_transaction, TransactionInterfaceActions action, Interface Interface, List<Transaction_Result> results) {
		this.id_transaction = id_transaction;
		this.action = action;
		this.Interface = Interface;
		this.results = results;
	}

	/**
	 *
	 * @param payload
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public TransactionInterfacesResponse getInterfaceResponse(TransactionInterfacesPayload payload, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(payload);
		String strResponse = HTTP.postResource(
				httpClient,
				//apiURL + "/interfaces/interface/getForm",
				apiURL + "/interfaces/interfaceBase/exec",
				strPayload,
				bearerToken
		);
		Logger.getLogger(TransactionInterfacesResponse.class.getName()).log(Level.INFO, "Payload: \n{0}", strPayload);
		Logger.getLogger(TransactionInterfacesResponse.class.getName()).log(Level.INFO, "Response\n{0}", strResponse);
		Logger.getLogger(TransactionInterfacesResponse.class.getName()).log(Level.INFO, "\n\n");
		return JSON.toObject(strResponse, TransactionInterfacesResponse.class);
	}

	/**
	 *
	 * @param response
	 * @param ex
	 * @param user
	 * @return
	 * @throws Exception
	 */
	static public TransactionInterfacesResponse fromException(TransactionInterfacesResponse response, Exception ex, User user) throws Exception {
		response.setAction(TransactionInterfaceActions.ERROR);
		response.setValue(InterfaceOptions.ERROR);
		response.getJson().set("error", JSON.toNode(ex));
		Interface inter = response.getInterface();
		Log log = new Log(0,
				user == null ? 0 : user.getId(),
				"Transacciones",
				response.id_transaction,
				"Error ejecutando interfaz '" + (inter == null ? "" : inter.getId() + " " + inter.getName()) + "'",
				new Date(),
				ex.getMessage(),
				JSON.toString(ex),
				user == null ? "" : user.getIp()
		);
		response.getLogs().add(log);
		response.getResults().add(new Transaction_Result(0,
				inter == null ? Interfaces.TRANSACTION_INFO_INTERFACE_ID : inter.getId(),
				inter == null ? Interfaces.TRANSACTION_INFO_INTERFACE_NAME + "_error_calling_interface" : inter.getName() + "_error_calling_interface",
				"Error consultando interfaz " + (inter == null ? "" : inter.getLabel()),
				ex.getMessage(), "{}"
		));
		return response;
	}

	/**
	 *
	 * @return
	 */
	public int getId_transaction() {
		return id_transaction;
	}

	/**
	 *
	 * @param id_transaction
	 */
	public void setId_transaction(int id_transaction) {
		this.id_transaction = id_transaction;
	}

	/**
	 *
	 * @return
	 */
	public List<Transaction_Result> getResults() {
		return results;
	}

	/**
	 *
	 * @param results
	 */
	public void setResults(List<Transaction_Result> results) {
		this.results = results;
	}

	/**
	 *
	 * @return
	 */
	public TransactionInterfaceActions getAction() {
		return action;
	}

	/**
	 *
	 * @param action
	 */
	public void setAction(TransactionInterfaceActions action) {
		this.action = action;
	}

	/**
	 *
	 * @return
	 */
	public Interface getInterface() {
		return Interface;
	}

	/**
	 *
	 * @param Interface
	 */
	public void setInterface(Interface Interface) {
		this.Interface = Interface;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public ObjectNode getJson() {
		return json;
	}

	/**
	 *
	 * @param json
	 */
	public void setJson(ObjectNode json) {
		this.json = json;
	}

	/**
	 *
	 * @return
	 */
	public HashMap<String, String> getTransactionChanges() {
		return transactionChanges;
	}

	/**
	 *
	 * @param transactionChanges
	 */
	public void setTransactionChanges(HashMap<String, String> transactionChanges) {
		this.transactionChanges = transactionChanges;
	}

	/**
	 *
	 * @return
	 */
	public List<Log> getLogs() {
		return logs;
	}

	/**
	 *
	 * @param logs
	 */
	public void setLogs(List<Log> logs) {
		this.logs = logs;
	}

}
