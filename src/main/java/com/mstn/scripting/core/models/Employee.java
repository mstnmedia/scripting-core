package com.mstn.scripting.core.models;

import com.mstn.scripting.core.enums.Interfaces;
import java.util.Arrays;
import java.util.List;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author amatos
 */
public class Employee {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_parent;

	/**
	 *
	 */
	protected int id_role;

	/**
	 *
	 */
	@NotEmpty
	protected String name;

	/**
	 *
	 */
	protected String email;

	/**
	 *
	 */
	protected String phone;

	/**
	 *
	 */
	protected String position;

	/**
	 *
	 */
	protected String status;

	/**
	 *
	 */
	protected String source;

	/**
	 *
	 */
	protected Employee parent;

	/**
	 *
	 */
	public Employee() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_parent
	 * @param name
	 */
	public Employee(int id, int id_center, int id_parent, String name) {
		this.id = id;
		this.id_center = id_center;
		this.id_parent = id_parent;
		this.name = name;
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_parent
	 * @param id_role
	 * @param name
	 * @param email
	 * @param phone
	 * @param position
	 * @param status
	 * @param source
	 */
	public Employee(int id, int id_center, int id_parent, int id_role, String name, String email, String phone, String position, String status, String source) {
		this.id = id;
		this.id_center = id_center;
		this.id_parent = id_parent;
		this.id_role = id_role;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.position = position;
		this.status = status;
		this.source = source;
	}

	/**
	 *
	 * @param employee
	 * @return
	 */
	static public List<Transaction_Result> getResults(Employee employee) {
		boolean empty = employee == null;
		String id_employee = empty ? "" : String.valueOf(employee.getId());
		String id_parent = empty ? "" : String.valueOf(employee.getId_parent());
		String name = empty ? "" : String.valueOf(employee.getName());
		// String email = empty ? "" : String.valueOf(employee.getEmail());
		// String phone = empty ? "" : String.valueOf(employee.getPhone());
		String position = empty ? "" : String.valueOf(employee.getPosition());
		String status = empty ? "" : String.valueOf(employee.getStatus());
		String source = empty ? "" : String.valueOf(employee.getSource());
		String card = "";
		if (!empty) {
			if ("claro".equals(source.toLowerCase())) {
				card = "TD" + id_employee;
			} else if ("opitel".equals(source.toLowerCase())) {
				card = "CT" + id_employee;
			} else {
				card = id_employee;
			}
		}

		String parent_name = empty ? "" : String.valueOf(
				employee.getParent() == null ? null : employee.getParent().getName()
		);

		int ID = Interfaces.EMPLOYEE_INFO_INTERFACE_ID;
		String prefix = Interfaces.EMPLOYEE_INFO_INTERFACE_NAME + "_";
		return Arrays.asList(
				new Transaction_Result(0, ID, prefix + "id_employee", "ID del Empleado", id_employee, ""),
				new Transaction_Result(0, ID, prefix + "id_parent", "ID del Supervisor", id_parent, ""),
				new Transaction_Result(0, ID, prefix + "name", "Nombre", name, ""),
				// new Transaction_Result(0, ID, prefix + "email", "Email", email, ""),
				// new Transaction_Result(0, ID, prefix + "phone", "Teléfono", phone, ""),
				new Transaction_Result(0, ID, prefix + "position", "Posición", position, ""),
				new Transaction_Result(0, ID, prefix + "status", "Estado", status, ""),
				new Transaction_Result(0, ID, prefix + "source", "Fuente", source, ""),
				new Transaction_Result(0, ID, prefix + "card", "Tarjeta del empleado", card, ""),
				new Transaction_Result(0, ID, prefix + "parent_name", "Nombre del supervisor", parent_name, "")
		);
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_parent() {
		return id_parent;
	}

	/**
	 *
	 * @param id_parent
	 */
	public void setId_parent(int id_parent) {
		this.id_parent = id_parent;
	}

	/**
	 *
	 * @return
	 */
	public int getId_role() {
		return id_role;
	}

	/**
	 *
	 * @param id_role
	 */
	public void setId_role(int id_role) {
		this.id_role = id_role;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 *
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 *
	 * @return
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 *
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 *
	 * @return
	 */
	public String getPosition() {
		return position;
	}

	/**
	 *
	 * @param position
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 *
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 *
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 *
	 * @return
	 */
	public String getSource() {
		return source;
	}

	/**
	 *
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 *
	 * @return
	 */
	public Employee getParent() {
		return parent;
	}

	/**
	 *
	 * @param parent
	 */
	public void setParent(Employee parent) {
		this.parent = parent;
	}

}
