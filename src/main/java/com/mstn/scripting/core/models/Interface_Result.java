package com.mstn.scripting.core.models;

import com.mstn.scripting.core.JSON;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author amatos
 */
public class Interface_Result {

	/**
	 *
	 */
	static public String NULL_VALUE = "null";

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_interface;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected String label;

	/**
	 *
	 */
	protected String props;

	private Interface Interface;

	/**
	 *
	 */
	public Interface_Result() {
	}

	/**
	 *
	 * @param id_interface
	 * @param name
	 * @param label
	 */
	public Interface_Result(int id_interface, String name, String label) {
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param props
	 */
	public Interface_Result(int id, int id_interface, String name, String label, String props) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		this.props = props;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param props
	 * @param Interface
	 */
	public Interface_Result(int id, int id_interface, String name, String label, String props, Interface Interface) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		this.props = props;
		this.Interface = Interface;
	}

	/**
	 *
	 * @param result
	 * @return
	 */
	static public boolean canSave(final Interface_Result result) {
		boolean canSave = true;
		String props = result.getProps();
		if (props != null && !props.isEmpty()) {
			try {
				canSave = !JSON.toNode(props).hasNonNull("notSave");
			} catch (Exception ex) {
				Logger.getLogger(Interface_Result.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return canSave;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_interface() {
		return id_interface;
	}

	/**
	 *
	 * @param id_interface
	 */
	public void setId_interface(int id_interface) {
		this.id_interface = id_interface;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 *
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 *
	 * @return
	 */
	public String getProps() {
		return props;
	}

	/**
	 *
	 * @param props
	 */
	public void setProps(String props) {
		this.props = props;
	}

	/**
	 *
	 * @return
	 */
	public Interface getInterface() {
		return Interface;
	}

	/**
	 *
	 * @param Interface
	 */
	public void setInterface(Interface Interface) {
		this.Interface = Interface;
	}

}
