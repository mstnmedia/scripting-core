/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

/**
 *
 * @author amatos
 */
public class ContentFile {

	private String name;
	private String path;
	private long upladedDate;

	/**
	 *
	 * @param name
	 * @param path
	 * @param upladedDate
	 */
	public ContentFile(String name, String path, long upladedDate) {
		this.name = name;
		this.path = path;
		this.upladedDate = upladedDate;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getPath() {
		return path;
	}

	/**
	 *
	 * @param path
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 *
	 * @return
	 */
	public long getUpladedDate() {
		return upladedDate;
	}

	/**
	 *
	 * @param upladedDate
	 */
	public void setUpladedDate(long upladedDate) {
		this.upladedDate = upladedDate;
	}

}
