package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.JSON;
import java.util.Date;

/**
 *
 * @author amatos
 */
public class Delegation {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_user;

	/**
	 *
	 */
	protected int id_delegate;

	/**
	 *
	 */
	@JsonDeserialize(using = JSON.DateDeserializer.class)
	protected Date end_date;
	private String user_name;
	private String delegate_name;

	/**
	 *
	 */
	public Delegation() {
	}

	/**
	 *
	 * @param id
	 * @param id_user
	 * @param id_delegate
	 * @param date
	 * @param user_name
	 * @param delegate_name
	 */
	public Delegation(int id, int id_user, int id_delegate, Date date, String user_name, String delegate_name) {
		this.id = id;
		this.id_user = id_user;
		this.id_delegate = id_delegate;
		this.end_date = date;
		this.user_name = user_name;
		this.delegate_name = delegate_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_user() {
		return id_user;
	}

	/**
	 *
	 * @param id_user
	 */
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	/**
	 *
	 * @return
	 */
	public int getId_delegate() {
		return id_delegate;
	}

	/**
	 *
	 * @param id_delegate
	 */
	public void setId_delegate(int id_delegate) {
		this.id_delegate = id_delegate;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateSerializer.class)
	public Date getEnd_date() {
		return end_date;
	}

	/**
	 *
	 * @param end_date
	 */
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	/**
	 *
	 * @return
	 */
	public String getUser_name() {
		return user_name;
	}

	/**
	 *
	 * @param user_name
	 */
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	/**
	 *
	 * @return
	 */
	public String getDelegate_name() {
		return delegate_name;
	}

	/**
	 *
	 * @param delegate_name
	 */
	public void setDelegate_name(String delegate_name) {
		this.delegate_name = delegate_name;
	}

}
