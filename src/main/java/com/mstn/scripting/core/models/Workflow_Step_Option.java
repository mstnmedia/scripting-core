/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

import com.mstn.scripting.core.HTTP;
import com.mstn.scripting.core.JSON;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.http.client.HttpClient;

/**
 *
 * @author josesuero
 */
public class Workflow_Step_Option {

	private int id;

	/**
	 *
	 */
	protected int id_center;
	private int id_workflow;
	private int id_workflow_version;
	private int id_workflow_step;
	private String guid_step = "";
	private int id_type;
	private String name;
	private String value;
	private String color;
	private int order_index;

	private Workflow_Step parentWorkflowStep;
	private Workflow_Step referedWorkflowStep;

	/**
	 *
	 */
	public Workflow_Step_Option() {

	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_workflow
	 * @param id_workflow_version
	 * @param id_workflow_step
	 * @param guid_step
	 * @param id_type
	 * @param name
	 * @param color
	 * @param value
	 * @param order_index
	 */
	public Workflow_Step_Option(int id, int id_center, int id_workflow, int id_workflow_version, int id_workflow_step, String guid_step, int id_type, String name, String color, String value, int order_index) {
		this.id = id;
		this.id_center = id_center;
		this.id_workflow = id_workflow;
		this.id_workflow_version = id_workflow_version;
		this.id_workflow_step = id_workflow_step;
		this.guid_step = guid_step;
		this.id_type = id_type;
		this.name = name;
		this.color = color;
		this.value = value;
		this.order_index = order_index;
	}

	/**
	 *
	 * @param id_step
	 * @param httpClient
	 * @param apiURL
	 * @param bearerToken
	 * @return
	 * @throws Exception
	 */
	static public List<Workflow_Step_Option> getOptionsByStepID(int id_step, HttpClient httpClient, String apiURL, String bearerToken) throws Exception {
		String strPayload = JSON.toString(new IdValueParams(id_step));
		String strResponse = HTTP.postResource(
				httpClient,
				apiURL + "/workflows/workflowStepOption/getOptionsByStepID",
				strPayload,
				bearerToken
		);
		return Arrays.asList(JSON.toObject(strResponse, Workflow_Step_Option[].class));
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 * @return the id_workflow
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 * @param id_workflow the id_workflow to set
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 * @return the id_workflow_version
	 */
	public int getId_workflow_version() {
		return id_workflow_version;
	}

	/**
	 * @param id_workflow_version the id_workflow_version to set
	 */
	public void setId_workflow_version(int id_workflow_version) {
		this.id_workflow_version = id_workflow_version;
	}

	/**
	 * @return the id_workflow_step
	 */
	public int getId_workflow_step() {
		return id_workflow_step;
	}

	/**
	 * @param id_workflow_step the id_workflow_step to set
	 */
	public void setId_workflow_step(int id_workflow_step) {
		this.id_workflow_step = id_workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public String getGuid_step() {
		return guid_step;
	}

	/**
	 *
	 * @param guid_step
	 */
	public void setGuid_step(String guid_step) {
		this.guid_step = Objects.nonNull(guid_step) ? guid_step : "";
	}

	/**
	 *
	 * @return
	 */
	public int getId_type() {
		return id_type;
	}

	/**
	 * @param id_type the id_type to set
	 */
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the value to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getColor() {
		return color;
	}

	/**
	 *
	 * @param color
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the order_index
	 */
	public int getOrder_index() {
		return order_index;
	}

	/**
	 * @param order_index the order_index to set
	 */
	public void setOrder_index(int order_index) {
		this.order_index = order_index;
	}

	/**
	 *
	 * @return
	 */
	public Workflow_Step getParentWorkflowStep() {
		return parentWorkflowStep;
	}

	/**
	 *
	 * @param parentWorkflowStep
	 */
	public void setParentWorkflowStep(Workflow_Step parentWorkflowStep) {
		this.parentWorkflowStep = parentWorkflowStep;
	}

	/**
	 *
	 * @return
	 */
	public Workflow_Step getReferedWorkflowStep() {
		return referedWorkflowStep;
	}

	/**
	 *
	 * @param referedWorkflowStep
	 */
	public void setReferedWorkflowStep(Workflow_Step referedWorkflowStep) {
		this.referedWorkflowStep = referedWorkflowStep;
	}

}
