package com.mstn.scripting.core.models;

/**
 *
 * @author MSTN Media
 */
public class IdValueParams {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected String value;

	/**
	 *
	 */
	public IdValueParams() {
	}

	/**
	 *
	 * @param id
	 */
	public IdValueParams(int id) {
		this.id = id;
	}

	/**
	 *
	 * @param id
	 * @param value
	 */
	public IdValueParams(int id, String value) {
		this.id = id;
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
