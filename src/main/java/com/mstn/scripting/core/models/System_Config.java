package com.mstn.scripting.core.models;

/**
 *
 * @author amatos
 */
public class System_Config {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected String label;

	/**
	 *
	 */
	protected String value;

	/**
	 *
	 */
	public System_Config() {
	}

	/**
	 *
	 * @param id
	 * @param name
	 * @param label
	 * @param value
	 */
	public System_Config(int id, String name, String label, String value) {
		this.id = id;
		this.name = name;
		this.label = label;
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 *
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
