package com.mstn.scripting.core.models;

import com.mstn.scripting.core.JSON;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author amatos
 */
public class Transaction_Result {

	/**
	 *
	 */
	static public String NULL_VALUE = null;//"null";

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_transaction;

	/**
	 *
	 */
	protected int id_workflow_step;

	/**
	 *
	 */
	protected int id_interface;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected String label;

	/**
	 *
	 */
	protected String value;

	/**
	 *
	 */
	protected String props;

	private Transaction transaction;
	private Workflow_Step workflow_step;
	private Interface Interface;

	/**
	 *
	 */
	public Transaction_Result() {
	}

	/**
	 *
	 * @param id_interface
	 * @param name
	 * @param label
	 */
	public Transaction_Result(int id_interface, String name, String label) {
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
	}

	/**
	 *
	 * @param id
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param value
	 * @param props
	 */
	public Transaction_Result(int id, int id_interface, String name, String label, String value, String props) {
		this.id = id;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		this.value = value;
		this.props = props;
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_transaction
	 * @param id_workflow_step
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param value
	 * @param props
	 */
	public Transaction_Result(int id, int id_center, int id_transaction, int id_workflow_step, int id_interface, String name, String label, String value, String props) {
		this.id = id;
		this.id_center = id_center;
		this.id_transaction = id_transaction;
		this.id_workflow_step = id_workflow_step;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		this.value = value;
		this.props = props;
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_transaction
	 * @param id_workflow_step
	 * @param id_interface
	 * @param name
	 * @param label
	 * @param value
	 * @param props
	 * @param transaction
	 * @param workflow_step
	 * @param Interface
	 */
	public Transaction_Result(int id, int id_center, int id_transaction, int id_workflow_step, int id_interface, String name, String label, String value, String props, Transaction transaction, Workflow_Step workflow_step, Interface Interface) {
		this.id = id;
		this.id_center = id_center;
		this.id_transaction = id_transaction;
		this.id_workflow_step = id_workflow_step;
		this.id_interface = id_interface;
		this.name = name;
		this.label = label;
		this.value = value;
		this.props = props;
		this.transaction = transaction;
		this.workflow_step = workflow_step;
		this.Interface = Interface;
	}

	/**
	 *
	 * @param result
	 * @return
	 */
	static public boolean canSave(final Transaction_Result result) {
		boolean canSave = true;
		String props = result.getProps();
		if (props != null && !props.isEmpty()) {
			try {
				canSave = !JSON.toNode(props).hasNonNull("notSave");
			} catch (Exception ex) {
				Logger.getLogger(Transaction_Result.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return canSave;
	}

	/**
	 *
	 * @param results
	 * @param resultName
	 * @return
	 */
	static public String getValueFromResults(List<Transaction_Result> results, String resultName) {
		for (int i = 0; i < results.size(); i++) {
			Transaction_Result result = results.get(i);
			if (resultName == null && result.getName().equals(resultName)) {
				return result.getValue();
			}
		}
		return "";
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_transaction() {
		return id_transaction;
	}

	/**
	 *
	 * @param id_transaction
	 */
	public void setId_transaction(int id_transaction) {
		this.id_transaction = id_transaction;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_step() {
		return id_workflow_step;
	}

	/**
	 *
	 * @param id_workflow_step
	 */
	public void setId_workflow_step(int id_workflow_step) {
		this.id_workflow_step = id_workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public int getId_interface() {
		return id_interface;
	}

	/**
	 *
	 * @param id_interface
	 */
	public void setId_interface(int id_interface) {
		this.id_interface = id_interface;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 *
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public String getProps() {
		return props;
	}

	/**
	 *
	 * @param props
	 */
	public void setProps(String props) {
		this.props = props;
	}

	/**
	 *
	 * @return
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 *
	 * @param transaction
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 *
	 * @return
	 */
	public Workflow_Step getWorkflow_step() {
		return workflow_step;
	}

	/**
	 *
	 * @param workflow_step
	 */
	public void setWorkflow_step(Workflow_Step workflow_step) {
		this.workflow_step = workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public Interface getInterface() {
		return Interface;
	}

	/**
	 *
	 * @param Interface
	 */
	public void setInterface(Interface Interface) {
		this.Interface = Interface;
	}

}
