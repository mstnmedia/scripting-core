/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

/**
 *
 * @author amatos
 */
public class InterfaceFieldOptionItem {

	/**
	 *
	 */
	protected String id;

	/**
	 *
	 */
	protected String name;

	/**
	 *
	 */
	protected String label;

	/**
	 *
	 */
	protected String props;

	/**
	 *
	 */
	public InterfaceFieldOptionItem() {
	}

	/**
	 *
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 *
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 *
	 * @return
	 */
	public String getProps() {
		return props;
	}

	/**
	 *
	 * @param props
	 */
	public void setProps(String props) {
		this.props = props;
	}

}
