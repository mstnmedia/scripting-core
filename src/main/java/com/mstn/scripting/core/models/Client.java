package com.mstn.scripting.core.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author amatos
 */
public class Client {
    
	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	@NotEmpty
    protected String name;

	/**
	 *
	 */
	protected int document_type;

	/**
	 *
	 */
	protected String document;

	/**
	 *
	 */
	protected Boolean deleted;
    
	/**
	 *
	 */
	public Client() {
        
    }

	/**
	 *
	 * @param id
	 * @param name
	 */
	public Client(int id, String name) {
        this.id = id;
        this.name = name;
    }

	/**
	 *
	 * @param id
	 * @param name
	 * @param deleted
	 */
	public Client(int id, String name, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
    }

	/**
	 *
	 * @return
	 */
	public int getId() {
        return id;
    }

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
        this.id = id;
    }

	/**
	 *
	 * @return
	 */
	public String getName() {
        return name;
    }

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
        this.name = name;
    }

	/**
	 *
	 * @return
	 */
	public Boolean getDeleted() {
		return deleted;
	}

	/**
	 *
	 * @param deleted
	 */
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
