package com.mstn.scripting.core.models;

/**
 *
 * @author amatos
 */
public class Center_Manager {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_manager;

	/**
	 *
	 */
	protected int goal_seconds;

	/**
	 *
	 */
	protected int margin_percentage;

	/**
	 *
	 */
	protected String center_name;

	/**
	 *
	 */
	protected String manager_name;

	/**
	 *
	 */
	public Center_Manager() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_manager
	 * @param goal_seconds
	 * @param margin_percentage
	 */
	public Center_Manager(int id, int id_center, int id_manager, int goal_seconds, int margin_percentage) {
		this.id = id;
		this.id_center = id_center;
		this.id_manager = id_manager;
		this.goal_seconds = goal_seconds;
		this.margin_percentage = margin_percentage;
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_manager
	 * @param goal_seconds
	 * @param margin_percentage
	 * @param center_name
	 * @param manager_name
	 */
	public Center_Manager(int id, int id_center, int id_manager,
			int goal_seconds, int margin_percentage,
			String center_name, String manager_name) {
		this(id, id_center, id_manager, goal_seconds, margin_percentage);
		this.center_name = center_name;
		this.manager_name = manager_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_manager() {
		return id_manager;
	}

	/**
	 *
	 * @param id_manager
	 */
	public void setId_manager(int id_manager) {
		this.id_manager = id_manager;
	}

	/**
	 *
	 * @return
	 */
	public int getGoal_seconds() {
		return goal_seconds;
	}

	/**
	 *
	 * @param goal_seconds
	 */
	public void setGoal_seconds(int goal_seconds) {
		this.goal_seconds = goal_seconds;
	}

	/**
	 *
	 * @return
	 */
	public int getMargin_percentage() {
		return margin_percentage;
	}

	/**
	 *
	 * @param margin_percentage
	 */
	public void setMargin_percentage(int margin_percentage) {
		this.margin_percentage = margin_percentage;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

	/**
	 *
	 * @return
	 */
	public String getManager_name() {
		return manager_name;
	}

	/**
	 *
	 * @param manager_name
	 */
	public void setManager_name(String manager_name) {
		this.manager_name = manager_name;
	}

}
