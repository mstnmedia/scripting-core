/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.models;

/**
 *
 * @author josesuero
 */
public class Content_Language {

	private int id;
	private int id_center;
	private int id_content;
	private int id_language;
	private String value;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 * @return the id_content
	 */
	public int getId_content() {
		return id_content;
	}

	/**
	 * @param id_content the id_content to set
	 */
	public void setId_content(int id_content) {
		this.id_content = id_content;
	}

	/**
	 * @return the id_language
	 */
	public int getId_language() {
		return id_language;
	}

	/**
	 * @param id_language the id_language to set
	 */
	public void setId_language(int id_language) {
		this.id_language = id_language;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
