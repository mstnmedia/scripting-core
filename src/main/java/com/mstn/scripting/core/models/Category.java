package com.mstn.scripting.core.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author amatos
 */
public class Category {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_parent;

	/**
	 *
	 */
	protected String id_type;

	/**
	 *
	 */
	protected String code;

	/**
	 *
	 */
	@NotEmpty
	protected String name;

	/**
	 *
	 */
	protected boolean deleted;

	/**
	 *
	 */
	protected String parent_name;

	/**
	 *
	 */
	protected String center_name;

	private Category parent;

	/**
	 *
	 */
	public Category() {
	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param center_name
	 * @param id_parent
	 * @param parent_name
	 * @param id_type
	 * @param code
	 * @param name
	 * @param deleted
	 * @param parent
	 */
	public Category(int id, int id_center, String center_name, int id_parent, String parent_name, String id_type, String code, String name, boolean deleted, Category parent) {
		this.id = id;
		this.id_center = id_center;
		this.center_name = center_name;
		this.id_parent = id_parent;
		this.parent_name = parent_name;
		this.id_type = id_type;
		this.code = code;
		this.name = name;
		this.deleted = deleted;
		this.parent = parent;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_parent() {
		return id_parent;
	}

	/**
	 *
	 * @param id_parent
	 */
	public void setId_parent(int id_parent) {
		this.id_parent = id_parent;
	}

	/**
	 *
	 * @return
	 */
	public String getId_type() {
		return id_type;
	}

	/**
	 *
	 * @param id_type
	 */
	public void setId_type(String id_type) {
		this.id_type = id_type;
	}

	/**
	 *
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 *
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return
	 */
	public boolean getDeleted() {
		return deleted;
	}

	/**
	 *
	 * @param deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 *
	 * @return
	 */
	public String getParent_name() {
		return parent_name;
	}

	/**
	 *
	 * @param parent_name
	 */
	public void setParent_name(String parent_name) {
		this.parent_name = parent_name;
	}

	/**
	 *
	 * @return
	 */
	public String getCenter_name() {
		return center_name;
	}

	/**
	 *
	 * @param center_name
	 */
	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}

	/**
	 *
	 * @return
	 */
	public Category getParent() {
		return parent;
	}

	/**
	 *
	 * @param parent
	 */
	public void setParent(Category parent) {
		this.parent = parent;
	}

}
