package com.mstn.scripting.core.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mstn.scripting.core.JSON;
import java.util.Date;

/**
 *
 * @author amatos
 */
public class V_Transaction_Step {

	/**
	 *
	 */
	protected int id;

	/**
	 *
	 */
	protected int id_center;

	/**
	 *
	 */
	protected int id_transaction;

	/**
	 *
	 */
	protected int id_parent;

	/**
	 *
	 */
	protected int id_workflow;

	/**
	 *
	 */
	protected int id_workflow_step;

	/**
	 *
	 */
	protected int id_employee;

	/**
	 *
	 */
	protected Date docdate;

	/**
	 *
	 */
	protected boolean active;

	/**
	 *
	 */
	protected String value;

	/**
	 *
	 */
	protected String workflow_name;

	/**
	 *
	 */
	protected String workflow_step_name;

	/**
	 *
	 */
	protected String employee_name;

	/**
	 *
	 */
	public V_Transaction_Step() {

	}

	/**
	 *
	 * @param id
	 * @param id_center
	 * @param id_transaction
	 * @param id_parent
	 * @param id_workflow
	 * @param id_workflow_step
	 * @param id_employee
	 * @param docdate
	 * @param active
	 * @param value
	 * @param workflow_name
	 * @param workflow_step_name
	 * @param employee_name
	 */
	public V_Transaction_Step(
			int id, int id_center, int id_transaction, int id_parent,
			int id_workflow, int id_workflow_step, int id_employee,
			Date docdate, boolean active, String value,
			String workflow_name, String workflow_step_name, String employee_name) {
		this.id = id;
		this.id_center = id_center;
		this.id_transaction = id_transaction;
		this.id_parent = id_parent;
		this.id_workflow = id_workflow;
		this.id_workflow_step = id_workflow_step;
		this.id_employee = id_employee;
		this.docdate = docdate;
		this.active = active;
		this.value = value;
		this.workflow_name = workflow_name;
		this.workflow_step_name = workflow_step_name;
		this.employee_name = employee_name;
	}

	/**
	 *
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return
	 */
	public int getId_center() {
		return id_center;
	}

	/**
	 *
	 * @param id_center
	 */
	public void setId_center(int id_center) {
		this.id_center = id_center;
	}

	/**
	 *
	 * @return
	 */
	public int getId_transaction() {
		return id_transaction;
	}

	/**
	 *
	 * @param id_transaction
	 */
	public void setId_transaction(int id_transaction) {
		this.id_transaction = id_transaction;
	}

	/**
	 *
	 * @return
	 */
	public int getId_parent() {
		return id_parent;
	}

	/**
	 *
	 * @param id_parent
	 */
	public void setId_parent(int id_parent) {
		this.id_parent = id_parent;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow() {
		return id_workflow;
	}

	/**
	 *
	 * @param id_workflow
	 */
	public void setId_workflow(int id_workflow) {
		this.id_workflow = id_workflow;
	}

	/**
	 *
	 * @return
	 */
	public int getId_workflow_step() {
		return id_workflow_step;
	}

	/**
	 *
	 * @param id_workflow_step
	 */
	public void setId_workflow_step(int id_workflow_step) {
		this.id_workflow_step = id_workflow_step;
	}

	/**
	 *
	 * @return
	 */
	public int getId_employee() {
		return id_employee;
	}

	/**
	 *
	 * @param id_employee
	 */
	public void setId_employee(int id_employee) {
		this.id_employee = id_employee;
	}

	/**
	 *
	 * @return
	 */
	@JsonSerialize(using = JSON.DateTimeSerializer.class)
	public Date getDocdate() {
		return docdate;
	}

	/**
	 *
	 * @param docdate
	 */
	public void setDocdate(Date docdate) {
		this.docdate = docdate;
	}

	/**
	 *
	 * @return
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 *
	 * @param active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_name() {
		return workflow_name;
	}

	/**
	 *
	 * @param workflow_name
	 */
	public void setWorkflow_name(String workflow_name) {
		this.workflow_name = workflow_name;
	}

	/**
	 *
	 * @return
	 */
	public String getWorkflow_step_name() {
		return workflow_step_name;
	}

	/**
	 *
	 * @param workflow_step_name
	 */
	public void setWorkflow_step_name(String workflow_step_name) {
		this.workflow_step_name = workflow_step_name;
	}

	/**
	 *
	 * @return
	 */
	public String getEmployee_name() {
		return employee_name;
	}

	/**
	 *
	 * @param employee_name
	 */
	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

}
