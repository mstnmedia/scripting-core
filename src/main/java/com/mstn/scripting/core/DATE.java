/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import static com.mstn.scripting.core.enums.DateFormat.ISO_DATE_FORMAT;
import static com.mstn.scripting.core.enums.DateFormat.DATE_TIME_FORMAT;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;

/**
 * Clase con funciones comunes para el manejo de fechas.
 *
 * @author amatos
 */
public class DATE {

	/**
	 * {@code Locale} por defecto indicando la configuración del español
	 * dominicano.
	 */
	static public Locale LOCALE = Locale.forLanguageTag("es-DO");

	/**
	 * SimpleDateFormatter por defecto con la configururación por defecto.
	 */
	static public SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", LOCALE);

	/**
	 * Conjunto de formatos de fecha ISO como serán interpretados los textos en
	 * campos y filtros tipo fecha enviados por los usuarios.
	 */
	static public String[] parsePatterns = {"yyyy-MM-dd'T'HH:mm:ss", "yyyy-MM-dd'T'HH:mm", "yyyy-MM-dd"};

	/**
	 * Convierte XMLGregorianCalendar a Date.
	 *
	 * @param date Instancia de {@link XMLGregorianCalendar} que se quiere
	 * convertir.
	 * @return El valor Date equivalente al XMLGregorianCalendar indicado.
	 */
	static public Date toDate(XMLGregorianCalendar date) {
		GregorianCalendar gc = date == null ? null : date.toGregorianCalendar();
		return gc == null ? null : gc.getTime();
	}

	/**
	 * Convierte de DateTime a Date.
	 *
	 * @param dateTime Instancia de {@link DateTime} que se quiere convertir
	 * @return El valor Date equivalente al DateTime indicado.
	 */
	static public Date toDate(DateTime dateTime) {
		return dateTime == null ? null : dateTime.toDate();
	}

	/**
	 * Convierte a fecha el texto indicado.
	 *
	 * @param strDate Fecha en texto con formato ISO
	 * @return Una instancia de {@link Date} con el valor obtenido del texto
	 * indicado.
	 * @throws IllegalArgumentException Si el texto es vacío o nulo.
	 * @throws ParseException Si no se pudo interpretar el texto como una fecha.
	 */
	static public Date toDate(String strDate) throws IllegalArgumentException, ParseException {
		Date date = DateUtils.parseDateStrictly(strDate, LOCALE, parsePatterns);
		return date;
	}

	/**
	 * Convierte una fecha a texto en formato ISO.
	 *
	 * @param xmlGC Instancia de {@link XMLGregorianCalendar} que se quiere
	 * convertir.
	 * @return La representación textual de la fecha indicada.
	 */
	static public String toString(XMLGregorianCalendar xmlGC) {
		Date date = toDate(xmlGC);
		return toString(date);
	}

	/**
	 * Convierte una fecha a texto en formato ISO.
	 *
	 * @param dateTime Instancia de {@link DateTime} que se quiere convertir.
	 * @return La representación textual de la fecha indicada.
	 */
	static public String toString(DateTime dateTime) {
		Date date = toDate(dateTime);
		return toString(date);
	}

	/**
	 * Convierte una fecha a texto en formato ISO.
	 *
	 * @param date Instancia de {@link Date} que se quiere convertir.
	 * @return La representación textual de la fecha indicada.
	 */
	static public String toString(Date date) {
		return toDateTimeString(date);
	}

	/**
	 * Obtiene el número de milisecundos desde las 12 am del 1 de enero del 1970
	 * que representa la fecha actual.
	 *
	 * @return Un número {@code long} que representa la fecha actual;
	 */
	static public long nowAsLong() {
		return new Date().getTime();
	}

	/**
	 * Función creada para obtener de la base de datos columnas tipo datetime
	 * como instancias de la clase Timestamp.
	 *
	 * @param hasColumn Map que indica todas las columnas que se consiguieron en
	 * el resultSet;
	 * @param resultSet ResultSet que contiene un registro de la consulta.
	 * @param column Nombre de la columna que contiene el valor datetime.
	 * @return Una instancia de Timestamp con el valor obtenido de la columna.
	 * @throws SQLException Si el nombre de la columna es inválido, el ResultSet
	 * está cerrado u ocurre un error en el acceso a la base de datos.
	 */
	static public Timestamp getTimestamp(HashMap<String, Boolean> hasColumn, ResultSet resultSet, String column) throws SQLException {
		if (hasColumn.containsKey(column)) {
			java.sql.Date date = resultSet.getDate(column);
			if (date != null) {
				return new Timestamp(date.getTime());
			}
		}
		return null;
	}

	/**
	 * Función creada para obtener de la base de datos columnas tipo datetime
	 * como instancias de la clase DateTime.
	 *
	 * @param hasColumn Map que indica todas las columnas que se consiguieron en
	 * el resultSet;
	 * @param resultSet ResultSet que contiene un registro de la consulta.
	 * @param column Nombre de la columna que contiene el valor datetime.
	 * @return Una instancia de DateTime con el valor obtenido de la columna.
	 * @throws SQLException Si el nombre de la columna es inválido, el ResultSet
	 * está cerrado u ocurre un error en el acceso a la base de datos.
	 */
	static public DateTime getDateTime(HashMap<String, Boolean> hasColumn, ResultSet resultSet, String column) throws SQLException {
		if (hasColumn.containsKey(column)) {
			java.sql.Date date = resultSet.getDate(column);
			if (date != null) {
				return new DateTime(date.getTime());
			}
		}
		return null;
	}

	/**
	 * Convierte la fecha indicada a texto con formato ISO 'yyyy-MM-dd'.
	 *
	 * @param date La fecha que se quiere convertir.
	 * @return Un texto que representa la fecha sin valor del tiempo.
	 */
	static public String toISODate(Date date) {
		return date == null ? null : new SimpleDateFormat(ISO_DATE_FORMAT).format(date);
	}

	/**
	 * Convierte la fecha indicada a texto con formato 'yyyy-MM-ddTHH:mm:ss'.
	 *
	 * @param date La fecha que se quiere convertir.
	 * @return Un texto que representa la fecha con valor del tiempo.
	 */
	static public String toDateTimeString(Date date) {
		return date == null ? null : new SimpleDateFormat(DATE_TIME_FORMAT).format(date);
	}

	/**
	 * Convierte la fecha indicada a texto con el formato indicado. El formato
	 * indicado será interpretado con el {@code Locale} por defecto.
	 *
	 * @param date La fecha que se quiere convertir.
	 * @param pattern El formato en que se quiere obtener el texto.
	 * @return El valor de la fecha indicada expresado en texto con el formato
	 * indicado.
	 */
	static public String format(Date date, String pattern) {
		return date == null ? null : new SimpleDateFormat(pattern, LOCALE).format(date);
	}

	/**
	 * Convierte la fecha indicada a texto con el formato indicado.
	 *
	 * @param date La fecha que se quiere convertir.
	 * @param pattern El formato en que se quiere obtener el texto.
	 * @param locale {@code Locale} en que se interpretará el formato indicado.
	 * @return El valor de la fecha indicada expresado en texto con el formato
	 * indicado.
	 */
	static public String format(Date date, String pattern, Locale locale) {
		return date == null ? null : new SimpleDateFormat(pattern, locale).format(date);
	}
}
