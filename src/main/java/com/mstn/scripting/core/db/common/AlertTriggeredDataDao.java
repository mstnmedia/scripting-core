/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Alert_Triggered_Data;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code alert_triggered_data}.
 *
 * @author amatos
 */
@RegisterMapper(AlertTriggeredDataDao.AlertTriggeredDataMapper.class)
@UseStringTemplate3StatementLocator
public interface AlertTriggeredDataDao {

	/**
	 * Clase que crea una instancia de {@link Alert_Triggered_Data} desde un
	 * {@link ResultSet} que contiene el registro correspondiente en la tabla
	 * {@code alert_triggered_data}
	 */
	static public class AlertTriggeredDataMapper implements ResultSetMapper<Alert_Triggered_Data> {

		/**
		 * Nombre del campo ID en la tabla de variables de alertas disparadas.
		 */
		private static final String ID = "id";
		/**
		 * Nombre del campo ID_CENTER en la tabla de variables de alertas
		 * disparadas.
		 */
		private static final String ID_CENTER = "id_center";
		/**
		 * Nombre del campo ID_ALERT_TRIGGERED en la tabla de variables de
		 * alertas disparadas.
		 */
		private static final String ID_ALERT_TRIGGERED = "id_alert_triggered";
		/**
		 * Nombre del campo NAME en la tabla de variables de alertas disparadas.
		 */
		private static final String NAME = "name";
		/**
		 * Nombre del campo VALUE en la tabla de variables de alertas
		 * disparadas.
		 */
		private static final String VALUE = "value";

		@Override
		public Alert_Triggered_Data map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Alert_Triggered_Data(hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_ALERT_TRIGGERED) ? resultSet.getInt(ID_ALERT_TRIGGERED) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(VALUE) ? resultSet.getString(VALUE) : "");
		}
	}

	/**
	 * Obtiene una lista de variables de alertas disparadas que coinciden con el
	 * filtro dado.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered_data} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return Una lista de variables de alertas disparadas.
	 */
	@SqlQuery("select * from alert_triggered_data <where>")
	List<Alert_Triggered_Data> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene todas las variables de alertas disparadas que pertenecen a la
	 * alerta disparada indicada.
	 *
	 * @param id_alert_triggered ID de la alerta disparada.
	 * @return Una lista de las variables de una alerta disparada.
	 */
	@SqlQuery("select * from alert_triggered_data where id_alert_triggered=:id_alert_triggered")
	List<Alert_Triggered_Data> getAll(@Bind("id_alert_triggered") int id_alert_triggered);

	/**
	 * Obtiene la variable de alerta disparada que corresponde al ID indicado.
	 *
	 * @param id ID de la variable de alerta disparada que se buscará.
	 * @return Instancia de variable de alerta disparada.
	 */
	@SqlQuery("select * from alert_triggered_data where id = :id")
	Alert_Triggered_Data get(@Bind("id") final int id);

	/**
	 * Inserta la variable de alerta disparada indicada en la base de datos,
	 * asignándole un ID autoincremental con una secuencia de Oracle.
	 *
	 * @param item Instancia de la variable de alerta disparada que se
	 * insertará.
	 * @return El ID de la variable de alerta disparada insertada.
	 */
	@SqlUpdate("insert into alert_triggered_data(id, id_center, id_alert_triggered, name, value) "
			+ " values(alert_triggered_data_seq.nextVal, :id_center, :id_alert_triggered, :name, :value)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	int insert(@BindBean final Alert_Triggered_Data item);

	/**
	 * Actualiza en la tabla de variables de alertas disparadas el registro que
	 * coincide con el ID de la instancia dada.
	 *
	 * @param item Instancia de alerta disparada con los datos nuevos.
	 */
	@SqlUpdate("update alert_triggered_data set"
			+ " id_center = :id_center,"
			+ " id_alert_triggered = :id_alert_triggered,"
			+ " name = :name,"
			+ " value = :value "
			+ "where id = :id")
	void update(@BindBean final Alert_Triggered_Data item);

	/**
	 * Elimina de la tabla de variables de alertas disparadas el registro que
	 * coindice con el ID indicado.
	 *
	 * @param id ID del registro que se eliminará.
	 * @return La cantidad de registros eliminados.
	 */
	@SqlUpdate("delete from alert_triggered_data where id = :id")
	int delete(@Bind("id") final int id);
}
