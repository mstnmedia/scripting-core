/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db;

import com.mstn.scripting.core.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase utilizada para guardar en la base de datos valores de textos largos.
 *
 * @author amatos
 */
public class ClobValue {

	/**
	 *
	 */
	public List<String> values = new ArrayList<>();

	/**
	 *
	 */
	public String preparedString = ""; // "to_clob(:value1)||to_clob(:value2)"

	/**
	 *
	 * @param cv
	 * @return
	 */
	static public ClobValue emptyIfNull(ClobValue cv) {
		if (cv == null) {
			cv = new ClobValue();
		}
		return cv;
	}

	/**
	 *
	 */
	public ClobValue() {
	}

	/**
	 *
	 * @param textValue
	 */
	public ClobValue(String textValue) {
		try {
			this.values = new ArrayList();
			if (Utils.stringIsNullOrEmpty(textValue)) {
				values.add("");
			} else if (textValue.length() <= 4000) {
				values.add(textValue);
			} else {
				int start = 0;
				int end = 0;
				while (start < textValue.length() - 1) {
					end = Math.min(start + 4000, textValue.length());
					String part = textValue.substring(start, end);
					this.values.add(part);
					start = end;
				}
			}
			this.calcQuery();
		} catch (Exception ex) {
			Logger.getLogger(ClobValue.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 *
	 * @param filters
	 */
	public ClobValue(String... filters) {
		this.values.addAll(Arrays.asList(filters));
		this.calcQuery();
	}

	/**
	 *
	 * @param values
	 */
	public ClobValue(List<String> values) {
		this.values = values;
		this.calcQuery();
	}

	/**
	 *
	 * @param value
	 * @return
	 */
	public ClobValue add(String... value) {
		this.values.addAll(Arrays.asList(value));
		return this;
	}

	/**
	 *
	 * @return
	 */
	final public ClobValue calcQuery() {
		try {
			preparedString = "";
			for (int i = 0; i < values.size(); i++) {
				if (i != 0) {
					preparedString += "||";
				}
				preparedString += "to_clob(:value" + i + ")";
			}
		} catch (Exception ex) {
			Logger.getLogger(ClobValue.class
					.getName()).log(Level.SEVERE, "Error in calcQuery", ex);
			throw ex;
		}
		return this;
	}

	/**
	 *
	 * @return
	 */
	public List<String> getQueryValues() {
		return this.values;
	}

	/**
	 *
	 * @param values
	 */
	public void setQueryValues(List<String> values) {
		this.values = values;
	}

	/**
	 *
	 * @return
	 */
	public String getPreparedString() {
		return preparedString;
	}

	/**
	 *
	 * @param preparedString
	 */
	public void setPreparedString(String preparedString) {
		this.preparedString = preparedString;
	}

}
