/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.models.Employee;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code employee}.
 *
 * @author josesuero
 */
@RegisterMapper(EmployeeDao.EmployeeMapper.class)
@UseStringTemplate3StatementLocator
public abstract class EmployeeDao {

	/**
	 * Clase que crea una instancia de {@link Employee} desde un
	 * {@link ResultSet} que contiene el registro correspondiente en la tabla
	 * {@code employee}
	 */
	static public class EmployeeMapper implements ResultSetMapper<Employee> {

		/**
		 * Nombre del campo ID en la tabla de empleados.
		 */
		private static final String ID = "id";
		/**
		 * Nombre del campo ID_CENTER en la tabla de empleados.
		 */
		private static final String ID_CENTER = "id_center";
		/**
		 * Nombre del campo ID_PARENT en la tabla de empleados.
		 */
		private static final String ID_PARENT = "id_parent";
		/**
		 * Nombre del campo ID_ROLE en la tabla de empleados.
		 */
		private static final String ID_ROLE = "id_role";
		/**
		 * Nombre del campo NAME en la tabla de empleados.
		 */
		private static final String NAME = "name";
		/**
		 * Nombre del campo EMAIL en la tabla de empleados.
		 */
		private static final String EMAIL = "email";
		/**
		 * Nombre del campo PHONE en la tabla de empleados.
		 */
		private static final String PHONE = "phone";
		/**
		 * Nombre del campo POSITION en la tabla de empleados.
		 */
		private static final String POSITION = "position";
		/**
		 * Nombre del campo STATUS en la tabla de empleados.
		 */
		private static final String STATUS = "status";
		/**
		 * Nombre del campo SOURCE en la tabla de empleados.
		 */
		private static final String SOURCE = "source";

		@Override
		public Employee map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Employee(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(ID_PARENT) ? resultSet.getInt(ID_PARENT) : 0,
					hasColumn.containsKey(ID_ROLE) ? resultSet.getInt(ID_ROLE) : 0,
					hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
					hasColumn.containsKey(EMAIL) ? resultSet.getString(EMAIL) : "",
					hasColumn.containsKey(PHONE) ? resultSet.getString(PHONE) : "",
					hasColumn.containsKey(POSITION) ? resultSet.getString(POSITION) : "",
					hasColumn.containsKey(STATUS) ? resultSet.getString(STATUS) : "",
					hasColumn.containsKey(SOURCE) ? resultSet.getString(SOURCE) : ""
			);
		}
	}

	/**
	 * Obtiene la cantidad total de registros en la vista de empleados.
	 *
	 * @return
	 */
	public long getCount() {
		return getCount(null);
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de empleados que coinciden
	 * con el filtro {@code where}.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de empleados que coinciden
	 * con el filtro {@code where}.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return
	 */
	@SqlQuery("select count(id) from employee e <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene una lista de empleados que coinciden con el filtro dado, que se
	 * encuentran en la página indicada y ordenadas por el orden indicado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de empleados.
	 */
	public List<Employee> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 * Obtiene una lista de empleados que coinciden con el filtro dado, que se
	 * encuentran en la página indicada y ordenadas por el orden indicado.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de empleados.
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, e.* from ("
			+ "  select e.* from employee e <where> order by <order_by>"
			+ " ) e"
			+ ") e where e.rowno >= ((:page_number - 1) * :page_size + 1) and e.rowno \\<= (:page_number * :page_size)")
	abstract public List<Employee> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	// TODO: order_by debe ser una lista como WhereClause
	);

	/**
	 * Obtiene una lista de todos los empleados que coinciden con el filtro
	 * dado, ordenadas por Nombre ascendientemente.
	 *
	 * @param where Texto vacío o con un filtro para la tabla {@code employee}
	 * con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return Una lista de empleados.
	 */
	@SqlQuery("select * from employee e <where> order by name")
	abstract public List<Employee> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene una lista de empleados que coinciden con el filtro dado, que se
	 * encuentran en la página indicada y ordenadas por el orden indicado.
	 *
	 * @param where Texto vacío o con un filtro para la tabla {@code employee}
	 * con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de empleados
	 */
	@SqlQuery("select * from employee e <where> order by <order_by>")
	abstract public List<Employee> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Define("order_by") String orderBy);

	/**
	 * Obtiene una lista de todos los empleados que se reportan al supervisor
	 * indicado.
	 *
	 * @param id_parent ID del supervisor indicado.
	 * @return Una lista de empleados.
	 */
	@SqlQuery("select * from employee where id_parent = :id_parent order by name")
	abstract public List<Employee> getReports(@Bind("id_parent") final int id_parent);

	/**
	 * Obtiene el empleado que corresponde al ID indicado.
	 *
	 * @param id ID del empleado que se buscará.
	 * @return Instancia de empleado.
	 */
	@SqlQuery("select * from employee where id = :id")
	abstract public Employee get(@Bind("id") final int id);

	/**
	 * Inserta el empleado indicado en la base de datos, con el ID especificado
	 * en la instancia.
	 *
	 * @param item Instancia de empleado que se insertará.
	 * @return El ID del empleado insertado.
	 */
	@SqlUpdate("insert into employee_bck(id, id_center, id_parent, id_role, name, email, phone, position, status) "
			+ " values(:id, :id_center, :id_parent, :id_role, :name, :email, :phone, :position, :status)")
	abstract public int insert(@BindBean final Employee item);

	/**
	 * Actualiza en la tabla de empleados el registro que coincide con el ID de
	 * la instancia dada.
	 *
	 * @param item Instancia de empleado con los datos nuevos.
	 */
	@SqlUpdate("update employee_bck set "
			+ "	id_center = :id_center,"
			+ "	id_parent = :id_parent,"
			+ "	id_role = :id_role,"
			+ "	name = :name,"
			+ "	email = :email,"
			+ "	phone = :phone,"
			+ "	position = :position,"
			+ "	status = :status "
			+ "where id = :id")
	abstract public void update(@BindBean final Employee item);

	/**
	 * Elimina de la tabla de empleados al registro con el ID indicado.
	 *
	 * @param id ID del registro a eliminar
	 * @return La cantidad de registros eliminados.
	 */
	@SqlUpdate("delete from employee where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 * Query base para consultar los empleados que se reportan a un usuario
	 * junto a los que se reportan a los que está delegando.
	 */
	static public final String TREEDOWNBASE = "with delegations as ("
			+ "	select id_user, id_delegate from delegation del where trunc(del.end_date, 'DD')>=trunc(sysdate, 'DD')"
			+ "), treeDown as ("
			+ "	select distinct e.id, e.id_parent, e.name"
			+ "	from employee e"
			+ "	connect by nocycle prior e.id = e.id_parent "
			+ "	start with e.id=:userID or e.id in (select distinct del.id_user from delegations del where del.id_delegate=:userID)"
			+ ")";

	/**
	 * Obtiene una lista de empleados que se reportan al usuario indicado junto
	 * a la de los empleados a los que está delegando.
	 *
	 * @param userID Tarjeta del usuario indicado.
	 * @return
	 */
	@SqlQuery(TREEDOWNBASE + "	select * from treeDown")
	abstract public List<Employee> getTreeDown(@Bind("userID") final long userID);

	/**
	 * Obtiene una lista de todos los IDs de los empleados que se reportan al
	 * usuario indicado junto a la de los empleados a los que está delegando.
	 *
	 * @param userID Tarjeta del usuario indicado.
	 * @return
	 */
	@SqlQuery(TREEDOWNBASE + "	select id from treeDown")
	abstract public List<Integer> getTreeDownInt(@Bind("userID") final long userID);
}
