/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Delegation;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

/**
 * Clase para interactuar con la tabla de delegaciones.
 *
 * @author amatos
 */
public abstract class DelegationService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 * Constructor
	 */
	public DelegationService() {

	}

	@CreateSqlObject
	abstract DelegationDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 * Obtiene la cantidad total de registros en la tabla de delegaciones.
	 *
	 * @return
	 */
	public long getCount() {
		return dao().getCount();
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de delegaciones que
	 * coinciden con el filtro {@code where}.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 * Obtiene una {@code Representation} con la lista de delegaciones que
	 * coinciden con el filtro dado, que se encuentran en la página indicada y
	 * ordenadas por el orden indicado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de delegaciones.
	 */
	public Representation<List<Delegation>> getRepresentation(
			String where, int pageSize, int pageNumber, String orderBy) {
		WhereClause whereClause = new WhereClause(where);
		long totalRows = getCount(whereClause);
		List<Delegation> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 * Obtiene una lista de delegaciones que coinciden con el filtro dado, que
	 * se encuentran en la página indicada y ordenadas por el orden indicado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de delegaciones.
	 */
	public List<Delegation> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 * Obtiene una lista de todas las delegaciones que cumplen los criterios del
	 * filtro consultado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @return Una lista de delegaciones.
	 */
	public List<Delegation> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 * Obtiene todas las delegaciones del empleado indicado.
	 *
	 * @param id_employee ID del empleado que se consulta.
	 * @return Una lista de delegaciones.
	 */
	public List<Delegation> getByDelegateID(int id_employee) {
		List<Delegation> list = dao().getByDelegateID(id_employee);
		return list;
	}

	/**
	 * Obtiene la delegación que corresponde al ID indicado.
	 *
	 * @param id ID de la delegación que se buscará.
	 * @return Instancia de delegacion.
	 */
	public Delegation getOrNull(int id) {
		Delegation item = dao().get(id);
		return item;
	}

	/**
	 * Obtiene la delegación que corresponde al ID indicado.
	 *
	 * @param id ID de la delegación que se buscará.
	 * @return Instancia de delegacion.
	 * @throws WebApplicationException si el registro no es encontrado en la
	 * base de datos.
	 */
	public Delegation get(int id) {
		Delegation item = getOrNull(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}

		return item;
	}

	/**
	 * Inserta la delegación indicada en la base de datos y guarda un histórico
	 * del cambio.
	 *
	 * @param item Delegación que se insertará.
	 * @param user Usuario que realiza esta acción.
	 * @return La delegación insertada.
	 */
	@Transaction
	public Delegation create(Delegation item, User user) {
		int id = dao().insert(item);
		Delegation after = get(id);
		log().insert(user.getId(), "Delegaciones", id, "Crear", item, after, user.getIp());
		return after;
	}

	/**
	 * Actualiza en la base de datos la delegación que corresponde con el ID de
	 * la delegación indicada y guarda un histórico del cambio.
	 *
	 * @param item Delegación que se actualizará.
	 * @param user Usuario que realiza esta acción.
	 * @return La delegación actualizada.
	 */
	@Transaction
	public Delegation update(Delegation item, User user) {
		Delegation before = get(item.getId());
		dao().update(item);
		Delegation after = get(item.getId());
		log().insert(user.getId(), "Delegaciones", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 * Elimina la delegación que coindice con el ID indicado y guarda un
	 * histórico del cambio.
	 *
	 * @param id ID de la delegación que se quiere eliminar.
	 * @param user Usuario que realiza la acción.
	 * @return String indicando que la accion fue satisfactoria.
	 * @throws WebApplicationException si no se encuentra en la base de datos el
	 * registro con el ID indicado.
	 */
	public String delete(final int id, User user) {
		Delegation before = get(id);
		int result = dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Delegaciones", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
