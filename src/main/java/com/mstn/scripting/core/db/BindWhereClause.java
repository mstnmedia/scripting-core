/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

/**
 * Clase que enlaza los valores de los filtros en la posición del query
 * correspondiente con la validación de inyecciones SQL.
 *
 * @author MSTN Media
 */
@BindingAnnotation(BindWhereClause.WhereClauseBinderFactory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface BindWhereClause {

	/**
	 *
	 */
	public class WhereClauseBinderFactory implements BinderFactory {

		@Override
		public Binder build(Annotation annotation) {
			return new Binder<BindWhereClause, WhereClause>() {

				void bindSimpleWhere(SQLStatement q, Where where) {
					String label = where.getLabel();
					Object value = where.getValue();
					String criteria = where.getCriteria();
					if (Where.LIKE.equals(criteria) || Where.NOT_LIKE.equals(criteria)) {
						value = "%" + value + "%";
					}
					if (Where.IN.equals(criteria) || Where.NOT_IN.equals(criteria)) {
						List values = (List) value;
						for (int i = 0; i < values.size(); i++) {
							Object element = values.get(i);
							q.bind(label + i, String.valueOf(element));
						}
					} else {
						q.bind(label, String.valueOf(value));
					}
				}

				void bindListWhere(SQLStatement q, List<Where> wheres) {
					wheres.forEach(where -> {
						List<Where> group = where.getGroup();
						if (group == null || group.isEmpty()) {
							bindSimpleWhere(q, where);
						} else {
							bindListWhere(q, group);
						}
					});
				}

				@Override
				public void bind(SQLStatement q, BindWhereClause bind, WhereClause clause) {
					bindListWhere(q, clause.getQueryValues());
				}
			};
		}
	}
}
