/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Delegation;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code alert_triggered}.
 *
 * @author josesuero
 */
@RegisterMapper(DelegationDao.DelegationMapper.class)
@UseStringTemplate3StatementLocator
public abstract class DelegationDao {

	/**
	 * Clase que crea una instancia de {@link Delegation} desde un
	 * {@link ResultSet} que contiene el registro correspondiente en la tabla
	 * {@code delegation}.
	 */
	static public class DelegationMapper implements ResultSetMapper<Delegation> {

		/**
		 * Nombre del campo ID en la vista de delegaciones.
		 */
		private static final String ID = "id";
		/**
		 * Nombre del campo ID_USER en la vista de delegaciones.
		 */
		private static final String ID_USER = "id_user";
		/**
		 * Nombre del campo ID_DELEGATE en la vista de delegaciones.
		 */
		private static final String ID_DELEGATE = "id_delegate";
		/**
		 * Nombre del campo END_DATE en la vista de delegaciones.
		 */
		private static final String END_DATE = "end_date";
		/**
		 * Nombre del campo USER_NAME en la vista de delegaciones.
		 */
		private static final String USER_NAME = "user_name";
		/**
		 * Nombre del campo DELEGATE_NAME en la vista de delegaciones.
		 */
		private static final String DELEGATE_NAME = "delegate_name";

		@Override
		public Delegation map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Delegation(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_USER) ? resultSet.getInt(ID_USER) : 0,
					hasColumn.containsKey(ID_DELEGATE) ? resultSet.getInt(ID_DELEGATE) : 0,
					hasColumn.containsKey(END_DATE) ? resultSet.getDate(END_DATE) : null,
					hasColumn.containsKey(USER_NAME) ? resultSet.getString(USER_NAME) : "",
					hasColumn.containsKey(DELEGATE_NAME) ? resultSet.getString(DELEGATE_NAME) : ""
			);
		}
	}

	/**
	 * Obtiene la cantidad total de registros en la tabla de delegaciones.
	 *
	 * @return
	 */
	public long getCount() {
		return getCount(null);
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de delegaciones que
	 * coinciden con el filtro {@code where}.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de delegaciones que
	 * coinciden con el filtro {@code where}.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return
	 */
	@SqlQuery("select count(id) from v_delegation <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * /**
	 * Obtiene una lista de todas las delegaciones que coinciden con el
	 * filtro dado, ordenadas por ID descendientemente.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return Una lista de delegaciones.
	 */
	@SqlQuery("select * from v_delegation <where> order by ID desc")
	abstract public List<Delegation> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene una lista de delegaciones que coinciden con el filtro dado
	 * que se encuentran en la página indicada y ordenadas por el orden
	 * indicado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de delegaciones.
	 */
	public List<Delegation> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de delegaciones.
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, d.* from ("
			+ "  select d.* from v_delegation d <where> order by <order_by>"
			+ " ) d"
			+ ") d where d.rowno >= ((:page_number - 1) * :page_size + 1) and d.rowno \\<= (:page_number * :page_size)")
	abstract public List<Delegation> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 * Obtiene una lista todas las delegaciones que se le han asignado a un
	 * usuario.
	 *
	 * @param id_delegate
	 * @return
	 */
	@SqlQuery("select * from v_delegation where id_delegate = :id_delegate order by id")
	abstract public List<Delegation> getByDelegateID(@Bind("id_delegate") final long id_delegate);

	/**
	 * Obtiene la delegación que corresponde al ID indicado.
	 *
	 * @param id ID de la delegación que se buscará.
	 * @return Instancia de delegación.
	 */
	@SqlQuery("select * from v_delegation where id = :id")
	abstract public Delegation get(@Bind("id") final long id);

	/**
	 * Inserta la delegación indicada en la base de datos, asignándole un
	 * ID autoincremental con una secuencia de Oracle.
	 *
	 * @param item Instancia de delegación que se insertará.
	 * @return El ID de la delegación insertada.
	 */
	@SqlUpdate("insert into delegation(id, id_user, id_delegate, end_date) values(delegation_seq.nextVal, :id_user, :id_delegate, :end_date)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Delegation item);

	/**
	 * Actualiza en la tabla de delegaciones el registro que coincide con
	 * el ID de la instancia dada.
	 *
	 * @param item Instancia de delegación con los datos nuevos.
	 */
	@SqlUpdate("update delegation set "
			+ "	id_user = :id_user,"
			+ " id_delegate = :id_delegate,"
			+ " end_date = :end_date "
			+ "where id = :id")
	abstract public void update(@BindBean final Delegation item);

	/**
	 * Elimina de la tabla de delegaciones el registro que coindice con el
	 * ID indicado.
	 *
	 * @param id ID del registro que se eliminará.
	 * @return La cantidad de registros eliminados.
	 */
	@SqlUpdate("delete from delegation where id = :id")
	abstract public int delete(@Bind("id") final int id);

}
