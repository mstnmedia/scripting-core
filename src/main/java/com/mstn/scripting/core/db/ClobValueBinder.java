/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

/**
 * Clase utilizada para guardar en la base de datos valores de textos largos.
 *
 * @author MSTN Media
 */
@BindingAnnotation(ClobValueBinder.Factory.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface ClobValueBinder {

	/**
	 *
	 */
	public class Factory implements BinderFactory {

		@Override
		public Binder build(Annotation annotation) {
			return new Binder<ClobValueBinder, ClobValue>() {
				@Override
				public void bind(SQLStatement q, ClobValueBinder bind, ClobValue clause) {
					List<String> values = clause.getQueryValues();
					for (int i = 0; i < values.size(); i++) {
						String value = values.get(i);
						q.bind("value" + i, value);
					}
				}
			};
		}
	}
}
