/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Alert_Triggered;
import com.mstn.scripting.core.models.Alert_Triggered_Data;
import com.mstn.scripting.core.models.Alert_Triggered_Transaction;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con alertas disparadas.
 *
 * @author amatos
 */
public abstract class AlertTriggeredService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR = "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 * Constructor
	 */
	public AlertTriggeredService() {
	}

	AlertTriggeredDataService dataService;
	AlertTriggeredTransactionService aTTService;

	/**
	 *
	 * @param dataService
	 * @return
	 */
	public AlertTriggeredService setDataService(AlertTriggeredDataService dataService) {
		this.dataService = dataService;
		return this;
	}

	/**
	 *
	 * @param aTTService
	 * @return
	 */
	public AlertTriggeredService setTransactionService(AlertTriggeredTransactionService aTTService) {
		this.aTTService = aTTService;
		return this;
	}

	@CreateSqlObject
	abstract AlertTriggeredDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @return
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = null;
		if (!user.isMaster()) {
			userWheres = new Where("id_center", user.getCenterId());
		}
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 *
	 * @return
	 */
	public long getCount() {
		return dao().getCount();
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 *
	 * @param whereClause
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public Representation<List<Alert_Triggered>> getRepresentation(
			WhereClause whereClause, int pageSize, int pageNumber, String orderBy) {
		long totalRows = getCount(whereClause);
		List<Alert_Triggered> items = new ArrayList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 *
	 * @param where
	 * @param pageSize
	 * @param pageNumber
	 * @param orderBy
	 * @return
	 */
	public List<Alert_Triggered> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return loadChildren(dao().getAll(where, pageSize, pageNumber, orderBy));
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public List<Alert_Triggered> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return loadChildren(dao().getAll(where.getPreparedString(), where));
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Alert_Triggered getOnly(int id) {
		Alert_Triggered item = dao().get(id);
		return item;
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	public Alert_Triggered get(int id) {
		Alert_Triggered item = getOnly(id);
		if (item == null) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		loadData(item);
		return item;
	}

	/**
	 *
	 * @param item
	 * @return
	 */
	public Alert_Triggered loadData(Alert_Triggered item) {
		item.setData(dataService.getAll(item.getId()));
		return item;
	}

	/**
	 *
	 * @param triggereds
	 * @return
	 */
	public List<Alert_Triggered> loadChildren(List<Alert_Triggered> triggereds) {
		if (Utils.isNullOrEmpty(triggereds)) {
			return triggereds;
		}
		HashMap<Integer, Alert_Triggered> mapTriggereds = new HashMap();
		List<Integer> triggeredIDs = triggereds.stream()
				.map(item -> {
					mapTriggereds.put(item.getId(), item);
					item.setData(new ArrayList());
					return item.getId();
				})
				.collect(Collectors.toList());
		WhereClause where = new WhereClause(new Where("id_alert_triggered", Where.IN, triggeredIDs));
		List<Alert_Triggered_Data> datas = dataService.getAll(where);
		datas.forEach(data -> {
			Alert_Triggered triggered = mapTriggereds.get(data.getId_alert_triggered());
			triggered.getData().add(data);
		});
		return triggereds;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Alert_Triggered create(Alert_Triggered item, User user) {
		item.setId_employee(user.getId());
		if (item.getStart_date() == null) {
			item.setStart_date(new Date());
		}
		if ((item.getState() == 0 && item.getEnd_date() == null)
				|| (item.getEnd_date() != null && item.getStart_date().getTime() > item.getEnd_date().getTime())) {
			item.setEnd_date(new Date());
		}
		int id = dao().insert(item);
		item.setId(id);
		Alert_Triggered after = getOnly(id);
		log().insert(user.getId(), "Alertas disparadas", item.getId(), "Crear", item, after, user.getIp());

		after.setData(new ArrayList());
		List<Alert_Triggered_Data> datas = Utils.coalesce(item.getData(), new ArrayList());
		for (int i = 0; i < datas.size(); i++) {
			Alert_Triggered_Data data = datas.get(i);
			data.setId_alert_triggered(after.getId());
			data.setId_center(after.getId_center());
			Alert_Triggered_Data newData = dataService.create(data, user);
			after.getData().add(newData);
		}
		after.setTransaction(new ArrayList());
		if (after.getState() == 1) {
			List<Alert_Triggered_Transaction> aTTList = aTTService.getATTList(after.getId());
			for (Alert_Triggered_Transaction aTT : aTTList) {
				Alert_Triggered_Transaction newATT = aTTService.create(aTT, user);
				after.getTransaction().add(newATT);
			}
		}
		return after;
	}

	/**
	 *
	 * @param item
	 * @param user
	 * @return
	 */
	public Alert_Triggered update(Alert_Triggered item, User user) {
		Alert_Triggered before = get(item.getId());
		dao().update(item);
		Alert_Triggered after = get(item.getId());
		log().insert(user.getId(), "Alertas disparadas", before.getId(), "Actualizar", before, after, user.getIp());
		return after;
	}

	/**
	 *
	 * @param id
	 * @param user
	 * @return
	 */
	public String delete(final int id, User user) {
		Alert_Triggered before = get(id);
		int result = id == -1 ? -1 : dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Alertas disparadas", before.getId(), "Eliminar", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
