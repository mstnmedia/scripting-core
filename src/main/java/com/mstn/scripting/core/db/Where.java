/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Clase que representa una condición en un filtro que se realiza en cualquier
 * consulta del usuario a la base de datos del sistema.
 *
 * @author josesuero
 */
public class Where {

	/**
	 *
	 */
	static public final String AND = "and";

	/**
	 *
	 */
	static public final String OR = "or";

	/**
	 *
	 */
	static public final String EQUALS = "=";

	/**
	 *
	 */
	static public final String DESEQUALS = "!=";

	/**
	 *
	 */
	static public final String DESEQUALS2 = "<>";

	/**
	 *
	 */
	static public final String LIKE = "like";

	static public final String NOT_LIKE = "not like";

	/**
	 *
	 */
	static public final String LESS_THAN = "<";

	/**
	 *
	 */
	static public final String GREATER_THAN = ">";

	/**
	 *
	 */
	static public final String LESS_EQUALS_THAN = "<=";

	/**
	 *
	 */
	static public final String GREATER_EQUALS_THAN = ">=";

	/**
	 *
	 */
	static public final String IN = "in";

	/**
	 *
	 */
	static public final String NOT_IN = "not in";

	/**
	 *
	 */
	static public final String IS_NULL = "is null";

	/**
	 *
	 */
	static public final String NOT_IS_NULL = "not is null";

	/**
	 *
	 */
	static public final List<String> ALLOWED_CRITERIAS = Arrays.asList(
			EQUALS, DESEQUALS, DESEQUALS2,
			LIKE, NOT_LIKE, LESS_THAN, GREATER_THAN,
			LESS_EQUALS_THAN, GREATER_EQUALS_THAN,
			IN, NOT_IN, IS_NULL, NOT_IS_NULL
	);
	static public final int MAX_ITEMS_CLAUSE_IN = 1000;

	private String label;
	private String name;
	private String criteria;
	private Object value;
	private String separator;
	private String type;
	private List<Where> group = new ArrayList();

	/**
	 *
	 */
	public Where() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public Where(String name, Object value) {
		this.name = name;
		this.value = value;
	}

	/**
	 *
	 * @param name
	 * @param criteria
	 * @param value
	 */
	public Where(String name, String criteria, Object value) {
		this.name = name;
		this.criteria = criteria;
		this.value = value;
	}

	/**
	 *
	 * @param name
	 * @param criteria
	 * @param value
	 * @param separator
	 */
	public Where(String name, String criteria, Object value, String separator) {
		this.name = name;
		this.criteria = criteria;
		this.value = value;
		this.separator = separator;
	}

	/**
	 *
	 * @param name
	 * @param separator
	 * @param group
	 */
	public Where(String name, String separator, Where... group) {
		this.name = name;
		this.separator = separator;
		this.group.addAll(Arrays.asList(group));
	}

	/**
	 *
	 * @param label
	 * @param name
	 * @param criteria
	 * @param value
	 * @param separator
	 * @param type
	 */
	public Where(String label, String name, String criteria, Object value, String separator, String type) {
		this.label = label;
		this.name = name;
		this.criteria = criteria;
		this.value = value;
		this.separator = separator;
		this.type = type;
	}

	/**
	 *
	 * @param jsonList
	 * @return
	 * @throws Exception
	 */
	static public List<Where> listFrom(String jsonList) throws Exception {
		jsonList = Utils.stringNonNullOrEmpty(jsonList) ? jsonList : "[]";
		return new ArrayList(Arrays.asList(JSON.toObject(jsonList, Where[].class)));
	}

	/**
	 *
	 * @param where
	 * @return
	 * @throws Exception
	 */
	static public Where groupFrom(String where) throws Exception {
		if (Utils.stringIsNullOrEmpty(where)) {
			return null;
		}
		Where[] group = JSON.toObject(where, Where[].class);
		return groupFrom(group);

	}

	/**
	 *
	 * @param group
	 * @return
	 */
	static public Where groupFrom(Where... group) {
		if (group.length == 0) {
			return null;
		}
		return new Where("(", AND, group);
	}

	/**
	 *
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 *
	 * @param label
	 * @return
	 */
	public Where setLabel(String label) {
		this.label = label;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public Where setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getCriteria() {
		return criteria;
	}

	/**
	 *
	 * @param criteria
	 * @return
	 */
	public Where setCriteria(String criteria) {
		this.criteria = criteria;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public Object getValue() {
		return value;
	}

	/**
	 *
	 * @param value
	 * @return
	 */
	public Where setValue(Object value) {
		this.value = value;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 *
	 * @param separator
	 * @return
	 */
	public Where setSeparator(String separator) {
		this.separator = separator;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 *
	 * @param type
	 * @return
	 */
	public Where setType(String type) {
		this.type = type;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public List<Where> getGroup() {
		return group;
	}

	/**
	 *
	 * @param group
	 * @return
	 */
	public Where setGroup(List<Where> group) {
		this.group = group;
		return this;
	}

}
