/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db;

import java.sql.SQLException;
import java.sql.ResultSet;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para obtener el ID de los registros insertados.
 *
 * @author josesuero
 */
public class OracleGeneratedKeyMapper implements ResultSetMapper<Long> {

	@Override
	public Long map(int i, ResultSet rs, StatementContext sc) throws SQLException {
		return rs.getLong(1);
	}
}
