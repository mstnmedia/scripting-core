/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.Representation;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Employee;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;
import org.eclipse.jetty.http.HttpStatus;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con la vista de empleados.
 *
 * @author amatos
 */
public abstract class EmployeeService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 * Constructor
	 */
	public EmployeeService() {

	}

	private DelegationService delegationService;

	/**
	 * Obtiene la instancia del servicio de delegaciones disponible.
	 *
	 * @return
	 */
	public DelegationService getDelegationService() {
		return delegationService;
	}

	/**
	 * Configura la instancia del servicio de delegaciones disponible.
	 *
	 * @param delegationService
	 * @return
	 */
	public EmployeeService setDelegationService(DelegationService delegationService) {
		this.delegationService = delegationService;
		return this;
	}

	@CreateSqlObject
	abstract EmployeeDao dao();

	/**
	 * Obtiene un dao para acceder a la tabla de históricos.
	 *
	 * @return
	 */
	@CreateSqlObject
	abstract public LogDao log();

	/**
	 * Obtiene los filtros indicados modificados, si el usuario indicado no
	 * tiene el permiso Máster, para que el usuario sólo obtenga en la consulta
	 * información a la que tiene permiso.
	 *
	 * @param user Usuario que consulta.
	 * @param clientWhere Filtro que el usuario consulta.
	 * @return Instancia procesada de {@link WhereClause} con la lista de
	 * {@link Where} que especifica el usuario.
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = null;
		if (user.isMaster()) {
			List<Integer> treeDown = getEmployeeTreeDownInt(user.getId());
			userWheres = new Where("id_employee", Where.IN, treeDown);
		}
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 * Obtiene la cantidad total de registros en la tabla de empleados.
	 *
	 * @return
	 */
	public long getCount() {
		return dao().getCount();
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de empleados que coinciden
	 * con el filtro {@code where}.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @return
	 */
	public long getCount(WhereClause where) {
		return dao().getCount(where);
	}

	/**
	 * Obtiene una {@code Representation} con la lista de empleados que
	 * coinciden con el filtro dado, que se encuentran en la página indicada y
	 * ordenadas por el orden indicado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de empleados.
	 */
	public Representation<List<Employee>> getRepresentation(
			String where, int pageSize, int pageNumber, String orderBy) {
		WhereClause whereClause = new WhereClause(where);
		long totalRows = getCount(whereClause);
		List<Employee> items = Arrays.asList();
		if (totalRows > 0) {
			items = getAll(whereClause, pageSize, pageNumber, orderBy);
		}
		return new Representation<>(
				HttpStatus.OK_200, items, pageSize, pageNumber, totalRows
		);
	}

	/**
	 * Obtiene una lista de empleados que coinciden con el filtro dado, que se
	 * encuentran en la página indicada y ordenadas por el orden indicado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de empleados.
	 */
	public List<Employee> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		return dao().getAll(where, pageSize, pageNumber, orderBy);
	}

	/**
	 * Obtiene una lista de todos los empleados que cumplen los
	 * criterios del filtro consultado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @return Una lista de empleados.
	 */
	public List<Employee> getAll(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 * Obtiene una lista de empleados con el usuario que corresponde con el ID
	 * indicado y toda su cadena de mando hacia arriba.
	 *
	 * @param id ID de usuario del que se consulta la cadena de mando hacia
	 * arriba.
	 * @return Una lista de empleados
	 */
	public List<Employee> getEmployeeTreeUp(final int id) {
		List<Employee> list = new ArrayList<>();
		int auxID = 0;
		int parentID = id;
		while (parentID != 0 && parentID != auxID) {
			Employee employee = getOrNull(parentID);
			if (employee != null) {
				list.add(employee);
				auxID = employee.getId();
				parentID = employee.getId_parent();
			} else {
				parentID = 0;
			}
		}
		return list;
	}

	/**
	 * Obtiene una lista de enteros con la tarjeta del usuario que corresponde
	 * al ID indicado y las tarjetas de toda su cadena de mando hacia arriba.
	 *
	 * @param id ID de usuario del que se consulta la cadena de mando hacia
	 * arriba.
	 * @return Una lista de enteros.
	 */
	public List<Integer> getEmployeeTreeUpInt(final int id) {
		return getEmployeeTreeUp(id)
				.stream()
				.map(i -> i.getId())
				.collect(Collectors.toList());
	}

//	/** Función auxiliar para evitar bucles en la cadena de mando. */
//	void addDistinct(
//			List<Employee> list,
//			HashMap<Integer, Boolean> map,
//			Employee item) {
//		int key = item.getId();
//		if (!map.getOrDefault(key, false)) {
//			list.add(item);
//			map.put(key, true);
//		}
//	}
	/**
	 * Obtiene una lista de empleados con la cadena de mando del usuario con el
	 * ID indicado e incluye cadena de mando de cada uno de los usuarios a los
	 * que el usuario indicado delega.
	 *
	 * @param id ID de usuario del que se consulta la cadena de mando hacia
	 * abajo.
	 * @return Una lista de empleados.
	 */
	public List<Employee> getEmployeeTreeDown(final int id) {
		return dao().getTreeDown(id);
	}

	/*public List<Employee> getEmployeeTreeDown(final int id) {
		List<Employee> list = new ArrayList<>();
		HashMap<Integer, Boolean> loaded = new HashMap();
		Employee initial = getOrNull(id);
		if (initial != null) {
			addDistinct(list, loaded, initial);

			List<Integer> delegatingUsersInt = delegationService.getByDelegateID(initial.getId())
					.stream()
					.map(item -> item.getId_user())
					.collect(Collectors.toList());
			if (!delegatingUsersInt.isEmpty()) {
				List<Employee> delegatingEmployees = getAll(new WhereClause(new Where("id", Where.IN, delegatingUsersInt)));
				delegatingEmployees.forEach(item -> addDistinct(list, loaded, item));
			}
			for (int i = 0; i < list.size(); i++) {
				Employee current = list.get(i);
				List<Employee> reports = dao().getReports(current.getId());
				reports.forEach(item -> addDistinct(list, loaded, item));
			}
		}
		return list;
	}*/
	/**
	 * Obtiene una lista de tarjetas de empleados de la cadena de mando hacia
	 * abajo del usuario con el ID indicado e incluye las tarjetas de cadena de
	 * mando de cada uno de los usuarios a los que el usuario indicado delega.
	 *
	 * @param id ID de usuario del que se consulta la cadena de mando hacia
	 * abajo.
	 * @return Una lista de tarjetas de empleados.
	 */
	public List<Integer> getEmployeeTreeDownInt(final int id) {
		return dao().getTreeDownInt(id);
	}

	/*public List<Integer> getEmployeeTreeDownInt(final int id) {
		return getEmployeeTreeDown(id)
				.stream()
				.map(i -> i.getId())
				.collect(Collectors.toList());
	}
	 */
	/**
	 * Obtiene el empleado que corresponde al ID indicado.
	 *
	 * @param id ID del empleado que se buscará.
	 * @return Instancia de empleado.
	 */
	public Employee getOrNull(int id) {
		Employee item = dao().get(id);
		return item;
	}

	/**
	 * Obtiene al empleado que corresponde al ID indicado.
	 *
	 * @param id ID del empleado que se buscará.
	 * @return Instancia de empleado.
	 * @throws WebApplicationException si el registro no es encontrado en la base
	 * de datos.
	 */
	public Employee get(int id) {
		Employee item = getOrNull(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}

		return item;
	}

	/**
	 * Inserta el empleado y retorna el registro insertado.
	 *
	 * @param item Empleado que se insertará.
	 * @return
	 */
	public Employee create(Employee item) {
		int id = dao().insert(item);
		return get(id);
	}

	/**
	 * Actualiza en la base de datos el empleado con la nueva información.
	 *
	 * @param item Instancia de empleado con la nueva información.
	 * @return Una instancia del registro de empleado actualizado.
	 * @throws WebApplicationException si el usuario no es encontrado en la base
	 * de datos.
	 */
	public Employee update(Employee item) {
		get(item.getId());
		dao().update(item);
		return get(item.getId());
	}

//	/**
//	 *
//	 * @param id
//	 * @return
//	 */
//	public String delete(final int id) {
//		int result = 3; //dao().delete(id);
//		switch (result) {
//			case 1:
//				return SUCCESS;
//			case 0:
//				throw new WebApplicationException(String.format(NOT_FOUND, id), Response.Status.NOT_FOUND);
//			default:
//				throw new WebApplicationException(UNEXPECTED_ERROR, Response.Status.INTERNAL_SERVER_ERROR);
//		}
//	}
	/**
	 * Prueba si existe una conección sana a la base de datos, de lo contrario
	 * se provoca una excepción.
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
