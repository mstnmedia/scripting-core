/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.models.Log;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code log}.
 *
 * @author josesuero
 */
@RegisterMapper(LogDao.LogMapper.class)
@UseStringTemplate3StatementLocator
public abstract class LogDao {

	/**
	 * Clase que crea una instancia de {@link Log} desde un {@link ResultSet}
	 * que contiene el registro correspondiente en la tabla {@code log}.
	 */
	static public class LogMapper implements ResultSetMapper<Log> {

		/**
		 * Nombre del campo ID en la tabla {@code log}.
		 */
		private static final String ID = "id";
		/**
		 * Nombre del campo ID_EMPLOYEE en la tabla {@code log}.
		 */
		private static final String ID_EMPLOYEE = "id_employee";
		/**
		 * Nombre del campo TABLE_NAME en la tabla {@code log}.
		 */
		private static final String TABLE_NAME = "table_name";
		/**
		 * Nombre del campo ID_RECORD en la tabla {@code log}.
		 */
		private static final String ID_RECORD = "id_record";
		/**
		 * Nombre del campo ACTION en la tabla {@code log}.
		 */
		private static final String ACTION = "action";
		/**
		 * Nombre del campo EVENT_DATE en la tabla {@code log}.
		 */
		private static final String EVENT_DATE = "event_date";
		/**
		 * Nombre del campo BEFORE_VALUE en la tabla {@code log}.
		 */
		private static final String BEFORE_VALUE = "before_value";
		/**
		 * Nombre del campo AFTER_VALUE en la tabla {@code log}.
		 */
		private static final String AFTER_VALUE = "after_value";
		/**
		 * Nombre del campo IP en la tabla {@code log}.
		 */
		private static final String IP = "ip";

		@Override
		public Log map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Log(
					hasColumn.containsKey(ID) ? resultSet.getLong(ID) : 0,
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getLong(ID_EMPLOYEE) : 0,
					hasColumn.containsKey(TABLE_NAME) ? resultSet.getString(TABLE_NAME) : "",
					hasColumn.containsKey(ID_RECORD) ? resultSet.getLong(ID_RECORD) : 0,
					hasColumn.containsKey(ACTION) ? resultSet.getString(ACTION) : "",
					hasColumn.containsKey(EVENT_DATE) ? resultSet.getDate(EVENT_DATE) : null,
					hasColumn.containsKey(BEFORE_VALUE) ? resultSet.getString(BEFORE_VALUE) : "",
					hasColumn.containsKey(AFTER_VALUE) ? resultSet.getString(AFTER_VALUE) : "",
					hasColumn.containsKey(IP) ? resultSet.getString(IP) : ""
			);
		}
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de logs que coinciden con el
	 * filtro {@code where}.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 * Obtiene la cantidad de registros que coinciden con el filtro.
	 *
	 * @param where Texto vacío o con un filtro para la tabla {@code log} con
	 * sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return
	 */
	@SqlQuery("select count(id) from log <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene una lista de logs que coinciden con el filtro, ordenados por ID
	 * descendientemente.
	 *
	 * @param where Texto vacío o con un filtro para la tabla {@code log} con
	 * sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return Una lista de logs
	 */
	@SqlQuery("select * from log <where> order by ID desc")
	abstract public List<Log> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene un registro de la tabla {@code log} que corresponde al ID
	 * indicado.
	 *
	 * @param id ID del registro que se buscará.
	 * @return Una instancia de {@link Log}.
	 */
	@SqlQuery("select * from log where id = :id")
	abstract public Log get(@Bind("id") final long id);

	/**
	 * Inserta en la tabla {@code log} los datos de la la instancia de
	 * {@link Log} indicada.
	 *
	 * @param item
	 * @return El ID del registro insertado.
	 */
	@SqlUpdate("insert into log(id, id_employee, table_name, id_record, action, event_date, before_value, after_value, ip) "
			+ "values(log_seq.nextVal, :id_employee, :table_name, :id_record, :action, :event_date, :before_value, :after_value, :ip)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Log item);

	/**
	 * Inserta un registro en la tabla {@code log} con los datos indicados. Los
	 * valores de los objetos before_value y after_value son insertados tal
	 * cuáles sean enviados.
	 *
	 * @param id Siempre se le envía 0.
	 * @param id_employee ID del usuario que realizó la acción.
	 * @param table_name Nombre de la tabla que el usuario afectó.
	 * @param id_record ID del registro que fue afectado por el usuario.
	 * @param action Acción que realizó el usuario sobre el registro.
	 * @param event_date Fecha en que se realizó la acción.
	 * @param before_value Estado del registro antes de ser afectado por el
	 * usuario.
	 * @param after_value Estado del registro luego de la acción del usuario.
	 * @param ip IP desde la cual el usuario realizó la acción.
	 * @return El ID del registro insertado.
	 */
	public int insert(long id, long id_employee,
			String table_name, long id_record,
			String action, Date event_date,
			String before_value, String after_value,
			String ip
	) {
		Log item = new Log(id, id_employee,
				table_name, id_record,
				action, event_date,
				before_value, after_value,
				ip
		);
		return insert(item);
	}

	/**
	 * Inserta un registro en la tabla {@code log} con los datos indicados y la
	 * fecha actual. Los valores de los objetos before_value y after_value se
	 * intentan convertir a texto con mediante
	 * {@link JSON#toString(java.lang.Object)} i si falla, se guarda el registro
	 * con el valor convertido a texto con
	 * {@link String#valueOf(java.lang.Object)}.
	 *
	 * @param id_employee ID del usuario que realizó la acción.
	 * @param table_name Nombre de la tabla que el usuario afectó.
	 * @param id_record ID del registro que fue afectado por el usuario.
	 * @param action Acción que realizó el usuario sobre el registro.
	 * @param before_value Estado del registro antes de ser afectado por el
	 * usuario.
	 * @param after_value Estado del registro luego de la acción del usuario.
	 * @param ip IP desde la cual el usuario realizó la acción.
	 * @return El ID del registro insertado.
	 */
	public int insert(long id_employee,
			String table_name, long id_record,
			String action,
			Object before_value, Object after_value,
			String ip
	) {
		String before;
		String after;
		try {
			before = JSON.toString(before_value);
		} catch (Exception ex) {
			Utils.logException(LogDao.class, "Error serializing to json", ex);
			before = String.valueOf(before_value);
		}
		try {
			after = JSON.toString(after_value);
		} catch (Exception ex) {
			Utils.logException(LogDao.class, "Error serializing to json", ex);
			after = String.valueOf(after_value);
		}
		Log item = new Log(0, id_employee,
				table_name, id_record,
				action, new Date(),
				before, after,
				ip
		);
		return insert(item);
	}

//	@SqlUpdate("update log set "
//			+ "	name = :name,"
//			+ "	id_employee = :id_employee,"
//			+ "	deleted = :deleted"
//			+ "where id = :id")
//	abstract public void update(@BindBean final Log item);
//
	/**
	 * Elimina de la tabla de logs el registro que coindice con el ID indicado.
	 *
	 * @param id ID del registro que se eliminará.
	 * @return La cantidad de registros eliminados.
	 */
	@SqlUpdate("delete from log where id = :id")
	abstract public int delete(@Bind("id") final int id);

}
