/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Alert_Triggered;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code alert_triggered}.
 *
 * @author amatos
 */
@RegisterMapper(AlertTriggeredDao.AlertTriggeredMapper.class)
@UseStringTemplate3StatementLocator
public abstract class AlertTriggeredDao {

	/**
	 * Clase que crea una instancia de {@link Alert_Triggered} desde un
	 * {@link ResultSet} que contiene el registro correspondiente en la tabla
	 * {@code alert_triggered}
	 */
	static public class AlertTriggeredMapper implements ResultSetMapper<Alert_Triggered> {

		/** Nombre del campo ID en la vista de alertas disparadas. */
		private static final String ID = "id";
		/** Nombre del campo ID_CENTER en la vista de alertas disparadas. */
		private static final String ID_CENTER = "id_center";
		/** Nombre del campo ID_ALERT en la vista de alertas disparadas. */
		private static final String ID_ALERT = "id_alert";
		/** Nombre del campo START_DATE en la vista de alertas disparadas. */
		private static final String START_DATE = "start_date";
		/** Nombre del campo END_DATE en la vista de alertas disparadas. */
		private static final String END_DATE = "end_date";
		/** Nombre del campo STATE en la vista de alertas disparadas. */
		private static final String STATE = "state";
		/** Nombre del campo FIRST_TRANSACTION_DATE en la vista de alertas disparadas. */
		private static final String FIRST_TRANSACTION_DATE = "first_transaction_date";
		/** Nombre del campo TRANSACTIONS_COUNT en la vista de alertas disparadas. */
		private static final String TRANSACTIONS_COUNT = "transactions_count";
		/** Nombre del campo DESCRIPTION en la vista de alertas disparadas. */
		private static final String DESCRIPTION = "description";
		/** Nombre del campo PARENT_TICKET en la vista de alertas disparadas. */
		private static final String PARENT_TICKET = "parent_ticket";
		/** Nombre del campo ACTION en la vista de alertas disparadas. */
		private static final String ACTION = "action";
		/** Nombre del campo ID_EMPLOYEE en la vista de alertas disparadas. */
		private static final String ID_EMPLOYEE = "id_employee";
		/** Nombre del campo CENTER_NAME en la vista de alertas disparadas. */
		private static final String CENTER_NAME = "center_name";
		/** Nombre del campo ALERT_NAME en la vista de alertas disparadas. */
		private static final String ALERT_NAME = "alert_name";

		@Override
		public Alert_Triggered map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Alert_Triggered(
					hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : "",
					hasColumn.containsKey(ID_ALERT) ? resultSet.getInt(ID_ALERT) : 0,
					hasColumn.containsKey(ALERT_NAME) ? resultSet.getString(ALERT_NAME) : "",
					hasColumn.containsKey(START_DATE) ? resultSet.getDate(START_DATE) : null,
					hasColumn.containsKey(END_DATE) ? resultSet.getDate(END_DATE) : null,
					hasColumn.containsKey(STATE) ? resultSet.getInt(STATE) : 0,
					hasColumn.containsKey(FIRST_TRANSACTION_DATE) ? resultSet.getDate(FIRST_TRANSACTION_DATE) : null,
					hasColumn.containsKey(TRANSACTIONS_COUNT) ? resultSet.getLong(TRANSACTIONS_COUNT) : 0,
					hasColumn.containsKey(DESCRIPTION) ? resultSet.getString(DESCRIPTION) : "",
					hasColumn.containsKey(PARENT_TICKET) ? resultSet.getString(PARENT_TICKET) : "",
					hasColumn.containsKey(ACTION) ? resultSet.getString(ACTION) : "",
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getInt(ID_EMPLOYEE) : 0
			);
		}
	}

	/**
	 * Obtiene la cantidad total de registros en la tabla de alertas disparadas.
	 *
	 * @return
	 */
	public long getCount() {
		return getCount(null);
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de alertas disparadas que
	 * coinciden con el filtro {@code where}.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @return
	 */
	public long getCount(WhereClause where) {
		where = WhereClause.emptyIfNull(where);
		return getCount(where.getPreparedString(), where);
	}

	/**
	 * Obtiene la cantidad de registros en la tabla de alertas disparadas que
	 * coinciden con el filtro {@code where}.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return
	 */
	@SqlQuery("select count(id) from v_alert_triggered <where>")
	abstract public long getCount(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene una lista de todas las alertas disparadas que coinciden con el
	 * filtro dado, ordenadas por ID descendientemente.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return Una lista de alertas disparadas.
	 */
	@SqlQuery("select * from v_alert_triggered <where> order by ID desc")
	abstract public List<Alert_Triggered> getAll(@Define("where") String where, @BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene una lista de alertas disparadas que coinciden con el filtro dado,
	 * que se encuentran en la página indicada y ordenadas por el orden
	 * indicado.
	 *
	 * @param where Instancia de {@link WhereClause} que contenga el filtro que
	 * se aplicará a los registros.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de alertas disparadas.
	 */
	public List<Alert_Triggered> getAll(WhereClause where, int pageSize, int pageNumber, String orderBy) {
		where = WhereClause.emptyIfNull(where);
		return getAll(where.getPreparedString(), where, pageSize, pageNumber, orderBy);
	}

	/**
	 * Obtiene una lista de alertas disparadas que coinciden con el filtro dado,
	 * que se encuentran en la página indicada y ordenadas por el orden
	 * indicado.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @param pageSize Número de registros por página.
	 * @param pageNumber Número de página solicitada.
	 * @param orderBy Texto SQL por el que se ordenarán los registros (Ej.:
	 * "column1 desc, column2").
	 * @return Una lista de alertas disparadas.
	 */
	@SqlQuery("select * from ("
			+ " select rownum rowno, a.* from ("
			+ "  select a.* from v_alert_triggered a <where> order by <order_by>"
			+ " ) a"
			+ ") a where a.rowno >= ((:page_number - 1) * :page_size + 1) and a.rowno \\<= (:page_number * :page_size)")
	abstract public List<Alert_Triggered> getAll(
			@Define("where") String where,
			@BindWhereClause() WhereClause whereClause,
			@Bind("page_size") int pageSize,
			@Bind("page_number") int pageNumber,
			@Define("order_by") String orderBy
	);

	/**
	 * Obtiene la alerta disparada que corresponde al ID indicado.
	 *
	 * @param id ID de la alerta disparada que se buscará.
	 * @return Instancia de alerta disparada.
	 */
	@SqlQuery("select * from v_alert_triggered where id = :id")
	abstract public Alert_Triggered get(@Bind("id") final int id);

	/**
	 * Inserta la alerta disparada indicada en la base de datos, asignándole un
	 * ID autoincremental con una secuencia de Oracle.
	 *
	 * @param item Instancia de alerta disparada que se insertará.
	 * @return El ID de la alerta disparada insertada.
	 */
	@SqlUpdate("insert into alert_triggered(id, id_alert, start_date, end_date, state, first_transaction_date, transactions_count, description, parent_ticket, action, id_employee) "
			+ "values(alert_triggered_seq.nextVal, :id_alert, :start_date, :end_date, :state, :first_transaction_date, :transactions_count, :description, :parent_ticket, :action, :id_employee)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Alert_Triggered item);

	/**
	 * Actualiza en la tabla de alertas disparadas el registro que coincide con
	 * el ID de la instancia dada.
	 *
	 * @param item Instancia de alerta disparada con los datos nuevos.
	 */
	@SqlUpdate("update alert_triggered set"
			+ " id_alert = :id_alert, "
			+ " state = :state, "
			+ " start_date = :start_date, "
			+ " end_date = :end_date, "
			+ " first_transaction_date = :first_transaction_date, "
			+ " transactions_count = :transactions_count, "
			+ " description = :description, "
			+ " parent_ticket = :parent_ticket, "
			+ " action = :action "
			+ "where id = :id")
	abstract public void update(@BindBean final Alert_Triggered item);

	/**
	 * Elimina de la tabla de alertas disparadas el registro que coindice con el
	 * ID indicado.
	 *
	 * @param id ID del registro que se eliminará.
	 * @return La cantidad de registros eliminados.
	 */
	@SqlUpdate("delete from alert_triggered where id = :id")
	abstract public int delete(@Bind("id") final int id);
}
