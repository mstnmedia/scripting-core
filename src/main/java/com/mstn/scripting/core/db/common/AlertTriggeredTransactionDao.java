/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.models.Alert_Triggered_Transaction;
import com.mstn.scripting.core.db.BindWhereClause;
import com.mstn.scripting.core.db.OracleGeneratedKeyMapper;
import com.mstn.scripting.core.db.WhereClause;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Clase para interactuar con la tabla {@code alert_triggered_transaction}.
 *
 * @author amatos
 */
@RegisterMapper(AlertTriggeredTransactionDao.AlertTriggeredTransactionMapper.class)
@UseStringTemplate3StatementLocator
public abstract class AlertTriggeredTransactionDao {

	/**
	 * Clase que crea una instancia de {@link Alert_Triggered_Transaction} desde
	 * un {@link ResultSet} que contiene el registro correspondiente en la tabla
	 * {@code alert_triggered_transaction}
	 */
	static public class AlertTriggeredTransactionMapper implements ResultSetMapper<Alert_Triggered_Transaction> {

		/**
		 * Nombre del campo ID en la vista transacciones por alertas disparadas.
		 */
		private static final String ID = "id";
		/**
		 * Nombre del campo ID_ALERT_TRIGGERED en la vista transacciones por
		 * alertas disparadas.
		 */
		private static final String ID_ALERT_TRIGGERED = "id_alert_triggered";
		/**
		 * Nombre del campo ID_TRANSACTION en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String ID_TRANSACTION = "id_transaction";
		/**
		 * Nombre del campo TRIGGERER en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String TRIGGERER = "triggerer";
		/**
		 * Nombre del campo ID_ALERT en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String ID_ALERT = "id_alert";
		/**
		 * Nombre del campo ALERT_NAME en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String ALERT_NAME = "alert_name";
		/**
		 * Nombre del campo ID_CENTER en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String ID_CENTER = "id_center";
		/**
		 * Nombre del campo CENTER_NAME en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String CENTER_NAME = "center_name";
		/**
		 * Nombre del campo ID_EMPLOYEE en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String ID_EMPLOYEE = "id_employee";
		/**
		 * Nombre del campo EMPLOYEE_NAME en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String EMPLOYEE_NAME = "employee_name";
		/**
		 * Nombre del campo WORKFLOW_NAME en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String WORKFLOW_NAME = "workflow_name";
		/**
		 * Nombre del campo VERSION_ID_VERSION en la vista transacciones por
		 * alertas disparadas.
		 */
		private static final String VERSION_ID_VERSION = "version_id_version";
		/**
		 * Nombre del campo SUBSCRIBER_NO en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String SUBSCRIBER_NO = "subscriber_no";
		/**
		 * Nombre del campo START_DATE en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String START_DATE = "start_date";
		/**
		 * Nombre del campo END_DATE en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String END_DATE = "end_date";
		/**
		 * Nombre del campo END_CAUSE_NAME en la vista transacciones por alertas
		 * disparadas.
		 */
		private static final String END_CAUSE_NAME = "end_cause_name";

		@Override
		public Alert_Triggered_Transaction map(int i, ResultSet resultSet, StatementContext sc) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap<>();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			return new Alert_Triggered_Transaction(hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
					hasColumn.containsKey(ID_ALERT_TRIGGERED) ? resultSet.getInt(ID_ALERT_TRIGGERED) : 0,
					hasColumn.containsKey(ID_TRANSACTION) ? resultSet.getInt(ID_TRANSACTION) : 0,
					hasColumn.containsKey(TRIGGERER) ? resultSet.getBoolean(TRIGGERER) : false,
					hasColumn.containsKey(ID_CENTER) ? resultSet.getInt(ID_CENTER) : 0,
					hasColumn.containsKey(CENTER_NAME) ? resultSet.getString(CENTER_NAME) : "",
					hasColumn.containsKey(ID_ALERT) ? resultSet.getInt(ID_ALERT) : 0,
					hasColumn.containsKey(ALERT_NAME) ? resultSet.getString(ALERT_NAME) : "",
					hasColumn.containsKey(ID_EMPLOYEE) ? resultSet.getInt(ID_EMPLOYEE) : 0,
					hasColumn.containsKey(EMPLOYEE_NAME) ? resultSet.getString(EMPLOYEE_NAME) : "",
					hasColumn.containsKey(WORKFLOW_NAME) ? resultSet.getString(WORKFLOW_NAME) : "",
					hasColumn.containsKey(VERSION_ID_VERSION) ? resultSet.getString(VERSION_ID_VERSION) : "",
					hasColumn.containsKey(SUBSCRIBER_NO) ? resultSet.getString(SUBSCRIBER_NO) : "",
					hasColumn.containsKey(START_DATE) ? resultSet.getDate(START_DATE) : null,
					hasColumn.containsKey(END_DATE) ? resultSet.getDate(END_DATE) : null,
					hasColumn.containsKey(END_CAUSE_NAME) ? resultSet.getString(END_CAUSE_NAME) : "");
		}
	}

	/**
	 * Obtiene una lista de transacciones por alertas disparadas que coinciden
	 * con el filtro dado.
	 *
	 * @param where Texto vacío o con un filtro para la tabla
	 * {@code alert_triggered_transaction} con sintaxis SQL (where ...).
	 * @param whereClause Instancia de {@link WhereClause} desde la que se
	 * generó el filtro en texto.
	 * @return Una lista de alertas disparadas.
	 */
	@SqlQuery("select * from v_alert_triggered_transaction a <where>")
	abstract public List<Alert_Triggered_Transaction> getAll(@Define("where") String where,
			@BindWhereClause() WhereClause whereClause);

	/**
	 * Obtiene todas las transacciones por alertas disparadas que pertenecen a
	 * la alerta disparada indicada.
	 *
	 * @param id_alert_triggered ID de la alerta disparada.
	 * @return Una lista de las transacciones por alerta disparada de la alerta
	 * disparada indicada.
	 */
	@SqlQuery("select * from v_alert_triggered_transaction a where id_alert_triggered=:id_alert_triggered")
	abstract public List<Alert_Triggered_Transaction> getAll(@Bind("id_alert_triggered") int id_alert_triggered);

	/**
	 * Obtiene la transacción por alerta disparada que corresponde al ID
	 * indicado.
	 *
	 * @param id ID de la transaccion por alerta disparada que se buscará.
	 * @return Instancia de transacción por alerta disparada.
	 */
	@SqlQuery("select * from v_alert_triggered_transaction a where id = :id")
	abstract public Alert_Triggered_Transaction get(@Bind("id") final int id);

	/**
	 * Inserta la transacción por alerta disparada indicada en la base de datos,
	 * asignándole un ID autoincremental con una secuencia de Oracle.
	 *
	 * @param item Instancia de la transacción por alerta disparada que se
	 * insertará.
	 * @return El ID de la transacción por alerta disparada insertada.
	 */
	@SqlUpdate("insert into alert_triggered_transaction(id, id_alert_triggered, id_transaction, triggerer) "
			+ "values(alert_triggered_transaction_seq.nextVal, :id_alert_triggered, :id_transaction, :triggerer)")
	@GetGeneratedKeys(columnName = "id", value = OracleGeneratedKeyMapper.class)
	abstract public int insert(@BindBean final Alert_Triggered_Transaction item);

	/**
	 * Actualiza en la tabla de transacciones por alertas disparadas el registro
	 * que coincide con el ID de la instancia dada.
	 *
	 * @param item Instancia de transacción por alerta disparada con los datos
	 * nuevos.
	 */
	@SqlUpdate("update alert_triggered_transaction set"
			+ " id_alert_triggered = :id_alert,"
			+ " id_transaction = :id_transaction, "
			+ " triggerer = :triggerer "
			+ "where id = :id")
	abstract public void update(@BindBean final Alert_Triggered_Transaction item);

	/**
	 * Elimina de la tabla de transacciones por alertas disparadas el registro
	 * que coindice con el ID indicado.
	 *
	 * @param id ID del registro que se eliminará.
	 * @return La cantidad de registros eliminados.
	 */
	@SqlUpdate("delete from alert_triggered_transaction where id = :id")
	abstract public int delete(@Bind("id") final int id);

	/**
	 * Obtiene una lista de transacciones por alertas disparadas que 
	 */
	@SqlQuery("with triggered as("
			+ "	select * from alert_triggered where id=:id_alert_triggered"
			+ "), datas as ("
			+ "	select * from alert_triggered_data where id_alert_triggered=:id_alert_triggered"
			+ ")"
			+ "	select 0 id, :id_alert_triggered id_alert_triggered, t.id id_transaction, 1 triggerer"
			+ "	from transaction t"
			+ "	where t.id_center=(select id_center from triggered)"
			+ "		and t.start_date>=(select first_transaction_date from triggered)"
			+ "		and t.id in ("
			+ "			select tr.id_transaction"
			+ "			from transaction_result tr"
			+ "				join datas atd on tr.name=atd.name and dbms_lob.compare(tr.value, atd.value)=0"
			+ "			where tr.id_transaction=t.id"
			+ "			group by tr.id_transaction"
			+ "			having count(tr.id)=(select count(id) from datas)"
			+ "		)")
	abstract List<Alert_Triggered_Transaction> getATT(@Bind("id_alert_triggered") int id_alert_triggered);

}
