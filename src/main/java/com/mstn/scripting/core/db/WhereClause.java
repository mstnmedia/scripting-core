/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db;

import com.mstn.scripting.core.JSON;
import com.mstn.scripting.core.Utils;
import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.enums.DateFormat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.mutable.MutableInt;

/**
 * Clase que genera el código SQL del filtro de la consulta a la base de datos a
 * partir del conjunto de condiciones {@link Where} recibidos.
 *
 * @author josesuero
 */
public class WhereClause {

	/**
	 *
	 */
	public List<Where> queryValues = new ArrayList<>();

	/**
	 *
	 */
	public String preparedString = ""; // "WHERE foo=:foo AND bar=:baz"

	/**
	 *
	 * @param wc
	 * @return
	 */
	static public WhereClause emptyIfNull(WhereClause wc) {
		if (wc == null) {
			wc = new WhereClause();
		}
		return wc;
	}

	/**
	 *
	 */
	public WhereClause() {
	}

	/**
	 *
	 * @param jsonString
	 */
	public WhereClause(String jsonString) {
		if (Utils.stringIsNullOrEmpty(jsonString)) {
			return;
		}
		try {
			List<Where> fields = JSON.toList(jsonString, Where.class);
			this.queryValues = fields;

			this.calcQuery();
		} catch (IOException ex) {
			Logger.getLogger(WhereClause.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 *
	 * @param filters
	 */
	public WhereClause(Where... filters) {
		this.queryValues.addAll(Arrays.asList(filters));
		this.calcQuery();
	}

	/**
	 *
	 * @param queryValues
	 */
	public WhereClause(List<Where> queryValues) {
		this.queryValues = queryValues;
		this.calcQuery();
	}

	/**
	 *
	 * @param where
	 * @return
	 */
	public WhereClause add(Where... where) {
		this.queryValues.addAll(Arrays.asList(where));
		return this;
	}

	String getSeparator(Where where, int idx) {
		if (idx == 0) {
			return "";
		}
		String separator = Utils.coalesce(where.getSeparator(), "")
				.trim().toLowerCase();
		if (!Where.AND.equals(separator) && !Where.OR.equals(separator)) {
			separator = Where.AND;
		}
		return separator;
	}

	String getCriteria(Where where) {
		String criteria = where.getCriteria();
		if (Utils.isNullOrEmpty(criteria)) {
			criteria = Where.EQUALS;
		} else {
			criteria = criteria.trim().toLowerCase();
			if (!Where.ALLOWED_CRITERIAS.contains(criteria)) {
				criteria = Where.EQUALS;
			}
			criteria = criteria.replaceAll("<", "\\<");
		}
		return criteria;
	}

	String getSimpleCondition(Where where, MutableInt labelCount, Integer idx) {
		if (isGroupCondition(where)) {
			return getGroupCondition(where, labelCount, idx);
		}
		String separator = getSeparator(where, idx);
		String columnName = getColumnNameOf(where);
		String criteria = getCriteria(where);
		String name = Utils.coalesce(where.getName(), "");

		String label = name + labelCount.incrementAndGet();
		where.setLabel(label);

		String type = Utils.coalesce(where.getType(), "").trim().toLowerCase();

		String value = ":" + label;

		if (Where.IN.equals(criteria) || Where.NOT_IN.equals(criteria)) {
			List values = (List) where.getValue();
			if (Utils.listIsNullOrEmpty(values)) {
				// Como explota cuando no se le envía valores, 
				// se usa 'where f in (null)' porque no explota y no trae nada.
				value = "(null)";
			} else {
				if (values.size() > Where.MAX_ITEMS_CLAUSE_IN) {
					// Separar en varias clausulas IN si sobrepasa el límite
					List<Where> groups = new ArrayList();
					for (int i = 0; i < values.size(); i += Where.MAX_ITEMS_CLAUSE_IN) {
						groups.add(
								new Where(
										label, name, criteria,
										values.subList(i, Math.min(i + Where.MAX_ITEMS_CLAUSE_IN, values.size())),
										Where.OR, //Debe tener OR para formar (f in (...) or f in (...))
										type //Debe tener el mismo tipo para trabajar columnas tipo fecha y clob
								)
						);
					}
					where.setGroup(groups);
					labelCount.decrement();
					return getGroupCondition(where, labelCount, idx);
				}
				value = "(";
				for (int i = 0; i < values.size(); i++) {
					value += i == 0 ? "" : ", ";
					value += ":" + label + i;
				}
				value += ")";
			}
		} else if (Where.LIKE.equals(criteria)||Where.NOT_LIKE.equals(criteria)) {
			columnName = "lower(" + columnName + ")";
			value = "lower(" + value + ")";
		} else if (Where.IS_NULL.equals(criteria) || Where.NOT_IS_NULL.equals(criteria)) {
			value = "";
		}
		if ("clob".equals(type)) {
			columnName = "to_clob(" + columnName + ")";
			value = "to_clob(" + value + ")";
			if (Where.EQUALS.equals(criteria) || Where.DESEQUALS.equals(criteria)) {
				columnName = "dbms_lob.compare(" + columnName + ", " + value + ")";
				value = "0";
			}
		} else if ("date".equals(type)) {
			columnName = "trunc(" + columnName + ", 'DD')";
			value = "to_date(" + value + ", '" + DateFormat.ORACLE_DATE_FORMAT + "')";
		} else if ("datetime".equals(type)) {
			columnName = "cast(trunc(" + columnName + ", 'MI') as timestamp)";
			value = "to_timestamp(" + value + ", '" + DateFormat.ORACLE_TIMESTAMP_FORMAT + "')";
		}

		//if (Where.IN.equals(criteria)) {
		//	value += ")";
		//}
		return new StringBuilder()
				.append(" ")
				.append(separator)
				.append(" ")
				.append(columnName)
				.append(" ")
				.append(criteria)
				.append(" ")
				.append(value)
				.toString();
	}

	String getGroupCondition(Where iWhere, MutableInt labelCount, Integer idx) {
		List<Where> wheres = iWhere.getGroup();
		wheres.removeIf(item -> item == null);
		StringBuilder clause = new StringBuilder();
		if (wheres.size() > 0) {
			String separator = getSeparator(iWhere, idx);
			clause.append(" ")
					.append(separator)
					.append(" ")
					.append("(");
			for (int j = 0; j < wheres.size(); j++) {
				Where jWhere = wheres.get(j);
				clause.append(getSimpleCondition(jWhere, labelCount, j));
			}
			clause.append(")");
		}

		return clause.toString();
	}

	boolean isGroupCondition(Where where) {
		boolean isGroup = Utils.listNonNullOrEmpty(where.getGroup());
		return isGroup;
	}

	String getListCondition(List<Where> wheres) {
		if (Utils.listIsNullOrEmpty(wheres)) {
			return "";
		}
		StringBuilder clause = new StringBuilder();
		MutableInt labelCount = new MutableInt(0);
		wheres.removeIf(where -> where == null);
		for (int i = 0; i < wheres.size(); i++) {
			Where where = wheres.get(i);
			if (isGroupCondition(where)) {
				clause.append(getGroupCondition(where, labelCount, i));
			} else {
				clause.append(getSimpleCondition(where, labelCount, i));
			}
		}
		return clause.toString();
	}

	/**
	 *
	 * @return
	 */
	final public WhereClause calcQuery() {
		try {
			String conditions = getListCondition(queryValues).trim();
			if (conditions.length() > 0) {
				preparedString = "where " + conditions;
			}
		} catch (Exception ex) {
			Logger.getLogger(WhereClause.class.getName()).log(Level.SEVERE, "Error in calcQuery", ex);
			throw ex;
		}
		return this;
	}

	/**
	 *
	 * @param user
	 * @param clientWhere
	 * @param userWhere
	 * @return
	 */
	static public WhereClause getWhere(User user, List<Where> clientWhere, Where userWhere) {
		WhereClause whereClause = new WhereClause();
		if (!user.isMaster()) {
			whereClause.add(userWhere);
		}
		if (Utils.listNonNullOrEmpty(clientWhere)) {
			whereClause.add(new Where().setGroup(clientWhere));
		}
		return whereClause.calcQuery();
	}

	/**
	 * @param where
	 * @return the column name formatted.
	 */
	final public String getColumnNameOf(Where where) {
		return where.getName();
	}

	/**
	 *
	 * @return
	 */
	public List<Where> getQueryValues() {
		return this.queryValues;
	}

	/**
	 *
	 * @param queryValues
	 */
	public void setQueryValues(List<Where> queryValues) {
		this.queryValues = queryValues;
	}

	/**
	 *
	 * @return
	 */
	public String getPreparedString() {
		return preparedString;
	}

	/**
	 *
	 * @param preparedString
	 */
	public void setPreparedString(String preparedString) {
		this.preparedString = preparedString;
	}

}
