/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.db.common;

import com.mstn.scripting.core.auth.jwt.User;
import com.mstn.scripting.core.db.LogDao;
import com.mstn.scripting.core.db.Where;
import com.mstn.scripting.core.db.WhereClause;
import com.mstn.scripting.core.models.Alert_Triggered_Transaction;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.WebApplicationException;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

/**
 * Clase para interactuar con transacciones por alertas disparadas.
 *
 * @author amatos
 */
public abstract class AlertTriggeredTransactionService {

	private static final String NOT_FOUND = "item id %s not found.";
	private static final String DATABASE_REACH_ERROR
			= "Could not reach the database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR
			= "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String DATABASE_UNEXPECTED_ERROR
			= "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success...";
	private static final String UNEXPECTED_ERROR = "An unexpected error occurred while deleting item.";

	/**
	 * Constructor
	 */
	public AlertTriggeredTransactionService() {
	}

	@CreateSqlObject
	abstract AlertTriggeredTransactionDao dao();

	@CreateSqlObject
	abstract LogDao log();

	/**
	 * Función que recibe los filtros y el usuario de la consulta para modificar
	 * el filtro para que el usuario sólo obtenga información a la que tiene
	 * permiso, si no tiene el permiso Máster.
	 *
	 * @param user Usuario que consulta.
	 * @param clientWhere Filtro que el usuario consulta.
	 * @return Instancia procesada de {@link WhereClause} con la lista de
	 * {@link Where} que especifica el usuario.
	 */
	public WhereClause getUserWhere(User user, List<Where> clientWhere) {
		Where userWheres = new Where("id_center", user.getCenterId());
		return WhereClause.getWhere(user, clientWhere, userWheres);
	}

	/**
	 * Obtiene una lista de todas las transacciones por alertas disparadas que
	 * cumplen los criterios del filtro consultado.
	 *
	 * @param where Instancia procesada de {@link WhereClause} que contenga el
	 * filtro que se aplicará a los registros.
	 * @return Una lista de variables de alertas disparadas.
	 */
	public List<Alert_Triggered_Transaction> getAll(WhereClause where) {
		return dao().getAll(where.getPreparedString(), where);
	}

	/**
	 * Obtiene una lista de todas las transacciones por alerta disparada de una
	 * alerta disparada indicada.
	 *
	 * @param id_alert_triggered ID de la alerta disparada.
	 * @return Una lista de transacciones por alertas disparadas.
	 */
	public List<Alert_Triggered_Transaction> getAll(int id_alert_triggered) {
		return dao().getAll(id_alert_triggered);
	}

	/**
	 * Obtiene una lista de instancias de transacciones por alerta disparada
	 * correspondientes a la alerta disparada indicada para que sean creadas
	 * junto a ella en base de datos.
	 *
	 * @param id_alert_triggered ID de la alerta disparada indicada.
	 * @return Una lista de transacciones por alertas disparadas con el campo ID
	 * igual a cero.
	 */
	public List<Alert_Triggered_Transaction> getATTList(int id_alert_triggered) {
		return dao().getATT(id_alert_triggered);
	}

	/**
	 * Obtiene la transacción por alerta disparada que corresponde al ID
	 * indicado.
	 *
	 * @param id ID de la transaccion por alerta disparada que se buscará.
	 * @return Instancia de transacción por alerta disparada.
	 */
	public Alert_Triggered_Transaction getOnly(int id) {
		Alert_Triggered_Transaction item = dao().get(id);
		return item;
	}

	/**
	 * Obtiene la transacción por alerta disparada que corresponde al ID
	 * indicado.
	 *
	 * @param id ID de la transaccion por alerta disparada que se buscará.
	 * @return Instancia de transacción por alerta disparada.
	 * @throws WebApplicationException Cuando no se encuentra en la base de
	 * datos el registro con el ID indicado.
	 */
	public Alert_Triggered_Transaction get(int id) throws WebApplicationException {
		Alert_Triggered_Transaction item = getOnly(id);
		if (Objects.isNull(item)) {
			throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
		}
		return item;
	}

	/**
	 * Inserta la transacción por alerta disparada indicada en la base de datos
	 * y guarda un histórico del cambio.
	 *
	 * @param item Instancia de la transacción por alerta disparada que se
	 * insertará.
	 * @param user Usuario que realiza esta acción.
	 * @return Una instancia de la transacción por alerta disparada insertada.
	 */
	public Alert_Triggered_Transaction create(Alert_Triggered_Transaction item, User user) {
		int id = dao().insert(item);
		Alert_Triggered_Transaction after = get(id);
		log().insert(user.getId(), "Alert_Triggered_Transaction", id, "create", item, after, user.getIp());
		return after;
	}

	/**
	 * Actualiza la transacción por alerta disparada que coincide con el ID de
	 * la instancia dada y guarda un histórico del cambio.
	 *
	 * @param item Instancia de la transacción por alerta disparada que se
	 * insertará.
	 * @param user Usuario que realiza esta acción.
	 * @return Una instancia de la transacción por alerta disparada modificada.
	 */
	public Alert_Triggered_Transaction update(Alert_Triggered_Transaction item, User user) {
		Alert_Triggered_Transaction before = get(item.getId());
		dao().update(item);
		Alert_Triggered_Transaction after = get(item.getId());
		log().insert(user.getId(), "Alert_Triggered_Transaction", before.getId(), "update", before, after, user.getIp());
		return after;
	}

	/**
	 * Elimina la transacción por alerta disparada que coindice con el ID
	 * indicado y guarda un histórico del cambio.
	 *
	 * @param id ID de la transacción por alerta disparada que se insertará.
	 * @param user Usuario que realiza esta acción.
	 * @return String indicando que la acción fue satisfactoria.
	 */
	public String delete(final int id, User user) {
		Alert_Triggered_Transaction before = get(id);
		int result = dao().delete(id);
		switch (result) {
			case 1:
				log().insert(user.getId(), "Alert_Triggered_Transaction", before.getId(), "delete", before, null, user.getIp());
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(NOT_FOUND, id), Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @return
	 */
	public String performHealthCheck() {
		try {
			dao().get(0);
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
