/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 *
 * @author MSTN Media
 */
public enum ContentTypes {

	/**
	 * Tipo HTML
	 */
	HTML(1),
	/**
	 * Tipo Imagen
	 */
	IMAGE(2),
	/**
	 * Tipo Archivo
	 */
	FILE(3);

	private int value;

	ContentTypes(int value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getValue() {
		return value;
	}

}
