/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 * Clase que contiene los valores posibles como tipos de campos de las
 * interfaces.
 *
 * @author amatos
 */
public class InterfaceFieldTypes {

	/**
	 *
	 */
	final public static int TEXT = (1);

	/**
	 *
	 */
	final public static int NUMBER = (2);

	/**
	 *
	 */
	final public static int OPTIONS = (3);

	/**
	 *
	 */
	final public static int EMAIL = (4);

	/**
	 *
	 */
	final public static int CHECKBOX = (5);

	/**
	 *
	 */
	final public static int DATE = (6);

	/**
	 *
	 */
	final public static int DATETIME = (7);

	/**
	 *
	 */
	final public static int RADIO = (8);

	/**
	 *
	 */
	final public static int AUTOCOMPLETE = (9);

	/**
	 *
	 */
	final public static int TEXTAREA = (10);

	/**
	 *
	 */
	final public static int LINK = (11);

	/**
	 *
	 */
	final public static int HIDDEN = (12);

	/**
	 *
	 */
	final public static int TIME = (13);

	/**
	 *
	 */
	final public static int BOOLEAN = (14);

	/**
	 *
	 */
	final public static int TAGS = (15);

	/**
	 *
	 */
	final public static int HTML = (16);

	/**
	 *
	 */
	final public static int TABLE = (17);

	/**
	 *
	 */
	final public static int PASSWORD = (18);

	/**
	 *
	 */
	final public static int JSON = (19);

	// TODO: Validate each field of each row-column
	/**
	 *
	 */
	final public static String VALIDATE_TABLE = "return Array.isArray(value)"
			+ " && (!field.required || value.length) ";

	/**
	 *
	 */
	final public static String VALIDATE_BASE = "return !(!!value || value == 0 || value == false)"
			+ " ? !field.required"
			+ " : ";
	final public static String VALIDATE_TEXTUAL_LENGTH = VALIDATE_BASE
			+ "(value == null || value == NaN || value == undefined ? 0 : (new String(value)).length) @operator @length;";

	/**
	 *
	 */
	final public static String VALIDATE_JSON = ""
			+ "var valid = false;\n"
			+ "try {"
			+ "    valid = !!JSON.parse(value);"
			+ "} catch(error) {"
			+ "    console.log('El campo Fuente no tiene formato JSON: ' + value, error);"
			+ "}\n"
			+ VALIDATE_BASE + "valid;";

	/**
	 *
	 */
	final public static String VALIDATE_TEXT = VALIDATE_BASE + "!!value;";

	/**
	 *
	 */
//	final public static String VALIDATE_CHAR = VALIDATE_BASE + "(new String(value)).length == 1;";
	final public static String VALIDATE_CHAR = VALIDATE_TEXTUAL_LENGTH
			.replace("@operator", "==")
			.replace("@length", "1");

	/**
	 *
	 */
	final public static String VALIDATE_DATE = VALIDATE_BASE + "Utils.isDate(value);";

	/**
	 *
	 */
	final public static String VALIDATE_DATETIME = VALIDATE_BASE + "Utils.isDateTime(value);";

	/**
	 *
	 */
	final public static String VALIDATE_TIME = VALIDATE_BASE + "Utils.isTime(value);";

	/**
	 *
	 */
	final public static String VALIDATE_EMAIL = VALIDATE_BASE + "Utils.isEmail(value);";

	/**
	 *
	 */
	final public static String VALIDATE_BOOLEAN = VALIDATE_BASE + "[true, false, 'true', 'false'].indexOf(value) > -1;";

	/**
	 *
	 */
	final public static String VALIDATE_PHONE = VALIDATE_BASE + "Utils.isValidPhone(value);";
	final public static String VALIDATE_DASHED_PHONE = VALIDATE_BASE + "Utils.isValidDashedPhone(value);";

	/**
	 *
	 */
	final public static String VALIDATE_NUMBER = VALIDATE_BASE + "Utils.isNumber(value) && value != 0 ";

	/**
	 *
	 */
	final public static String VALIDATE_FLOAT = VALIDATE_NUMBER + "&& Utils.isFloat(value);";

	/**
	 *
	 */
	final public static String VALIDATE_DOUBLE = VALIDATE_NUMBER + "&& Utils.isFloat(value);";

	/**
	 *
	 */
	final public static String VALIDATE_LONG = VALIDATE_NUMBER + "&& Utils.isInt(value) ";

	/**
	 *
	 */
	final public static String VALIDATE_INT = VALIDATE_LONG + "&& value >= -2147483648 && value <= 2147483647;";

	/**
	 *
	 */
	final public static String VALIDATE_SHORT = VALIDATE_LONG + "&& value >= -32768 && value <= 32767;";

	/**
	 *
	 */
	final public static String VALIDATE_BYTE = VALIDATE_LONG + "&& value >= -128 && value <= 127;";

}
