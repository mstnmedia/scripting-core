/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 *
 * @author MSTN Media
 */
public enum WorkflowTypes {
	//TODO: Evaluar usar este enum como tipo de dato para el campo id_type de la clase Workflow

	/**
	 * Flujos principales
	 */
	MAIN(1),
	/**
	 * Flujos secundarios
	 */
	SUB_WORKFLOW(0);

	private final int value;

	WorkflowTypes(int value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getValue() {
		return value;
	}
}
