/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 *
 * @author MSTN Media
 */
public enum WorkflowStepTypes {
	//TODO: Evaluar usar este enum como tipo de dato para el campo id_type de la clase Workflow_Step

	/**
	 *
	 */
	UNDEFINED(-1),

	/**
	 *
	 */
	REGULAR(1),

	/**
	 *
	 */
	WORKFLOW(2),

	/**
	 *
	 */
	EXTERNAL(3);
	
    private int value;

    WorkflowStepTypes(int value) {
        this.value = value;
    }

	/**
	 *
	 * @return
	 */
	public int getValue() {
        return value;
    }
	
}