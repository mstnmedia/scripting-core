/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 * Clase que representa los posibles valores que se pueden asignar como unidad
 * de tiempo a las alertas.
 *
 * @author amatos
 */
public class AlertTimeUnits {

	/**
	 * Últimos minutos.
	 */
	final public static int MINUTE = 1;

	/**
	 * Últimas horas.
	 */
	final public static int HOUR = 2;

	/**
	 * Últimos días.
	 */
	final public static int DAY = 3;

	/**
	 * Últimos meses.
	 */
	final public static int MONTH = 4;

}
