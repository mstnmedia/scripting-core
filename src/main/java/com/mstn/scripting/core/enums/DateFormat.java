/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 * Clase con los formatos de fecha como se usan en Java y en los queries
 * ejecutados en Oracle.
 *
 * @author amatos
 */
public enum DateFormat {

	/**
	 *
	 */
	DATETIME_FULL("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
	/**
	 *
	 */
	ISO_DATE("yyyy-MM-dd"),
	/**
	 *
	 */
	DATE_TIME("yyyy-MM-dd'T'HH:mm:ss"),
	/**
	 *
	 */
	ORACLE_DATE("YYYY-MM-DD"),
	/**
	 *
	 */
	ORACLE_TIMESTAMP("YYYY-MM-DD\"T\"HH24:MI:SS"),
	/**
	 *
	 */
	DDMMYYYY_A_LAS_HHMMAA("dd/MMM/YYYY' a las 'hh:mmaa");

	/**
	 *
	 */
	static public final String ISO_DATE_FORMAT = ISO_DATE.getValue();

	/**
	 *
	 */
	static public final String DATE_TIME_FORMAT = DATE_TIME.getValue();

	/**
	 *
	 */
	static public final String ORACLE_DATE_FORMAT = ORACLE_DATE.getValue();

	/**
	 *
	 */
	static public final String ORACLE_TIMESTAMP_FORMAT = ORACLE_TIMESTAMP.getValue();

	/**
	 *
	 */
	static public final String DDMMYYYY_A_LAS_HHMMAA_FORMAT = DDMMYYYY_A_LAS_HHMMAA.getValue();

	private final String value;

	DateFormat(String value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public String getValue() {
		return value;
	}

}
