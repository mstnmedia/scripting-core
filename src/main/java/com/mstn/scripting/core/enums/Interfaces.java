/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 * Clase que contiene valores estáticos de nombres y números de interfaces del
 * sistema.
 *
 * @author amatos
 */
public class Interfaces {

	/**
	 *
	 */
	static public final int TRANSACTION_INFO_INTERFACE_ID = 107;

	/**
	 *
	 */
	static public final int TRANSACTION_INFO_INTERFACE_ID_BASE = -1;

	/**
	 *
	 */
	static public final String TRANSACTION_INFO_INTERFACE_NAME = "transaction_info";

	/**
	 *
	 */
	static public final int EMPLOYEE_INFO_INTERFACE_ID = -2;

	/**
	 *
	 */
	static public final String EMPLOYEE_INFO_INTERFACE_NAME = "employee_info";

	/**
	 *
	 */
	static public final int CUSTOMER_INFO_INTERFACE_ID = 2010;

	/**
	 *
	 */
	static public final int CUSTOMER_INFO_INTERFACE_ID_BASE = -5;
	// When you change this name, also you must update enums.js in reactadmin proyect.

	/**
	 *
	 */
	static public final String CUSTOMER_INFO_INTERFACE_NAME = "crm_customer_info";

	/**
	 *
	 */
	static public final String CUSTOMER_INFO_INTERFACE_NAME_BASE = "customer_info";

	/**
	 *
	 */
	static public final int SUBSCRIPTION_INFO_INTERFACE_ID = 3000;

	/**
	 *
	 */
	static public final int SUBSCRIPTION_INFO_INTERFACE_ID_BASE = -4;

	/**
	 *
	 */
	static public final String SUBSCRIPTION_INFO_INTERFACE_NAME = "ensamble_subscription_by_phone";
	// When you change this name, also you must update enums.js in reactadmin proyect.

	/**
	 *
	 */
	static public final String SUBSCRIPTION_INFO_INTERFACE_NAME_BASE = "subscription_info";

	/**
	 *
	 */
	static public final String ID_CATEGORY = TRANSACTION_INFO_INTERFACE_NAME + "_id_category";

	/**
	 *
	 */
	static public final String CATEGORY_NAME = TRANSACTION_INFO_INTERFACE_NAME + "_category_name";

	/**
	 *
	 */
	static public final int CASE_HISTORIES_ID = 5000;
}
