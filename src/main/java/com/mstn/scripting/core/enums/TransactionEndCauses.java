/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 * @author MSTN Media
 * @deprecated Se implementó un mantenimiento para que pueda ser configurado por
 * el usuario.
 */
public enum TransactionEndCauses {
	//TODO: Evaluar usar este enum como tipo de dato para el campo state de la clase Transaction

	/**
	 *
	 */
	END(0),
	/**
	 *
	 */
	CALL_FAILURE(1),
	/**
	 *
	 */
	WRONG_CALLER(2);

	private int value;

	TransactionEndCauses(int value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getValue() {
		return value;
	}

}
