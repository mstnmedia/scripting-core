/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 * Clase que contiene las posibles respuestas de una interfaz al microservicio
 * de Transacciones.
 *
 * @author amatos
 */
public enum TransactionInterfaceActions {

	/**
	 * Le indica que ha ocurrido un error realizando el proceso.
	 */
	ERROR(-2),
	/**
	 * Le indica que se han faltado datos de los que se esperaban.
	 */
	INVALID_REQUEST(-1),
	/**
	 * Le indica que muestre un formulario al usuario para pedirle información.
	 */
	SHOW_FORM(1),
	/**
	 * Le indica que pueda pasar al siguiente paso.
	 */
	NEXT_STEP(2);

	private final int value;

	TransactionInterfaceActions(int value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getValue() {
		return value;
	}

}
