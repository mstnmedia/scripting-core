package com.mstn.scripting.core.enums;

/**
 * Clase que contiene los valores que pueden ser asignados como tipos de las categorías.
 * @author amatos
 */
public class CategoryTypes {

	/**
	 * CRM Tipo 1
	 */
	static public final String TYPE1 = "type1";

	/**
	 * CRM Tipo 2
	 */
	static public final String TYPE2 = "type2";

	/**
	 * CRM Tipo 3
	 */
	static public final String TYPE3 = "type3";

	/**
	 * Scripting Tipo Acción
	 */
	static public final String ACTION = "action";

	/**
	 *
	 */
	static public final String TYPE_LABEL = "Categoría Tipo ";

	/**
	 *
	 */
	static public final String ACTION_LABEL = "Categoría Acción";

	/**
	 *
	 */
	static public final String PREFIX = Interfaces.TRANSACTION_INFO_INTERFACE_NAME + "_category_";

	/**
	 *
	 */
	static public final String TYPE1_ID = PREFIX + TYPE1 + "_id";

	/**
	 *
	 */
	static public final String TYPE2_ID = PREFIX + TYPE2 + "_id";

	/**
	 *
	 */
	static public final String TYPE3_ID = PREFIX + TYPE3 + "_id";

	/**
	 *
	 */
	static public final String TYPE1_NAME = PREFIX + TYPE1 + "_name";

	/**
	 *
	 */
	static public final String TYPE2_NAME = PREFIX + TYPE2 + "_name";

	/**
	 *
	 */
	static public final String TYPE3_NAME = PREFIX + TYPE3 + "_name";

	/**
	 *
	 */
	static public final String ID_LABEL = "Categoría Tipo # ID";

	/**
	 *
	 */
	static public final String NAME_LABEL = "Categoría Tipo # Nombre ";

	/**
	 *
	 */
	static public final String TYPE1_ID_LABEL = ID_LABEL.replace("#", "1");

	/**
	 *
	 */
	static public final String TYPE2_ID_LABEL = ID_LABEL.replace("#", "2");

	/**
	 *
	 */
	static public final String TYPE3_ID_LABEL = ID_LABEL.replace("#", "3");

	/**
	 *
	 */
	static public final String TYPE1_NAME_LABEL = NAME_LABEL.replace("#", "1");

	/**
	 *
	 */
	static public final String TYPE2_NAME_LABEL = NAME_LABEL.replace("#", "2");

	/**
	 *
	 */
	static public final String TYPE3_NAME_LABEL = NAME_LABEL.replace("#", "3");
}
