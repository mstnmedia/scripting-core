package com.mstn.scripting.core.enums;

/**
 * @author amatos
 */
public class InterfaceOptions {

	/**
	 *
	 */
	static public final String SUCCESS = "true";

	/**
	 *
	 */
	static public final String MISSING_DATA = "false";

	/**
	 *
	 */
	static public final String ERROR = "error";

}
