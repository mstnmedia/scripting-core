package com.mstn.scripting.core.enums;

/**
 *
 * @author amatos
 */
public class WorkflowStepOptionTypes {

	/**
	 *
	 */
	static public final int UNDEFINED = 0;

	/**
	 *
	 */
	static public final int STEP = 1;

	/**
	 *
	 */
	static public final int VALUE = 2;

}
