/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core.enums;

/**
 * Clase que contiene todos los posibles valores como estados de transacciones.
 *
 * @author MSTN Media
 */
public enum TransactionStates {
	//TODO: Evaluar usar este enum como tipo de dato para el campo state de la clase Transaction

	/**
	 * En curso.
	 */
	RUNNING(1),
	/**
	 * Completada. Ha sido finalizada por el paso a paso.
	 */
	COMPLETED(2),
	/**
	 * Cancelada. Ha sido finalizada abruptamente.
	 */
	CANCELLED(3);

	private int value;

	TransactionStates(int value) {
		this.value = value;
	}

	/**
	 *
	 * @return
	 */
	public int getValue() {
		return value;
	}

}
