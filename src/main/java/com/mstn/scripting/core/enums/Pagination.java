package com.mstn.scripting.core.enums;

/**
 * Clase que contiene la información por defecto de las consultas de datos por
 * paginación.
 *
 * @author amatos
 */
public class Pagination {

	/**
	 *
	 */
	static public final String DEFAULT_WHERE = "[]";

	/**
	 *
	 */
	static public final int DEFAULT_PAGE_SIZE = 10;

	/**
	 *
	 */
	static public final String DEFAULT_PAGE_SIZE_STR = "10";

	/**
	 *
	 */
	static public final int DEFAULT_PAGE_NUMBER = 1;

	/**
	 *
	 */
	static public final String DEFAULT_PAGE_NUMBER_STR = "1";

	/**
	 *
	 */
	static public final String DEFAULT_ORDER_BY = "ID desc";
}
