package com.mstn.scripting.core.enums;

/**
 *
 * @author amatos
 */
public class WorkflowVersionTypes {

	/**
	 *
	 */
	static public final int INACTIVE = 0;

	/**
	 *
	 */
	static public final int ACTIVE = 1;

	/**
	 *
	 */
	static public final int ARCHIVED = 2;

	/**
	 *
	 */
	static public final String EDITABLE_ID_VERSION = "0";

}
