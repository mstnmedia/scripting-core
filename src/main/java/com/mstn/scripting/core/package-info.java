/**
 * Este proyecto contiene las clases que son compartidas entre los microservicios, 
 * clases que contienen funcionalidades comunes y las clases del modelo de la base 
 * de datos.
 */
package com.mstn.scripting.core;

