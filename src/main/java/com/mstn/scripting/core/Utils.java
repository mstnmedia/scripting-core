/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mstn.scripting.core;

import com.mstn.scripting.core.models.Transaction_Result;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Clase que contiene funciones comunes útiles a todos los microservicios.
 *
 * @author amatos
 */
public class Utils {

	/**
	 * Imprime información de la excepción ocurrida en el output de la
	 * aplicación. Crea un nuevo {@link Logger} para la clase indicada e imprime
	 * la información de la excepción indicada con el título indicado.
	 *
	 * @param classe Clase donde ocurrió la excepción.
	 * @param title Descripción manual de la excepción.
	 * @param ex Excepción ocurrida con información del error.
	 */
	static public void logException(Class classe, String title, Exception ex) {
		Logger.getLogger(classe.getName()).log(Level.SEVERE, title, ex);
	}

	static public void logException(Class classe, String title) {
		Logger.getLogger(classe.getName()).log(Level.SEVERE, title);
	}

	static public void logWarn(Class classe, String title, Object... params) {
		Logger.getLogger(classe.getName()).log(Level.WARNING, title, params);
	}

	static public void logInfo(Class classe, String title, Object... params) {
		Logger.getLogger(classe.getName()).log(Level.INFO, title, params);
	}

	static public void logConfig(Class classe, String title, Object... params) {
		Logger.getLogger(classe.getName()).log(Level.CONFIG, title, params);
	}

	static public void logDebug(Class classe, String title, Object... params) {
		Logger.getLogger(classe.getName()).log(Level.FINE, title, params);
	}

	static public void logTrace(Class classe, String title, Object... params) {
		Logger.getLogger(classe.getName()).log(Level.FINER, title, params);
	}

	static public void logFinest(Class classe, String title, Object... params) {
		Logger.getLogger(classe.getName()).log(Level.FINEST, title, params);
	}

	/**
	 * Obtiene el primer valor no nulo de la lista de parámetros.
	 *
	 * @param <T> Clase que describe a todos los parámetros.
	 * @param values Lista de parámetros para evaluar.
	 * @return El primer parámetro no nulo encontrado; si no encuentra ninguno,
	 * retorna nulo.
	 */
	static public <T> T coalesce(T... values) {
		for (T v : values) {
			if (v != null) {
				return v;
			}
		}
		return null;
	}

	/**
	 * Obtiene {@code true} si el mapa indicado es nulo o está vacío, de lo
	 * contrario {@code false}.
	 *
	 * @param map Mapa que se desea evaluar.
	 * @return {@code true} si el mapa es nulo o vacío, {@code false} si tiene
	 * uno o más keys-values.
	 */
	static public boolean isNullOrEmpty(Map map) {
		return map == null || map.isEmpty();
	}

	/**
	 * Obtiene {@code true} si el mapa indicado ni es nulo ni está vacío, de lo
	 * contrario {@code false}.
	 *
	 * @param map Mapa que se desea evaluar.
	 * @return {@code true} si el mapa ni es nulo ni vacío, {@code false} si
	 * tiene uno o más keys-values.
	 */
	static public boolean nonNullOrEmpty(Map map) {
		return map != null && !map.isEmpty();
	}

	static public boolean isNonNullOrEmpty(Map map) {
		return map != null && !map.isEmpty();
	}

	/**
	 * Obtiene {@code true} si la lista indicada es nula o está vacía, de lo
	 * contrario {@code false}.
	 *
	 * @param list Lista que se desea evaluar.
	 * @return {@code true} si la lista es nula o vacía, {@code false} si tiene
	 * uno o más elementos.
	 */
	static public boolean isNullOrEmpty(List list) {
		return list == null || list.isEmpty();
	}

	static public boolean listIsNullOrEmpty(List list) {
		return list == null || list.isEmpty();
	}

	/**
	 * Obtiene {@code true} si la lista indicada ni es nula ni está vacía, de lo
	 * contrario {@code false}.
	 *
	 * @param list Lista que se desea evaluar.
	 * @return {@code true} si la lista ni es nula ni está vacía, {@code false}
	 * si es nula o vacía.
	 */
	static public boolean nonNullOrEmpty(List list) {
		return list != null && !list.isEmpty();
	}

	static public boolean listNonNullOrEmpty(List list) {
		return list != null && !list.isEmpty();
	}

	/**
	 * Obtiene una lista resultante de dividir el texto indicado en cada
	 * aparición de la coma.
	 *
	 * @param str Texto que será separado.
	 * @return Una lista con cada segmento del texto dividido como elemento.
	 */
	static public List<String> splitAsList(String str) {
		return splitAsList(str, ",");
	}

	/**
	 * Obtiene una lista resultante de dividir el texto indicado en cada
	 * aparición de la coma.
	 *
	 * @param str Texto que será separado.
	 * @param limit El límite de partes en que se dividirá el texto.
	 * @return Una lista con cada segmento del texto dividido como elemento.
	 * @see String#split(java.lang.String, int)
	 */
	static public List<String> splitAsList(String str, int limit) {
		return splitAsList(str, ",", limit);
	}

	/**
	 * Obtiene una lista resultante de dividir el texto indicado en cada
	 * coincidencia con el separador indicado.
	 *
	 * @param str Texto que será separado.
	 * @param splitter Texto por el cual se buscarán coincidencias para separar.
	 * @return Una lista con cada segmento del texto dividido como elemento.
	 */
	static public List<String> splitAsList(String str, String splitter) {
		return splitAsList(str, splitter, 0);
	}

	/**
	 * Obtiene una lista resultante de dividir el texto indicado en cada
	 * coincidencia con el separador indicado.
	 *
	 * @param str Texto que será separado.
	 * @param splitter Texto por el cual se buscarán coincidencias para separar.
	 * @param limit El límite de partes en que se dividirá el texto.
	 * @return Una lista con cada segmento del texto dividido como elemento.
	 * @see String#split(java.lang.String, int)
	 */
	static public List<String> splitAsList(String str, String splitter, int limit) {
		List<String> values = new ArrayList<>();
		if (!stringIsNullOrEmpty(str)) {
			String[] parts = str.split(splitter, limit);
			values.addAll(Arrays.asList(parts));
		}
		return values;
	}

	/**
	 * Obtiene el texto resultante a remover todos los carácteres no numéricos
	 * al texto indicado.
	 *
	 * @param s Texto a remover todos los carácteres no numéricos.
	 * @return null: si el texto indicado es nulo. "": si el texto no tiene
	 * carácteres numéricos. Un número como string: si el texto contiene algún
	 * caracter númerico.
	 */
	static public String replaceNonDigit(String s) {
		return s == null ? null : s.replaceAll("[^\\d]", "");
	}

	/**
	 * Obtiene si el texto está sólo conformado por carácteres numéricos.
	 *
	 * @param s Texto que será evaluado.
	 * @return {@code true} si el texto sólo contiene números, {@code false} si
	 * contiene algún otro carácter.
	 */
	static public boolean isDigits(String s) {
		return s == null ? false : s.matches("^\\d+$");
	}

	static public boolean stringIsDigits(String s) {
		return isDigits(s);
	}

	/**
	 * Obtiene {@code true} si el texto indicado es nulo o está vacío, de lo
	 * contrario {@code false}.
	 *
	 * @param value Texto que se desea evaluar.
	 * @return {@code true} si el texto es nulo o está vacío, de lo contrario
	 * {@code false}.
	 */
	static public boolean isNullOrEmpty(String value) {
		return value == null || value.isEmpty();
	}

	static public boolean stringIsNullOrEmpty(String value) {
		return isNullOrEmpty(value);
	}

	static public boolean isNullOrWhiteSpaces(String value) {
		return value == null || value.trim().isEmpty();
	}

	/**
	 * Obtiene {@code true} si el texto indicado ni es nulo ni está vacío, de lo
	 * contrario {@code false}.
	 *
	 * @param value Texto que se desea evaluar.
	 * @return {@code true} si el texto ni es nulo ni está vacío, {@code false}
	 * si es nulo o vacío.
	 */
	static public boolean nonNullOrEmpty(String value) {
		return value != null && !value.isEmpty();
	}

	static public boolean stringNonNullOrEmpty(String value) {
		return value != null && !value.isEmpty();
	}

	/**
	 * Obtiene si dos textos son equivalentes.
	 *
	 * @param a Primer texto.
	 * @param b Segundo texto.
	 * @return {@code true} si los textos son iguales, {@code false} si son
	 * diferentes.
	 */
	static public boolean equals(String a, String b) {
		return a == null ? b == null : a.equals(b);
	}

	static public boolean stringAreEquals(String a, String b) {
		return a == null ? b == null : a.equals(b);
	}

	/**
	 * Obtiene una nueva instancia {@link ArrayList} de elementos de la clase
	 * indicada.
	 *
	 * @param <T> Clase que describe los objetos que poblarán la lista.
	 * @param type Clase que describe los objetos que poblarán la lista.
	 * @return Una lista vacía.
	 */
	static public <T extends Object> ArrayList<T> getListOf(Class<T> type) {
		return new ArrayList<T>();
	}

	/**
	 * Obtine un map vacío.
	 *
	 * @return Un map vacío.
	 */
	static public Map getMapOf() {
		return new HashMap();
	}

	/**
	 * Obtiene el texto resultante de reemplazar todas las claves del map por
	 * sus valores correspondientes.
	 *
	 * @param text Texto sobre el que se reemplazarán las claves del map.
	 * @param values Map que contiene las claves y los valores reemplazantes.
	 * @return El texto resultante después del reemplazo.
	 */
	static public String interpolate(String text, Map<String, String> values) {
		String localValue = Utils.coalesce(text, "");
		if (stringNonNullOrEmpty(localValue) && values != null && !values.isEmpty()) {
			List<String> keys = values.keySet()
					.stream()
					.sorted(Comparator.reverseOrder())
					.collect(Collectors.toList());
			for (String key : keys) {
				String _key = "$" + key;
				String _value = Utils.coalesce(values.get(key), Transaction_Result.NULL_VALUE);
				localValue = localValue.replace(_key, _value);
			}
		}
		return localValue;
	}

	/**
	 * Obtiene una lista de propiedades declaradas en la clase indicada y de las
	 * clases que ésta hereda.
	 *
	 * @param type Clase indicada.
	 * @return Una lista de propiedades de todo
	 */
	static public List<Field> getDeclaratedFields(Class type) {
		List<Field> fields = new ArrayList();
		fields.addAll(Arrays.asList(type.getDeclaredFields()));
		if (type.getSuperclass() != null) {
			fields.addAll(getDeclaratedFields(type.getSuperclass()));
		}
		return fields;
	}

	/**
	 * Obtiene una lista de campos declarados desde la clase de inicio indicada
	 * hasta la clase que hereda que es igual a la clase de fin indicada.
	 *
	 * @param startClass Clase inicial
	 * @param endClass Clase heredada para indicar hasta dónde escalar buscando
	 * campos en la herencia.
	 * @return Una lista de campos.
	 */
	static public List<Field> getFieldsUpTo(Class startClass, Class endClass) {
		List<Field> fields = new ArrayList(Arrays.asList(startClass.getDeclaredFields()));
		boolean end = false;
		Class parentClass = startClass.getSuperclass();
		while (!end) {
			if (parentClass == null || parentClass == endClass) {
				end = true;
			}
			if (parentClass != null) {
				List<Field> parentFields = Arrays.asList(parentClass.getDeclaredFields());
				fields.addAll(parentFields);
				parentClass = parentClass.getSuperclass();
			}
		}
		return fields;
	}
}
